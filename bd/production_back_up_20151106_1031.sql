CREATE DATABASE  IF NOT EXISTS `co_padres_lg` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `co_padres_lg`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: mysql.wunapps.com    Database: co_padres_lg
-- ------------------------------------------------------
-- Server version	5.1.56-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_cms_user`
--

DROP TABLE IF EXISTS `tbl_cms_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cms_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_cms_user`
--

LOCK TABLES `tbl_cms_user` WRITE;
/*!40000 ALTER TABLE `tbl_cms_user` DISABLE KEYS */;
INSERT INTO `tbl_cms_user` VALUES (1,'wunderman','uzoo82qKhUKe80T8a/G887W54RMnJhdw9M9H5MH80l6Cxd9iuSlmbtxUB3yQGiwKsHjVKk2wLlu0QYwGWgcTYQ==');
/*!40000 ALTER TABLE `tbl_cms_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_match`
--

DROP TABLE IF EXISTS `tbl_match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_match` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_user_id_challenger` int(11) NOT NULL,
  `fk_user_id_oponent` int(11) NOT NULL,
  `current_turn` enum('challenger','oponent') NOT NULL DEFAULT 'challenger',
  `fk_user_id_winner` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `is_friends_match` enum('yes','no') NOT NULL DEFAULT 'no',
  `challenger_share` enum('yes','no') NOT NULL DEFAULT 'no',
  `oponent_share` enum('yes','no') NOT NULL DEFAULT 'no',
  `ended_at` datetime DEFAULT NULL,
  `declined` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `fk_tbl_match_tbl_user1_idx` (`fk_user_id_challenger`),
  KEY `fk_tbl_match_tbl_user2_idx` (`fk_user_id_oponent`),
  KEY `fk_tbl_match_tbl_user3_idx` (`fk_user_id_winner`),
  CONSTRAINT `fk_tbl_match_tbl_user1` FOREIGN KEY (`fk_user_id_challenger`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_match_tbl_user2` FOREIGN KEY (`fk_user_id_oponent`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_match_tbl_user3` FOREIGN KEY (`fk_user_id_winner`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_match`
--

LOCK TABLES `tbl_match` WRITE;
/*!40000 ALTER TABLE `tbl_match` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_match_answers`
--

DROP TABLE IF EXISTS `tbl_match_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_match_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_match_id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_question_id` int(11) NOT NULL,
  `fk_question_answer_id` int(11) DEFAULT NULL,
  `time` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_match_answers_tbl_match1_idx` (`fk_match_id`),
  KEY `fk_tbl_match_answers_tbl_user1_idx` (`fk_user_id`),
  KEY `fk_tbl_match_answers_tbl_question_answers1_idx` (`fk_question_answer_id`),
  KEY `fk_tbl_match_answers_tbl_question1_idx` (`fk_question_id`),
  CONSTRAINT `fk_tbl_match_answers_tbl_match1` FOREIGN KEY (`fk_match_id`) REFERENCES `tbl_match` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_match_answers_tbl_question1` FOREIGN KEY (`fk_question_id`) REFERENCES `tbl_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_match_answers_tbl_question_answers1` FOREIGN KEY (`fk_question_answer_id`) REFERENCES `tbl_question_answers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_match_answers_tbl_user1` FOREIGN KEY (`fk_user_id`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_match_answers`
--

LOCK TABLES `tbl_match_answers` WRITE;
/*!40000 ALTER TABLE `tbl_match_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_match_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_match_intents`
--

DROP TABLE IF EXISTS `tbl_match_intents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_match_intents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_match_intents_tbl_user1_idx` (`fk_user_id`),
  CONSTRAINT `fk_tbl_match_intents_tbl_user1` FOREIGN KEY (`fk_user_id`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_match_intents`
--

LOCK TABLES `tbl_match_intents` WRITE;
/*!40000 ALTER TABLE `tbl_match_intents` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_match_intents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_question`
--

DROP TABLE IF EXISTS `tbl_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_question`
--

LOCK TABLES `tbl_question` WRITE;
/*!40000 ALTER TABLE `tbl_question` DISABLE KEYS */;
INSERT INTO `tbl_question` VALUES (5,'¿ Pregunta de prueba 1 ?','2015-05-26 17:24:19'),(6,'¿ Pregunta de prueba 2 ?','2015-05-26 17:25:12'),(7,'¿ Pregunta de prueba 3 ?','2015-05-29 09:33:45'),(8,'¿ Pregunta de prueba 4 ?','2015-05-29 09:34:08'),(9,'¿ Pregunta de prueba 5 ?','2015-05-29 09:34:37'),(10,'¿ Pregunta de prueba 6 ?','2015-05-29 09:34:54'),(11,'¿Pregunta de prueba 7.?','2015-06-01 08:58:23');
/*!40000 ALTER TABLE `tbl_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_question_answers`
--

DROP TABLE IF EXISTS `tbl_question_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_question_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_question_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `order` int(11) NOT NULL,
  `right_answer` enum('yes','no') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_question_answers_tbl_question1_idx` (`fk_question_id`),
  CONSTRAINT `fk_tbl_question_answers_tbl_question1` FOREIGN KEY (`fk_question_id`) REFERENCES `tbl_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_question_answers`
--

LOCK TABLES `tbl_question_answers` WRITE;
/*!40000 ALTER TABLE `tbl_question_answers` DISABLE KEYS */;
INSERT INTO `tbl_question_answers` VALUES (1,5,'Incorrecta',0,'no'),(2,5,'Incorrecta',1,'no'),(3,5,'Correcta',2,'yes'),(4,6,'Incorrecta',0,'no'),(5,6,'Correcta',1,'yes'),(6,6,'Incorrecta',4,'no'),(7,7,'Incorrecta',0,'no'),(8,7,'Correcta',1,'yes'),(9,7,'Incorrecta',2,'no'),(10,8,'Correcta',0,'yes'),(11,8,'Incorrecta',1,'no'),(12,8,'Incorrecta',2,'no'),(13,9,'Incorrecta',0,'no'),(14,9,'Correcta',1,'yes'),(15,9,'Incorrecta',2,'no'),(16,10,'Incorrecta',0,'no'),(17,10,'Incorrecta',1,'no'),(18,10,'Correcta',2,'yes'),(19,11,'Correcta',0,'yes'),(20,11,'Incorrecta',1,'no'),(21,11,'Incorrecta',2,'no');
/*!40000 ALTER TABLE `tbl_question_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_seasons`
--

DROP TABLE IF EXISTS `tbl_seasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_seasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_seasons`
--

LOCK TABLES `tbl_seasons` WRITE;
/*!40000 ALTER TABLE `tbl_seasons` DISABLE KEYS */;
INSERT INTO `tbl_seasons` VALUES (1,'2015-06-01 04:00:00','2015-06-08 00:00:00'),(2,'2015-06-08 04:00:00','2015-06-15 00:00:00'),(3,'2015-06-15 04:00:00','2015-06-22 00:00:00'),(4,'2015-06-22 04:00:00','2015-06-29 00:00:00');
/*!40000 ALTER TABLE `tbl_seasons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_share_content`
--

DROP TABLE IF EXISTS `tbl_share_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_share_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_share_content`
--

LOCK TABLES `tbl_share_content` WRITE;
/*!40000 ALTER TABLE `tbl_share_content` DISABLE KEYS */;
INSERT INTO `tbl_share_content` VALUES (1,'http://facebook.com'),(2,'http://google.com');
/*!40000 ALTER TABLE `tbl_share_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_shield_designs`
--

DROP TABLE IF EXISTS `tbl_shield_designs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_shield_designs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `pos_y` int(11) NOT NULL,
  `size_text` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_shield_designs`
--

LOCK TABLES `tbl_shield_designs` WRITE;
/*!40000 ALTER TABLE `tbl_shield_designs` DISABLE KEYS */;
INSERT INTO `tbl_shield_designs` VALUES (1,'lg-team-badge-01.png',8,850),(2,'lg-team-badge-02.png',10,750),(3,'lg-team-badge-03.png',6,600),(4,'lg-team-badge-04.png',10,800),(5,'lg-team-badge-05.png',10,800),(6,'lg-team-badge-06.png',11,700),(7,'lg-team-badge-07.png',9,850),(8,'lg-team-badge-08.png',7,800),(9,'lg-team-badge-09.png',9,900);
/*!40000 ALTER TABLE `tbl_shield_designs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_team`
--

DROP TABLE IF EXISTS `tbl_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_team` (
  `fk_user_id` int(11) NOT NULL,
  `fk_shield_designs_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `entity` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fk_user_id`),
  KEY `fk_team_shield_designs1_idx` (`fk_shield_designs_id`),
  CONSTRAINT `fk_team_shield_designs1` FOREIGN KEY (`fk_shield_designs_id`) REFERENCES `tbl_shield_designs` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_team_tbl_user1` FOREIGN KEY (`fk_user_id`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_team`
--

LOCK TABLES `tbl_team` WRITE;
/*!40000 ALTER TABLE `tbl_team` DISABLE KEYS */;
INSERT INTO `tbl_team` VALUES (17,1,'Ll','2015-06-09 16:00:14','AT - Atletico'),(18,2,'DL','2015-06-09 17:21:39','AT - Atletico'),(19,7,'AH','2015-06-10 09:42:43','AS - Associazione Sportiva'),(21,7,'AH','2015-06-10 17:39:54','AC - Associazione Calcio ');
/*!40000 ALTER TABLE `tbl_team` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`lg_db`@`%`*/ /*!50003 TRIGGER initial_team_score AFTER INSERT ON tbl_team
    FOR EACH ROW BEGIN

    INSERT INTO tbl_team_score SET
        fk_team_id = NEW.fk_user_id,
        score = 0,
        type = 'initial-score',
        created_at = season_1_start();

    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tbl_team_score`
--

DROP TABLE IF EXISTS `tbl_team_score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_team_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_team_id` int(11) NOT NULL,
  `fk_match_id` int(11) DEFAULT NULL,
  `score` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `extra_info` text,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_team_score_tbl_match1_idx` (`fk_match_id`),
  KEY `fk_tbl_team_score_tbl_team1` (`fk_team_id`),
  CONSTRAINT `fk_tbl_team_score_tbl_match1` FOREIGN KEY (`fk_match_id`) REFERENCES `tbl_match` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_team_score_tbl_team1` FOREIGN KEY (`fk_team_id`) REFERENCES `tbl_team` (`fk_user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_team_score`
--

LOCK TABLES `tbl_team_score` WRITE;
/*!40000 ALTER TABLE `tbl_team_score` DISABLE KEYS */;
INSERT INTO `tbl_team_score` VALUES (34,17,NULL,0,'initial-score','2015-06-01 04:00:00',NULL),(35,17,NULL,3,'pre-register','2015-06-01 04:00:00',NULL),(36,17,NULL,1,'landing-share','2015-06-01 04:00:00',NULL),(37,18,NULL,0,'initial-score','2015-06-01 04:00:00',NULL),(38,18,NULL,3,'pre-register','2015-06-01 04:00:00',NULL),(39,19,NULL,0,'initial-score','2015-06-01 04:00:00',NULL),(40,19,NULL,3,'pre-register','2015-06-01 04:00:00',NULL),(45,21,NULL,0,'initial-score','2015-06-01 04:00:00',NULL),(46,21,NULL,3,'pre-register','2015-06-01 04:00:00',NULL);
/*!40000 ALTER TABLE `tbl_team_score` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `nid_type` varchar(255) NOT NULL,
  `nid` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `cellphone` varchar(255) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_long_lived_token` text NOT NULL,
  `created_at` datetime NOT NULL,
  `subscribed` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` VALUES (17,'Luis','Martinez','1989-05-25','lochobm@gmail.com','Cédula de ciudadania','1081403080','cra 11 a 93b-30','bogota','3046198282','10153067609124412','CAAHYw8nvYNkBALTNptoRZCjMpJeTpg0ZCkuQSMYUMadtaZBdIHC5dK6OeAXqqhrL4hojIEW6ls4tldugl7paG5ChmiK3txKZCZA1sZBJIdi28htSa4hQfbHuULeq5a2xWVLuRxTguXCeQ50kivM6WQsD35k1czxZBVp7caEIhdgS9VuuNmABZCUuzsZB4oeMJ4tYZD','2015-06-09 15:59:26','no'),(18,'Diana','Gómez','1983-03-31','diana.mgomez@hotmail.com','Cédula de ciudadania','52778239','Prueba 1 12 23','Bogotá','1234567789','10153458851233413','CAAHYw8nvYNkBAOPlZBjNPZB439GL2zLkqpTeWS6Kg528EXozn24WszJ456Nrl3FfmgZBirtlAdasJf9gUpLisxREvru4pZC6pFK6xwSamqiAwIXRobUbnNHSvuZBj6vA9mv3MuWm08HwGsbBsUsQf4w8pDGVuZAAZCHZA4HUlemQVzTsbEWmf9OAYjUlZCxVcqfsZD','2015-06-09 17:21:12','yes'),(19,'Alex','Perea','1989-01-02','apexmd21@gmail.com','Cédula de ciudadania','1018422095','cra 72 a no 11 a 30','Bogota','3003583092','10205693170022792','CAAHYw8nvYNkBAHtLdGtVBfOecRIZCu5K2tM1kO6JAOMKcoeZAmAiiHobpA2nZCjCxRSgxge6ZAEDOpOWPqmxSZB70lJrov0KzZCqHZC2zisKe4Hh2oDMPpSsBlwMIfkgJ4BITMZCBPBdVpvzIR99nwEPXJxJ94gIFc44Yrojn574i8oZCzZCy5fIzw','2015-06-10 09:42:32','yes'),(21,'AlejÖ','Kdna','1993-03-10','alejandro-327@hotmail.com','Cédula de ciudadania','4','sdafsdfdsfdsf','sdfsdfsdfsdf','2516578121','10204100284139455','CAAHYw8nvYNkBAK3hEOecRob1qXCAmi0q126krBVxphpJlKaBOe0yfZAnXBTvrZBvcZBi0SNR9gL8y16EZBZBgMQKt8ZA76GwllZCxh8Ky1315ZAujjx6ODG741l1pGG916nWBY09SradCnxuUu7mavYX6xDcejYfEJ0P0proNb86hR9tdM3o182K','2015-06-10 17:39:22','no');
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user_father`
--

DROP TABLE IF EXISTS `tbl_user_father`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user_father` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_user_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `photo` text NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_user_father_tbl_user1_idx` (`fk_user_id`),
  CONSTRAINT `fk_tbl_user_father_tbl_user1` FOREIGN KEY (`fk_user_id`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user_father`
--

LOCK TABLES `tbl_user_father` WRITE;
/*!40000 ALTER TABLE `tbl_user_father` DISABLE KEYS */;
INSERT INTO `tbl_user_father` VALUES (17,17,'luis','bonilla','2facede2bbd7e7368cb3589d75a81fa7.jpg','2015-06-09 15:59:26'),(18,18,'Luis','Gómez','0048220ac1692f77daa3957c15cc8f2a.jpg','2015-06-09 17:21:12'),(19,19,'Hernán','Perea','927a0b361f741ab156bc7f7d735efd36.PNG','2015-06-10 09:42:32'),(21,21,'Hector Fernando','Cadena','ebf4f10d16c617c71dc894f6d8ca38e3.jpg','2015-06-10 17:39:22');
/*!40000 ALTER TABLE `tbl_user_father` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vw_current_season`
--

DROP TABLE IF EXISTS `vw_current_season`;
/*!50001 DROP VIEW IF EXISTS `vw_current_season`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_current_season` (
  `id` tinyint NOT NULL,
  `start_date` tinyint NOT NULL,
  `end_date` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_match_answers_detail`
--

DROP TABLE IF EXISTS `vw_match_answers_detail`;
/*!50001 DROP VIEW IF EXISTS `vw_match_answers_detail`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_match_answers_detail` (
  `match_answer_id` tinyint NOT NULL,
  `match_id` tinyint NOT NULL,
  `user_id` tinyint NOT NULL,
  `question_id` tinyint NOT NULL,
  `answer_id` tinyint NOT NULL,
  `time` tinyint NOT NULL,
  `right_answer` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_matches_overview`
--

DROP TABLE IF EXISTS `vw_matches_overview`;
/*!50001 DROP VIEW IF EXISTS `vw_matches_overview`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_matches_overview` (
  `match_id` tinyint NOT NULL,
  `winner_id` tinyint NOT NULL,
  `is_friends_match` tinyint NOT NULL,
  `oponent_share` tinyint NOT NULL,
  `challenger_share` tinyint NOT NULL,
  `match_created_at` tinyint NOT NULL,
  `match_ended_at` tinyint NOT NULL,
  `match_declined` tinyint NOT NULL,
  `challenger_id` tinyint NOT NULL,
  `challenger_firstname` tinyint NOT NULL,
  `challenger_lastname` tinyint NOT NULL,
  `challenger_fb_id` tinyint NOT NULL,
  `challenger_fb_token` tinyint NOT NULL,
  `challenger_team_name` tinyint NOT NULL,
  `challenger_team_design_id` tinyint NOT NULL,
  `challenger_team_image` tinyint NOT NULL,
  `oponent_id` tinyint NOT NULL,
  `oponent_firstname` tinyint NOT NULL,
  `oponent_lastname` tinyint NOT NULL,
  `oponent_fb_id` tinyint NOT NULL,
  `oponent_fb_token` tinyint NOT NULL,
  `oponent_team_name` tinyint NOT NULL,
  `oponent_team_design_id` tinyint NOT NULL,
  `oponent_team_image` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_questions_answers`
--

DROP TABLE IF EXISTS `vw_questions_answers`;
/*!50001 DROP VIEW IF EXISTS `vw_questions_answers`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_questions_answers` (
  `id` tinyint NOT NULL,
  `title` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `answer_id` tinyint NOT NULL,
  `answer_description` tinyint NOT NULL,
  `answer_order` tinyint NOT NULL,
  `right_answer` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_season_four_score`
--

DROP TABLE IF EXISTS `vw_season_four_score`;
/*!50001 DROP VIEW IF EXISTS `vw_season_four_score`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_season_four_score` (
  `team_score_id` tinyint NOT NULL,
  `team_id` tinyint NOT NULL,
  `score` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_season_one_ranking`
--

DROP TABLE IF EXISTS `vw_season_one_ranking`;
/*!50001 DROP VIEW IF EXISTS `vw_season_one_ranking`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_season_one_ranking` (
  `team_score_id` tinyint NOT NULL,
  `team_id` tinyint NOT NULL,
  `score` tinyint NOT NULL,
  `rank` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_season_one_score`
--

DROP TABLE IF EXISTS `vw_season_one_score`;
/*!50001 DROP VIEW IF EXISTS `vw_season_one_score`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_season_one_score` (
  `team_score_id` tinyint NOT NULL,
  `team_id` tinyint NOT NULL,
  `score` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_season_three_score`
--

DROP TABLE IF EXISTS `vw_season_three_score`;
/*!50001 DROP VIEW IF EXISTS `vw_season_three_score`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_season_three_score` (
  `team_score_id` tinyint NOT NULL,
  `team_id` tinyint NOT NULL,
  `score` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_season_two_score`
--

DROP TABLE IF EXISTS `vw_season_two_score`;
/*!50001 DROP VIEW IF EXISTS `vw_season_two_score`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_season_two_score` (
  `team_score_id` tinyint NOT NULL,
  `team_id` tinyint NOT NULL,
  `score` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_team_score`
--

DROP TABLE IF EXISTS `vw_team_score`;
/*!50001 DROP VIEW IF EXISTS `vw_team_score`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_team_score` (
  `team_id` tinyint NOT NULL,
  `score` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_team_with_score`
--

DROP TABLE IF EXISTS `vw_team_with_score`;
/*!50001 DROP VIEW IF EXISTS `vw_team_with_score`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_team_with_score` (
  `fk_user_id` tinyint NOT NULL,
  `fk_shield_designs_id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `entity` tinyint NOT NULL,
  `score` tinyint NOT NULL,
  `shield_image` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_user_father`
--

DROP TABLE IF EXISTS `vw_user_father`;
/*!50001 DROP VIEW IF EXISTS `vw_user_father`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_user_father` (
  `id` tinyint NOT NULL,
  `firstname` tinyint NOT NULL,
  `lastname` tinyint NOT NULL,
  `birthdate` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `nid_type` tinyint NOT NULL,
  `nid` tinyint NOT NULL,
  `address` tinyint NOT NULL,
  `city` tinyint NOT NULL,
  `cellphone` tinyint NOT NULL,
  `facebook_id` tinyint NOT NULL,
  `facebook_long_lived_token` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `father_id` tinyint NOT NULL,
  `father_firstname` tinyint NOT NULL,
  `father_lastname` tinyint NOT NULL,
  `father_photo` tinyint NOT NULL,
  `father_created_at` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'co_padres_lg'
--
/*!50003 DROP FUNCTION IF EXISTS `season_1_end` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`lg_db`@`%` FUNCTION `season_1_end`() RETURNS datetime
    DETERMINISTIC
BEGIN

DECLARE the_date DATETIME;

SET the_date = (SELECT end_date FROM tbl_seasons WHERE id = 1 );

RETURN the_date;
 
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `season_1_start` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`lg_db`@`%` FUNCTION `season_1_start`() RETURNS datetime
    DETERMINISTIC
BEGIN

DECLARE the_date DATETIME;

SET the_date = (SELECT start_date FROM tbl_seasons WHERE id = 1 );

RETURN the_date;
 
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `season_2_end` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`lg_db`@`%` FUNCTION `season_2_end`() RETURNS datetime
    DETERMINISTIC
BEGIN

DECLARE the_date DATETIME;

SET the_date = (SELECT end_date FROM tbl_seasons WHERE id = 2 );

RETURN the_date;
 
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `season_2_start` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`lg_db`@`%` FUNCTION `season_2_start`() RETURNS datetime
    DETERMINISTIC
BEGIN

DECLARE the_date DATETIME;

SET the_date = (SELECT start_date FROM tbl_seasons WHERE id = 2 );

RETURN the_date;
 
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `season_3_end` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`lg_db`@`%` FUNCTION `season_3_end`() RETURNS datetime
    DETERMINISTIC
BEGIN

DECLARE the_date DATETIME;

SET the_date = (SELECT end_date FROM tbl_seasons WHERE id = 3 );

RETURN the_date;
 
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `season_3_start` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`lg_db`@`%` FUNCTION `season_3_start`() RETURNS datetime
    DETERMINISTIC
BEGIN

DECLARE the_date DATETIME;

SET the_date = (SELECT start_date FROM tbl_seasons WHERE id = 3 );

RETURN the_date;
 
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `season_4_end` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`lg_db`@`%` FUNCTION `season_4_end`() RETURNS datetime
    DETERMINISTIC
BEGIN

DECLARE the_date DATETIME;

SET the_date = (SELECT end_date FROM tbl_seasons WHERE id = 4 );

RETURN the_date;
 
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `season_4_start` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`lg_db`@`%` FUNCTION `season_4_start`() RETURNS datetime
    DETERMINISTIC
BEGIN

DECLARE the_date DATETIME;

SET the_date = (SELECT start_date FROM tbl_seasons WHERE id = 4 );

RETURN the_date;
 
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `wunapps_now` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`lg_db`@`%` FUNCTION `wunapps_now`() RETURNS datetime
    DETERMINISTIC
BEGIN

RETURN CONVERT_TZ(NOW(),'-2:00','-0:00');

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `vw_current_season`
--

/*!50001 DROP TABLE IF EXISTS `vw_current_season`*/;
/*!50001 DROP VIEW IF EXISTS `vw_current_season`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_current_season` AS select `tbl_seasons`.`id` AS `id`,`tbl_seasons`.`start_date` AS `start_date`,`tbl_seasons`.`end_date` AS `end_date` from `tbl_seasons` where ((`tbl_seasons`.`start_date` <= `wunapps_now`()) and (`tbl_seasons`.`end_date` >= `wunapps_now`())) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_match_answers_detail`
--

/*!50001 DROP TABLE IF EXISTS `vw_match_answers_detail`*/;
/*!50001 DROP VIEW IF EXISTS `vw_match_answers_detail`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_match_answers_detail` AS select `a`.`id` AS `match_answer_id`,`a`.`fk_match_id` AS `match_id`,`a`.`fk_user_id` AS `user_id`,`a`.`fk_question_id` AS `question_id`,`a`.`fk_question_answer_id` AS `answer_id`,`a`.`time` AS `time`,`qa`.`right_answer` AS `right_answer` from (`tbl_match_answers` `a` left join `tbl_question_answers` `qa` on((`qa`.`id` = `a`.`fk_question_answer_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_matches_overview`
--

/*!50001 DROP TABLE IF EXISTS `vw_matches_overview`*/;
/*!50001 DROP VIEW IF EXISTS `vw_matches_overview`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_matches_overview` AS select `m`.`id` AS `match_id`,`m`.`fk_user_id_winner` AS `winner_id`,`m`.`is_friends_match` AS `is_friends_match`,`m`.`oponent_share` AS `oponent_share`,`m`.`challenger_share` AS `challenger_share`,`m`.`created_at` AS `match_created_at`,`m`.`ended_at` AS `match_ended_at`,`m`.`declined` AS `match_declined`,`uc`.`id` AS `challenger_id`,`uc`.`firstname` AS `challenger_firstname`,`uc`.`lastname` AS `challenger_lastname`,`uc`.`facebook_id` AS `challenger_fb_id`,`uc`.`facebook_long_lived_token` AS `challenger_fb_token`,`tc`.`name` AS `challenger_team_name`,`sc`.`id` AS `challenger_team_design_id`,`sc`.`image` AS `challenger_team_image`,`uop`.`id` AS `oponent_id`,`uop`.`firstname` AS `oponent_firstname`,`uop`.`lastname` AS `oponent_lastname`,`uop`.`facebook_id` AS `oponent_fb_id`,`uop`.`facebook_long_lived_token` AS `oponent_fb_token`,`top`.`name` AS `oponent_team_name`,`sop`.`id` AS `oponent_team_design_id`,`sop`.`image` AS `oponent_team_image` from ((((((`tbl_match` `m` join `tbl_user` `uc` on((`uc`.`id` = `m`.`fk_user_id_challenger`))) join `tbl_team` `tc` on((`tc`.`fk_user_id` = `uc`.`id`))) join `tbl_shield_designs` `sc` on((`sc`.`id` = `tc`.`fk_shield_designs_id`))) join `tbl_user` `uop` on((`uop`.`id` = `m`.`fk_user_id_oponent`))) join `tbl_team` `top` on((`top`.`fk_user_id` = `uop`.`id`))) join `tbl_shield_designs` `sop` on((`sop`.`id` = `top`.`fk_shield_designs_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_questions_answers`
--

/*!50001 DROP TABLE IF EXISTS `vw_questions_answers`*/;
/*!50001 DROP VIEW IF EXISTS `vw_questions_answers`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_questions_answers` AS select `q`.`id` AS `id`,`q`.`title` AS `title`,`q`.`created_at` AS `created_at`,`qa`.`id` AS `answer_id`,`qa`.`description` AS `answer_description`,`qa`.`order` AS `answer_order`,`qa`.`right_answer` AS `right_answer` from (`tbl_question` `q` join `tbl_question_answers` `qa` on((`qa`.`fk_question_id` = `q`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_season_four_score`
--

/*!50001 DROP TABLE IF EXISTS `vw_season_four_score`*/;
/*!50001 DROP VIEW IF EXISTS `vw_season_four_score`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_season_four_score` AS select `s`.`id` AS `team_score_id`,`s`.`fk_team_id` AS `team_id`,sum(`s`.`score`) AS `score` from `tbl_team_score` `s` where ((`s`.`created_at` >= `season_4_start`()) and (`s`.`created_at` <= `season_4_end`())) group by `s`.`fk_team_id` order by sum(`s`.`score`) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_season_one_ranking`
--

/*!50001 DROP TABLE IF EXISTS `vw_season_one_ranking`*/;
/*!50001 DROP VIEW IF EXISTS `vw_season_one_ranking`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_season_one_ranking` AS select `t`.`team_score_id` AS `team_score_id`,`t`.`team_id` AS `team_id`,`t`.`score` AS `score`,(1 + (select count(`vw_season_one_score`.`score`) from `vw_season_one_score` where (`t`.`score` < `vw_season_one_score`.`score`))) AS `rank` from `vw_season_one_score` `t` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_season_one_score`
--

/*!50001 DROP TABLE IF EXISTS `vw_season_one_score`*/;
/*!50001 DROP VIEW IF EXISTS `vw_season_one_score`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_season_one_score` AS select `s`.`id` AS `team_score_id`,`s`.`fk_team_id` AS `team_id`,sum(`s`.`score`) AS `score` from `tbl_team_score` `s` where ((`s`.`created_at` >= `season_1_start`()) and (`s`.`created_at` <= `season_1_end`())) group by `s`.`fk_team_id` order by sum(`s`.`score`) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_season_three_score`
--

/*!50001 DROP TABLE IF EXISTS `vw_season_three_score`*/;
/*!50001 DROP VIEW IF EXISTS `vw_season_three_score`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_season_three_score` AS select `s`.`id` AS `team_score_id`,`s`.`fk_team_id` AS `team_id`,sum(`s`.`score`) AS `score` from `tbl_team_score` `s` where ((`s`.`created_at` >= `season_3_start`()) and (`s`.`created_at` <= `season_3_end`())) group by `s`.`fk_team_id` order by sum(`s`.`score`) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_season_two_score`
--

/*!50001 DROP TABLE IF EXISTS `vw_season_two_score`*/;
/*!50001 DROP VIEW IF EXISTS `vw_season_two_score`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_season_two_score` AS select `s`.`id` AS `team_score_id`,`s`.`fk_team_id` AS `team_id`,sum(`s`.`score`) AS `score` from `tbl_team_score` `s` where ((`s`.`created_at` >= `season_2_start`()) and (`s`.`created_at` <= `season_2_end`())) group by `s`.`fk_team_id` order by sum(`s`.`score`) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_team_score`
--

/*!50001 DROP TABLE IF EXISTS `vw_team_score`*/;
/*!50001 DROP VIEW IF EXISTS `vw_team_score`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_team_score` AS select `s`.`fk_team_id` AS `team_id`,sum(`s`.`score`) AS `score` from `tbl_team_score` `s` group by `s`.`fk_team_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_team_with_score`
--

/*!50001 DROP TABLE IF EXISTS `vw_team_with_score`*/;
/*!50001 DROP VIEW IF EXISTS `vw_team_with_score`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_team_with_score` AS select `t`.`fk_user_id` AS `fk_user_id`,`t`.`fk_shield_designs_id` AS `fk_shield_designs_id`,`t`.`name` AS `name`,`t`.`created_at` AS `created_at`,`t`.`entity` AS `entity`,sum(`s`.`score`) AS `score`,`d`.`image` AS `shield_image` from ((`tbl_team` `t` left join `tbl_team_score` `s` on((`s`.`fk_team_id` = `t`.`fk_user_id`))) left join `tbl_shield_designs` `d` on((`d`.`id` = `t`.`fk_shield_designs_id`))) group by `t`.`fk_user_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_user_father`
--

/*!50001 DROP TABLE IF EXISTS `vw_user_father`*/;
/*!50001 DROP VIEW IF EXISTS `vw_user_father`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`lg_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_user_father` AS select `u`.`id` AS `id`,`u`.`firstname` AS `firstname`,`u`.`lastname` AS `lastname`,`u`.`birthdate` AS `birthdate`,`u`.`email` AS `email`,`u`.`nid_type` AS `nid_type`,`u`.`nid` AS `nid`,`u`.`address` AS `address`,`u`.`city` AS `city`,`u`.`cellphone` AS `cellphone`,`u`.`facebook_id` AS `facebook_id`,`u`.`facebook_long_lived_token` AS `facebook_long_lived_token`,`u`.`created_at` AS `created_at`,`uf`.`id` AS `father_id`,`uf`.`firstname` AS `father_firstname`,`uf`.`lastname` AS `father_lastname`,`uf`.`photo` AS `father_photo`,`uf`.`created_at` AS `father_created_at` from (`tbl_user` `u` join `tbl_user_father` `uf` on((`uf`.`fk_user_id` = `u`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-11 10:33:39
