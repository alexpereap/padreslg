/*READY COMMONS*/
  jQuery(document).ready(function($) { /*"use strict";*/

  	jQuery.backstretch("./images/bg-mainDads-LG.jpg",{fade:200});
  	jQuery('#sider-cta').sidr();

  	//*CUSTOM DROPDOWNS*//
  	jQuery(".dropDownfrm a.chssd").click(function(event){
        event.preventDefault();
        //event.stopPropagation();
        var theDropdown = jQuery(this).parent().find('ul');
        if( theDropdown.is(':visible') ){ theDropdown.slideUp(300);
        }else{ theDropdown.slideDown(400); }
        theDropdown.parent().find('a.chssd').toggleClass('open');
    });

  	//*CHOOSING BADGES*//
  	jQuery(".thumbs-badges li img").click(function(event){
  		event.preventDefault();
  		/*UPDATE TO DEFAULT BADGES*/
  		jQuery('.thumbs-badges li').removeClass('choosed');
  		var titTeam = jQuery('#badge-constructor figcaption h1');
  			titTeam.text('').hide();
  		var $_this = jQuery(this), theBadge = $_this.parent();
  			theBadge.removeClass('inactive').addClass('choosed').siblings('.thumbs-badges li:not(.choosed)').addClass('inactive');
  		/*PREPARE vars LARGE BADGE */
			var theBagdeSRC = $_this.attr('src'), nameTeam = theBadge.parent().attr('data-NameTeam'), sizeData = theBadge.attr('data-sizeTxt'), posYdata = theBadge.attr('data-PosY');	
			//console.log('src:'+theBagdeSRC);
			
		    jQuery('#badge-constructor img').fadeOut(400,function(){
		        $_this.theBagdeSRC = theBagdeSRC;
		        jQuery(this).fadeIn(400, function(){ /*SET THE TEAM NAME*/
					titTeam.css({'margin-top': posYdata+'0px', 'font-size': sizeData+'%'}).text(nameTeam).fadeIn(); 
				}).attr('src', theBagdeSRC);
		    });
		
  	});

    //GET NUMBER FROM
    var $numPoints = jQuery('#counter-points h1').text(), min_size = 6;
    //**COUNTER ANIMATED POINTS*//
      function incrementPoints(){
        var drawNum = parseFloat(jQuery('#counter-points h1').text()) + 1;
        jQuery('#counter-points h1').text( leading_zero( drawNum )  );
        //jQuery('#counter-points h1').text(parseFloat(jQuery('#counter-points h1').text())+1);
        //var incrementvalue =  parseFloat( jQuery('#counter-points h1').text() ) + 1 ;
        //jQuery('#counter-points h1').text( parseInt( incrementvalue , 6) );
          if(parseFloat(jQuery('#counter-points h1').text()) < $numPoints){
             setTimeout(incrementPoints,15.5); 
          }
      };

      /* http://stackoverflow.com/questions/29532899/increment-a-number-with-out-getting-rid-of-leading-zeroes-jquery
        ;) */
        function leading_zero (num) {
          var new_num = num.toString();
          for (var i = new_num.length; i < min_size; i++) {
              new_num = '0' + new_num;
          }
          return new_num;
        };
    
    //CHECK TO THE POINTS TWEEN

    if( ( $numPoints != null || $numPoints != '' ) && ( parseFloat($numPoints) > 0 )  ){
      jQuery('#counter-points h1').text('000000');
      jQuery(window).load(function(){
        setTimeout( incrementPoints, 2000 );
      });
    }else{
      jQuery('#counter-points h1').text('000000');
    };
    /*PANEL SIDER*/
  	function makeSiderMobile(){  
  		/*ONLY MOBILE STUFF*/
  		 if(jQuery(window).width() < 979 ){
  		 	//PREVENT OPEN SIDER
  		 	jQuery.sidr('close');
  		 	if(jQuery("#sidr #head-dads").length){}else{
  		 		/*CLONE AND APPEND HEAD DADS COMMONS*/
	  		 	jQuery("#head-dads").clone().appendTo('#sidr');
	  		 	jQuery("#sidr #lg-campaign-headlogo, #sidr #lg-main-logo").removeClass();
	  		 	jQuery("#sidr #lg-campaign-boxed").removeClass().attr('class',"container");
	  		 }
  		 }else{ /*console.log('here');*/ };
  	};

      if(jQuery('#sidr')[0] || jQuery('#sidr').length > 0) {
    	  /*SWIPPE TOUCH MOBILE*/
      	jQuery(window).touchwipe({
          
              wipeLeft: function() {
                jQuery.sidr('close'); // Close
              },
              wipeRight: function() {
                jQuery.sidr('open'); // Open
              },
              preventDefaultEvents: false
          
        });
      };

  		/*TRIGGER RESIZE FUNCTION*/
      jQuery(window).on('resize',function() {
          //console.log('resize');
          makeSiderMobile();        
      }).trigger('resize');


    /* GAMEPLAY */
      if( jQuery('.no-animated-head').length > 0 || jQuery('.no-animated-head')[0] ){
        jQuery('#lg-campaign-boxed').removeClass('fadeInDown');
        //jQuery('#head-dads').removeClass('animatedParent');
      }
      /*COUNTDOWN*/
        if( jQuery('body').hasClass('iscountdown') ){
           jQuery("#main-runner-counter").runner({ startAt: 50000, stopAt: 0, autostart: true, countdown: true });
        };     
      /*VALIDE IF TRIVIAL GROUP EXIST*/
        if(jQuery('#trivial-items')[0] || jQuery('#trivial-items').length > 0 ){
          if(jQuery(window).width() < 767 ){
            setTimeout(function(){ jQuery('#field-left-team .content, #field-right-team .content').addClass('toOpacity');
            }, 1200);
              if (jQuery(window).width() < 569) {
                jQuery('#footLgDads').hide();
              };
          } 
          //PREPARE QUESTIONS TRIVIALS
            jQuery('li.triviaQuestion').hide();
            jQuery('li.triviaQuestion.actvTrivia').show();
            
            //CTA BTNS INSIDE TRIVIA QUESTION
              jQuery('.triviaCTA').click(function(e) {
                e.preventDefault();
                //CALL THE FUNCTION VALIDATION TRIVIAL
                fadeTrivialQuestion(jQuery(this).closest('.triviaQuestion'));
              });
              jQuery('.trivial-questions li a').click(function(e){
                e.preventDefault();
                jQuery('.trivial-questions li a').removeClass('selected')/*.removeClass('unselected')*/;
                jQuery(this).addClass('selected');
              });

            //FUNCTION TO SHOW NEXT TRIVIA QUESTION / FINAL RESULT
            function fadeTrivialQuestion($trivial) {
                $trivial.fadeOut('fast',function() {
                  jQuery('.triviaQuestion').removeClass('actvTrivia');
                  //RESOLVE THE LENGTH TRIVIAL WITH N° ID´s - triviaQuestion#
                    if ($trivial.attr('id') !== jQuery('.triviaQuestion:last').attr('id')) {
                      animateTheFieldGame($trivial);
                      //SHOW THE ERROR OR SUCCESS TRIVIAL RESULT FOR THE QUESTION
                        jQuery('#trivial-responses').addClass('actvTrivia').fadeIn();
                        jQuery('#trivial-question-result').fadeIn();
                        var $trivResponseRes = jQuery('li#trivial-responses #trivial-question-result');
                        $trivResponseRes.find('h1').hide();
                        //RESPONSE TRIVIAL QUESTION VALIDATE ATTR LI.triviaQuestion DATA-RESULT-QTN
                        if(jQuery(this).attr('data-result-qtn').toLowerCase() === 'true'){
                          $trivResponseRes.find('h1.success-trivial-response').fadeIn();
                        }else{
                          $trivResponseRes.find('h1.error-trivial-response').fadeIn();
                        }
                        /* THE MAGIC IS CALLED AFTER
                          --- $trivial.next('.triviaQuestion').addClass('actvTrivia').fadeIn('fast'); */
                    } else {
                      //TRIVIAL IS COMPLETED
                        console.log('Trivial done!');
                        var theCurrentTeam = jQuery('.figPlayTeam.theCurrentTeam');
                        animateTheFieldGame($trivial, true);
                          //moveTeamAtPosition( $trivial, theCurrentTeam, 3 )
                    }
                });
            };

            //TWEEN FIELD GAME
            function animateTheFieldGame($trivial, isCompleted){
              //PAUSE THE COUNTER
                jQuery("#main-runner-counter").runner('stop');
                var $theTeamIs = jQuery('.figPlayTeam.theCurrentTeam'), trivialPos = $trivial.find('h1 span.num').attr('data-current-trivial');
                if (!isCompleted) {
                  //SHOW THE FIELD GAME AFTER RESULT IN 2secondS IS HIDES
                  setTimeout(function(){
                    //RESET TO DEFAULTS
                      jQuery('.listTrivia.actvTrivia').fadeOut(function(){ jQuery(this).removeClass('actvTrivia'); });
                      jQuery('#game-field').addClass('showField');
                        jQuery('#game-play-field').animate({opacity:1},'slow');
                      moveTeamAtPosition($trivial, $theTeamIs, trivialPos);
                  }, 2000);
                }else{
                  //TRIVIAL IS COMPLETED
                    jQuery('#game-field').addClass('showField');
                    jQuery('#game-play-field').animate({opacity:1},'slow');
                    setTimeout(function(){ moveTeamAtPosition($trivial, $theTeamIs, trivialPos); }, 2000);
                }

            };

            /*GAMEPLAYS TEAMS*/
            var limitsXPosNumber = [28,62,90], pathPosYTeamLeft = [75,24,50], pathPosYTeamRight = [24,75,50];
              /*minNumber = 0, maxNumber = 100,*/
              //moveTeamAtPosition(minNumber, maxNumber);
              function moveTeamAtPosition($trivial, $team, posQstnTrvl){
                //console.log('question trivial pos:'+posQstnTrvl)
                //var randomNumber = Math.floor(Math.random()*(max-min+1)+min);
                var leftORright, posXFieldTrvl, posYFieldTrvl, paramsAnimate, posQstnTrvlIS, dly;
                posXFieldTrvl = limitsXPosNumber[posQstnTrvl-1];
                if (posQstnTrvl != 3) { posQstnTrvlIS = true;  dly = 1000; }else{ dly = 200 };
                //CHECK OUT FOR THE TEAM TO MOVE
                  if( $team.attr('id') == 'team-left' ){
                    leftORright = 'left';
                    posYFieldTrvl = pathPosYTeamLeft[posQstnTrvl-1];
                    paramsAnimate = { top:posYFieldTrvl+'%', left:posXFieldTrvl+'%' };
                    jQuery('#trckPthLft.trckPth').delay(dly).animate({width:(posXFieldTrvl+8)+'%', opacity:1});
                      //console.log('trackPathWidth'+posXFieldTrvl+' trackPathWidthOpacity');
                  }else if( $team.attr('id') == 'team-right' ){ 
                    leftORright = 'right';
                    posYFieldTrvl = pathPosYTeamRight[posQstnTrvl-1];
                    paramsAnimate = { top:posYFieldTrvl+'%', right:posXFieldTrvl+'%' };
                    jQuery('#trckPthRgt.trckPth').delay(dly).animate({width:(posXFieldTrvl+8.2)+'%', opacity:1});
                  };  
                //return false;            
                //ANIMATE THE TEAM
                if(posQstnTrvlIS){
                  //NORMAL TRIVIAL TWEENS
                  setTimeout(function(){
                    $team.animate(paramsAnimate,'slow', function(){
                      //RETURN TO THE TRIVIAL QUESTION VAL FROM fadeTrivialQuestion
                        setTimeout(function(){
                          jQuery('#game-field').removeClass('showField');
                          //THE MAGIC
                          $trivial.next('.triviaQuestion').addClass('actvTrivia').fadeIn('fast');
                           jQuery('.ruller-status li').removeClass('activeTrivial').eq(posQstnTrvl).addClass('activeTrivial');
                          jQuery('#game-play-field').animate({opacity:0},'fast', function(){
                            jQuery(this).removeClass();
                          });
                          jQuery("#main-runner-counter").runner('start');
                        }, 1200);
                        //console.log('HAS ANIMATED');
                    });
                  }, 1000);
                }else{
                  //SHOW THE COMPLETE TRIVIAL RESULT
                    $team.animate(paramsAnimate,'slow', function(){
                      setTimeout(function(){
                        jQuery('#game-field').removeClass('showField');
                        jQuery('#game-play-field').animate({'opacity':0},'fast');
                        //RUNNER HAS BEEN STOPED -- 
                        jQuery("#main-runner-counter").runner('stop');
                          var finalTime = jQuery("#main-runner-counter").text();
                          jQuery('#result-runner-counter').text(finalTime);
                        //SHOW END RESULTS
                        $trivial.next('.triviaQuestion').removeClass('actvTrivia');
                        jQuery('#trivial-responses, #trivial-question-result').hide();
                        jQuery('#trivial-responses').fadeIn('fast', function(){ 
                          jQuery('#trivial-end-result').fadeIn('fast');
                        });
                      }, 1000);
                    });

                }
              };

        };

    /*CHARTS*/
      if( jQuery('.info-charts')[0] || jQuery('.info-charts').length > 0 ){
        jQuery(function() {
          jQuery('.chart').easyPieChart({
            barColor: '#c20e3a',
            trackColor: '#dee0e0',
            lineWidth: 22,
            size: 137,
            scaleColor : false,
            lineCap: 'butt',
            animate: 1400,
            onStop: function(el){
              jQuery('.fig-the-chart').addClass('bgChart');
            }
          });
        });
      };

  });