<section id="main-middle">
    <div class="tabs-content">
      <div class="tabs-header">
        <div class="title">Resumen de partidos:</div>
        <ul id="tabs" class="tabs-nav">
          <li class="active" ><span>Invitaciones</span></li>
          <li ><span>Jugados</span></li>
          <li><span>Pendiente</span></li>

        </ul>
      </div>
      <!--  -->

      <div id="invite" class="tabs-container active">
        <div class="title">Invitaciones:</div>
        <div class="table-responsive">
          <table class="table-info">
            <thead>
              <tr>
                <th>Fecha</th>
                <th class="text-left">Rival</th>
                <th colspan="2">Acciones</th>
              </tr>
            </thead>
            <tbody>

              <?php foreach( $received_matches as $r ): ?>
              <tr>
                <td><?php echo date('d/m', strtotime($r->match_created_at) )  ?></td>
                <td class="text-left"><span class="thumb"><img src="" alt=""></span><?php echo $r->challenger_team_name . ' ' . $r->challenger_team_entity . ' - ' . $r->challenger_firstname . ' ' . $r->challenger_lastname  ?></td>
                <?php if( $r->match_declined == 'no' ): ?>
                  <td>
                      <?php
                          if( $r->is_friends_match == 'yes'  ){

                              if( $remaining_matches_with_friends > 0 ){
                                echo '<a href="' . base_url('game/play_match/' . $r->match_id  ) . '"><span>Jugar</span></a>';
                              } else {
                                echo '<span>0 juegos con amigos disponibles por hoy</span>';
                              }

                          } else if( $r->is_friends_match == 'no' ) {

                              if(  $remaining_week_plays > 0  ){
                                echo '<a href="' . base_url('game/play_match/' . $r->match_id ) . '"><span>Jugar</span></a>';
                              } else {
                                echo '<span>0 juegos disponibles por hoy.</span>';
                              }

                          }
                      ?>

                  </td>
                  <td><a href="<?php echo base_url('game/decline_match/' . $r->match_id ) ?>"><span>Rechazar</span></a></td>
                <?php else: ?>
                <td colspan="2" >Has rechazado a tu ríval</td>
                <?php endif; ?>

              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>

      <div id="summary" class="tabs-container">
        <div class="title">Jugados:</div>
        <div class="table-responsive">
          <table class="table-info">
            <thead>
              <tr>
                <th>Fecha</th>
                <th class="text-left">Rival</th>
                <th>Resultado</th>

              </tr>
            </thead>
            <tbody>
            <?php foreach( $ended_matches as $r ): ?>
              <tr>
                <td><?php echo date('d/m', strtotime($r->match_created_at) )  ?></td>
                <td class="text-left"><span class="thumb"><img src="" alt=""></span>
                  <?php if( $this->session->userdata('user_public')->id == $r->challenger_id ): ?>
                    <?php echo $r->oponent_team_name . ' ' . $r->oponent_team_entity . ' - ' . $r->oponent_firstname . ' ' . $r->oponent_lastname  ?>
                  <?php else: ?>
                    <?php echo $r->challenger_team_name . ' ' . $r->challenger_team_entity . ' - ' . $r->challenger_firstname . ' ' . $r->challenger_lastname  ?>
                  <?php endif; ?>
                </td>
                <td>
                    <?php if( $this->session->userdata('user_public')->id == $r->winner_id ): ?>
                      Ganaste.
                  <?php else: ?>
                      Perdiste.
                  <?php endif; ?>
                </td>

              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
          <!-- <ul class="info-pagination">
            <li><div class="prev"></div></li>
            <li class="active"><a href="#"><span>1</span></a></li>
            <li><a href="#"><span>2</span></a></li>
            <li><a href="#"><span>3</span></a></li>
            <li><div class="next"></div></li>
          </ul> -->
        </div>
      </div>
      <!--  -->
      <div id="pending" class="tabs-container">
        <div class="title">Pendientes:</div>
        <div class="table-responsive">
          <table class="table-info">
            <thead>
              <tr>
                <th>Fecha</th>
                <th class="text-left">Rival</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach( $pending_matches as $r ): ?>
              <tr>
                <td><?php echo date('d/m', strtotime($r->match_created_at) )  ?></td>
                <td class="text-left"><span class="thumb"><img src="" alt=""></span>
                  <?php echo $r->oponent_team_name . ' ' . $r->oponent_team_entity . ' - ' . $r->oponent_firstname . ' ' . $r->oponent_lastname ?>
                  <?php if( $r->match_declined == 'yes' ): ?>
                    - <span class="paint-it-red" ><b>Tu rival rechazo</b> <span class="success-txt">(se te ha devuelto 1 oportunidad de juego)</span></span>
                  <?php endif; ?>
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
      <!--  -->
      
    </div>
  </section><!--/#main-middle-->

<!-- ==================================================
  -CAMPAIGNS IMAGES BACK -->
  <section id="campaigns-elmnts">
  </section><!--/#campigns-elmnts-->