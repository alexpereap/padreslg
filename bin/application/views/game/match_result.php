<h1>match summary</h1>

<?php if( $match->fk_user_id_winner == '' ): ?>
    <p>Respondiste acertadamente <?php echo $your_number_of_right_answers ?> de 3 preguntas</p>
    <p>Tiempo total: <?php echo $your_match_time / 1000 ?>"</p>
    <p>Ahora es el turno de tu oponente</p>
<?php else: ?>

    <?php if( $you_re_winner == 'yes' ): ?>
        <p>Ganaste el partido </p>
    <?php else: ?>
        <p>Perdiste el partido</p>
    <?php endif; ?>

    <p>Respondiste acertadamente <?php echo $your_number_of_right_answers ?> de 3 preguntas</p>
    <p>Tiempo total: <?php echo $your_match_time / 1000 ?>"</p>

<?php endif; ?>

<?php if( $already_share == 'no' && $match_share_points < 5 ): ?>

    <?php if( $share_type == 'facebook' ): ?>
        <button id="shareMatchPointFb" >Facebook share</button>
    <?php endif; ?>

    <?php if( $share_type == 'url' ): ?>
        <a target="_BLANK" id="shareMatchPointUrl" href="<?php echo $share_content->url ?>">share url</a>
    <?php endif; ?>

<?php endif; ?>