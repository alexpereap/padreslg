

 <section id="main-middle">
 <?php if( $share_type == 'facebook' ): ?>
      <div class="row full-height toMidWidth">
        <div class="row-same-height row-full-height">
          <div class="col-xs-12 col-xs-height col-full-height">
              <div class="content to-the-center text-center off-set--10">
                <hgroup class="info-messages">
                  <h1>¡Gana un punto extra!</h1>
                  <p class="info" style="margin-top:15px;">Cuéntale a tus amigos que estás participando en la Trivia Futbolera de LG y gánate un punto adicional.</p>
                </hgroup>
                <figure class="share-figure">
                    <img src="images/shareFigure-Placeholder.png" />
                </figure>
                <br/>
                <br/>
                 <a class="lg-Btn trnstn" href="javascript:void(0);">
                    <button id="shareMatchPointFb" class="red-CTA fbk-icon">
                      Comparte con Facebook
                    </button></a><!--/.lg-Btn.trnstn-->
              </div><!--/.content.to-the-center-->

          </div><!--/.col-xs-12.col-xs-height.col-full-height-->
        </div><!--/.row-same-height.row-full-height-->
      </div><!--/.row.full-height-->
  <?php else: ?>
      <div class="product-content ">
        <hgroup class="info-messages">
          <h1>¡Gana un punto extra!</h1>
          <p class="info" style="margin-top:15px;">Conoce más sobre nuestros productos y gánate un punto adicional.</p>
        </hgroup>
        <figure class="product-figure">
          <!-- <img src="images/awards/01.png" alt=""> -->
          <!-- <img src="images/awards/02.png" alt=""> -->
          <!-- <img src="images/awards/03.png" alt=""> -->
          <img src="images/awards/<?php echo $share_content->image ?>" alt="">
          <!-- <img src="images/awards/05.png" alt=""> -->
          <!-- <img src="images/awards/06.png" alt=""> -->
          <!-- <img src="images/awards/07.png" alt=""> -->
          <!-- <img src="images/awards/08.png" alt=""> -->
        </figure>
        <br/>
        <br/>
        <a class="lg-Btn trnstn" target="_BLANK" id="shareMatchPointUrl" href="<?php echo $share_content->url ?>" >
                    <button id="shareMatchPointUrl" class="red-CTA ">
                      Conoce más
                    </button></a><!--/.lg-Btn.trnstn-->
      </div>
  <?php endif; ?>
  </section>