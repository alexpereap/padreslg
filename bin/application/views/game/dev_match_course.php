<h1>Pregunta <?php echo $this->session->userdata('current_match')->question_number ?> de 3 </h1>

<h2 id="timer"></h2>
<p><?php echo $question[0]->title ?></p>




    <?php foreach( $question as $key => $qa ): ?>
        <div>
            <span><?php echo $qa->answer_description ?></span>
            <input type="radio" name="answer" value="<?php echo $qa->answer_id ?>"  >
        </div>
    <?php endforeach; ?>


<form action="<?php echo base_url('game/handle_match_answer') ?>" method="POST" >
    <input type="hidden" id="inMatchCourse" value="1" >
    <button id="nextQuestion" style="display:none;" type="submit"> <?php echo $this->session->userdata('current_match')->question_number == 3 ? 'Disparar al arco' : 'Siguiente pregunta'; ?> </button>
</form>

<?php if( !isset( $this->session->userdata('current_match')->match_id ) ): ?>
    <a href="<?php echo base_url('game/cancel_match') ?>">Cancelar Partido</a>
<?php endif; ?>