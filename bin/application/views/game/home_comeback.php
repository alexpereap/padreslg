<section id="main-middle">
    <div class="home-content">
      <img src="images/lg-campaign-intro-logo.png" class="animated pulse" data-id='1'>
      <!--TITLES-->
      <!-- <hgroup class="info-messages animated growIn" data-id='2'> -->
      <hgroup class="info-messages text-center" data-id='2'>
        <h1 class="big">¡Bienvenido!</h1>
      </hgroup><!--/.info-messages-->

      <!--CONTENTS TO TEXT-->
      <!-- <article class="content-text text-center animated fadeIn"  data-id='3'> -->
      <article class="content-text text-center"  data-id='3'>
        <p><strong>Estamos en cierre de temporada, estaremos de vuelta a las 4:00am</strong></p>

      </article>
    </div>
      <div class="awards-expo">

          <div class="awards-wrapper">
            <img src="./images/awards/awards.png" alt="">
            <label for="uid01"><span class="btn-more awb-01"></span></label>
            <input class="toggle-info hidden" type="checkbox" name="" id="uid01">
            <div class="awards-text awt-01 tld">
              Nevera frost, 130lts, silver
            </div>
            <label for="uid02"><span class="btn-more awb-02"></span></label>
            <input class="toggle-info hidden" type="checkbox" name="" id="uid02">
            <div class="awards-text awt-02 tlu">
              lg ultra hd tv 49 9uf675t
            </div>
            <label for="uid03"><span class="btn-more awb-03"></span></label>
            <input class="toggle-info hidden" type="checkbox" name="" id="uid03">
            <div class="awards-text awt-03 trd">
              lg music flow hs7(las750m)
            </div>
            <label for="uid04"><span class="btn-more awb-04"></span></label>
            <input class="toggle-info hidden" type="checkbox" name="" id="uid04">
            <div class="awards-text awt-04 tlu">
              g gpad 8.0 lgv480
            </div>
            <label for="uid05"><span class="btn-more awb-05"></span></label>
            <input class="toggle-info hidden" type="checkbox" name="" id="uid05">
            <div class="awards-text awt-05 trd">
              lg music flow h3
            </div>
            <label for="uid06"><span class="btn-more awb-06"></span></label>
            <input class="toggle-info hidden" type="checkbox" name="" id="uid06">
            <div class="awards-text awt-06 tld">
              proyector hd portable ph300
            </div>
            <label for="uid07"><span class="btn-more awb-07"></span></label>
            <input class="toggle-info hidden" type="checkbox" name="" id="uid07">
            <div class="awards-text awt-07 tlu">
              lg g3 d855
            </div>
            <label for="uid08"><span class="btn-more awb-08"></span></label>
            <input class="toggle-info hidden" type="checkbox" name="" id="uid08">
            <div class="awards-text awt-08 tld">
              Tab-book 2, 1t540
            </div>
          </div>
          <div class="disclaimer-expo">
            *Imágenes de referencia, las caracteristicas y <br> especificaciones están sujetos a cambios sin previo aviso.
          </div>
        </div>
  </section>