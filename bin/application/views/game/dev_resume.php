<h2>Jugados</h2>

<?php foreach( $ended_matches as $r ): ?>
    <div>
        <p>Fecha : <?php echo $r->match_created_at ?></p>

        <?php if( $this->session->userdata('user_public')->id == $r->challenger_id ): ?>
            <p><?php echo $r->oponent_team_name . ' ' . $r->oponent_firstname . ' ' . $r->oponent_lastname  ?></p>
        <?php else: ?>
            <p><?php echo $r->challenger_team_name . ' ' . $r->challenger_firstname . ' ' . $r->challenger_lastname  ?></p>
        <?php endif; ?>

        <?php if( $this->session->userdata('user_public')->id == $r->winner_id ): ?>
            <p>Resultado: Ganaste.</p>
        <?php else: ?>
            <p>Resultado: Perdiste.</p>
        <?php endif; ?>

        <a href="<?php echo base_url('game/match_result/' . $r->match_id ) ?>">Ver detalles</a>
    </div>
<?php endforeach; ?>


<h2>Pendientes</h2>

<?php foreach( $pending_matches as $r ): ?>
    <div>
        <p>Fecha : <?php echo $r->match_created_at ?></p>
        <p><?php echo $r->oponent_team_name . ' ' . $r->oponent_firstname . ' ' . $r->oponent_lastname  ?></p>
    </div>

    <?php if( $r->match_declined == 'yes' ): ?>
        <p>Tu rival rechazo</p>
    <?php endif; ?>
<?php endforeach; ?>

<h2>Invitiaciones recibidas</h2>

<?php foreach( $received_matches as $r ): ?>
    <div>
        <p>Fecha : <?php echo $r->match_created_at ?></p>
        <p><?php echo $r->challenger_team_name . ' ' . $r->challenger_firstname . ' ' . $r->challenger_lastname  ?></p>

        <?php if( $r->match_declined == 'no' ): ?>
            <p><a href="<?php echo base_url('game/play_match/' . $r->match_id ) ?>">Jugar</a> <a href="<?php echo base_url('game/decline_match/' . $r->match_id ) ?> ">Rechazar</a> </p>
        <?php else: ?>
            <p>Has rechazado a tu rival</p>
        <?php endif; ?>
    </div>
<?php endforeach; ?>