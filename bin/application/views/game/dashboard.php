<section id="main-middle">

      <div class="row full-height toFluidWidth">
        <div class="row-same-height row-full-height" style="outline:1px solid red">
          <div class="col-xs-12 col-xs-height col-full-height" style="outline:1px solid yellow">

              <div id="profile-info" class="content to-the-center text-center off-set--5 ">
                <div class="row">
                  <!--ROW INFO-->
                  <?php if( $this->session->flashdata('cancel_msg') ): ?>

                    <div class="col-xs-12">
                      <p class="notification" ><?php echo $this->session->flashdata('cancel_msg'); ?> </p>
                    </div>
                  <?php endif; ?>
                  <div class="col-xs-12 col-md-6">

                    <div class="row">
                        <!--BADGE TEAM-->
                      <figure class="col-xs-2 col-md-2 badge-avatar">
                        <!--IMG SOURCE SAVED FROM THE BADGE CONSTRUCTOR-->
                          <img src="images/lg-team-badge-07.png">
                            <figcaption>
                              <!--DATA-ATTR RECIVED & SAVED FROM THE BADGE CONSTRUCTOR-->
                              <h3 style="font-size:850%; top:9%;">JD</h3>
                            </figcaption>
                      </figure><!--/.badge.avatar-->
                      <hgroup class="col-xs-10 col-md-10 inf-team">
                        <!--NAME TEAM-->
                        <h1>Equipo <span>JD Fútbol Club</span></h1>
                        <h4>Creador: Juan Maldonado</h4>
                      </hgroup>
                    </div><!--/.row-->
                    <br class="clear">
                    <div class="row">
                        <h3 class="subtiles">Puntuación:</h3>
                        <table class="col-md-12 tbl-lg-profile" border="0">
                          <thead class="toLastBlack">
                            <tr>
                              <th>PJ</th>
                              <th>PG</th>
                              <th>PC</th>
                              <th>PTS</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><?php echo $played_games ?></td>
                              <td><?php echo $won_games ?></td>
                              <td><?php echo $share_points ?>/35</td>
                              <td><?php echo $team_score ?></td>
                            </tr>
                          </tbody>
                        </table><!--/.tbl-lg-profile-->

                        <ul class="converters">
                          <li>PJ: Partidos jugados</li>
                          <li>PG: Partidos ganados</li>
                          <li>PC: Puntos por Compartir</li>
                          <li>PTS: Total de puntos</li>
                        </ul><!--/.converters-->

                        <h3 class="subtiles">Posiciones:</h3>
                        <table class="col-md-12 tbl-lg-profile" border="0">
                          <thead>
                            <tr>
                              <th>temporada 1</th>
                              <th>temporada 2</th>
                              <th>temporada 3</th>
                              <th>temporada 4</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><?php echo $seasons_summary->season_one ? $seasons_summary->season_one->rank : '--'?></td>
                              <td><?php echo $seasons_summary->season_two ? $seasons_summary->season_two->rank : '--'?></td>
                              <td><?php echo $seasons_summary->season_three ? $seasons_summary->season_three->rank : '--'?></td>
                              <td><?php echo $seasons_summary->season_four ? $seasons_summary->season_four->rank : '--'?></td>
                            </tr>
                          </tbody>
                        </table><!--/.tbl-lg-profile-->
                        <br/>
                        <table class="col-md-12 tbl-lg-profile" border="0">
                          <thead>
                            <tr>
                              <th>Aciertos</th>
                              <th>Mejor tiempo</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><?php echo $right_answers ?></td>
                              <td><?php echo $best_time / 1000 ?> segundos</td>
                            </tr>
                          </tbody>
                        </table><!--/.tbl-lg-profile-->
                    </div><!--/.row-->
                  </div><!--/.col-xs-12.col-md-6-->
                  <div class="col-xs-12 col-md-6">
                    <br/>
                    <h2 class="subTitle">
                      <span class="red-text">Temporada <?php echo $current_season->id ?></span>
                      <span class="pipeDvdr">|</span> Posición Actual:
                      <strong class="toBig"><?php echo $user_ranking ?></strong>
                    </h2><!--/.subTitle-->
                    <br/>
                      <article class="info-charts">
                        <h2 class="subTitle text-left">Estadísticas:</h2>
                        <figure class="fig-the-chart">
                          <div class="chart" data-percent="<?php echo intval( $won_games_percentage );?>"><?php echo intval($won_games_percentage) ?>%</div>
                          <figcaption>Partidos Ganados</figcaption>
                        </figure><!--/.fig-the-chart-->
                        <figure class="fig-the-chart">
                          <div class="chart" data-percent="<?php echo intval($right_answers_percent) ?>"><?php echo intval($right_answers_percent) ?>%</div>
                          <figcaption>Preguntas Acertadas</figcaption>
                        </figure><!--/.fig-the-chart-->
                        <figure class="fig-the-chart">
                          <div class="chart" data-percent="<?php echo number_format((float) ($average_time / 1000 ) , 2, '.', '')?>"><?php echo number_format((float) ($average_time / 1000 ) , 2, '.', '')?><span class="minTxt">segundos</span></div>
                          <figcaption>Tiempo promedio</figcaption>
                        </figure><!--/.fig-the-chart-->
                      </article><!--/.info-charts-->
                  </div><!--/.col-xs-12.col-md-6-->
                </div><!--/.row-->
              </div><!--/.content.to-the-center-->
              <br/>
              <!--ROW CTA´s-->
              <div class="row">
                <div class="col-xs-12 col-md-6">
                  <a class="lg-Btn trnstn" href="<?php echo base_url('game/begin_match') ?>" style="float:right">
                    <button class="red-CTA">Iniciar Partido
                  </button></a><!--/.lg-Btn.trnstn-->
                </div>
                <div class="col-xs-12 col-md-6">
                  <a class="lg-Btn trnstn" href="<?php echo site_url('game/resume') ?>">
                    <button class="red-CTA"> Partidos Pendientes
                  </button></a><!--/.lg-Btn.trnstn-->
                </div>
              </div><!--/.row-->

          </div><!--/.col-xs-12.col-xs-height.col-full-height-->
        </div><!--/.row-same-height.row-full-height-->
      </div><!--/.row.full-height-->
  </section><!--/#main-middle-->

<!-- ==================================================
  -CAMPAIGNS IMAGES BACK -->
  <section id="campaigns-elmnts">
      <div id="bg-add-left"></div>
      <!--0div id="bg-right"></div-->
  </section><!--/#campigns-elmnts-->