<section id="main-middle">
      <div class="row full-height toMaxWidth">
        <div class="row-same-height row-full-height">
          <div class="col-xs-12 col-xs-height col-full-height">

              <div class="content to-the-center text-center off-set--10">

                <!--TITLES-->
                <hgroup class="info-messages">
                  <h1 class="">Iniciar Partido</h1>
                  <p class="info">Instrucciones:</p>
                </hgroup><!--/.info-messages-->

                <!--CONTENTS TO TEXT-->
                <article class="content-instrctns text-center">

                  <div class="row rules-init-match">

                    <figure class="col-md-4 col-sm-6 col-xs-12 fig-instruction">
                      <figcaption>
                        <h3><span class="">01</span></h3>
                        <p>Juega contra un amigo o contra un equipo aleatorio.</p>
                      </figcaption>
                      <img src="images/game-icon-teams.png" />
                    </figure><!--/.fig-instruction-->

                    <figure class="col-md-4 col-sm-6 col-xs-12 fig-instruction">
                      <figcaption>
                        <h3><span class="">02</span></h3>
                        <p>Responde cuantas preguntas puedas en el menor tiempo posible.</p>
                      </figcaption>
                      <img src="images/game-icon-match.png" style="margin-top:-10px" />
                    </figure><!--/.fig-instruction-->

                    <figure class="col-md-4 col-sm-6 col-xs-12 fig-instruction">
                      <figcaption>
                        <h3><span class="">03</span></h3>
                        <p>Obtén puntos adicionales por compartir contenido de LG o por conocer más sobre sus productos.</p>
                      </figcaption>
                      <img src="images/game-icon-coins.png" style="margin-top:-20px" />
                    </figure><!--/.fig-instruction-->

                  </div><!--/.row.rules-init-match-->

                  <div class="row centered">
                    <p class="col-md-3 col-sm-6 col-xs-12 col-centered">
                    <?php if( $remaining_matches_with_friends > 0 ): ?>
                      <a class="lg-Btn trnstn" href="javascript:void(0);">
                        <button id="findFriendOponent" class="red-CTA">
                         &nbsp;&nbsp;&nbsp;&nbsp;Contra un amigo&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                      </a><!--/.lg-Btn.trnstn-->
                    <?php endif; ?>
                      <span class="info"><strong class="nums"> <?php echo $remaining_matches_with_friends ?> </strong> Partidos restantes contra amigos</span>
                    </p>
                    <p class="col-md-4 col-sm-6 col-xs-12 col-centered">
                    <?php if( $remaining_week_plays > 0 ): ?>
                     <a class="lg-Btn trnstn" href="javascript:void(0);">
                      <button id="findRandomOponent" type="button" class="red-CTA">
                        Contra un equipo aleatorio
                      </button></a>
                    <?php endif; ?><!--/.lg-Btn.trnstn-->
                      <span class="info"><strong class="nums"> <?php echo $remaining_week_plays ?> </strong> Partidos restantes contra equipos</span>
                    </p>
                  </div><!--/.row centered-->

                    <div id="againstFriend" style="display:none;" class="row centered oponent-window" >
                        <?php if( count( (array) $user_friends) > 0 ): ?>
                        <select name="friend_oponent" id="friendOponent">
                            <option value="" selected >Selecciona</option>
                            <?php foreach( (array) $user_friends  as $f): ?>
                                <option value="<?php echo $this->encrypt->encode( $f->id) ?>"><?php echo $f->name ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php else: ?>
                        <p>No tienes amigos disponibles para jugar, invita amigos!</p>
                        <?php endif; ?>
                     </div>

                    <div id="randomPlayer" style="display:none;" class="row centered oponent-window"  >
                        <!-- Content Filled by JS -->
                    </div>

                    <?php if( $remaining_week_plays > 0 ||  $remaining_matches_with_friends > 0 ): ?>
                    <div class="row centered">
                      <form id="submitMatch" method="POST" action="<?php echo base_url('game/start_a_match') ?>" >
                        <input type="hidden" id="myOponent" name="my_oponent" value="" >
                        <input type="hidden" name="oponent_type" id="oponentType" >
                        <a class="lg-Btn trnstn" href="javascript:void(0);">
                            <button type="submit" class="red-CTA" >Jugar!</button>
                        </a>
                    </form>
                  </div>
                <?php endif; ?>

                </article><!--/.content-instrctns-->

              </div><!--/.content.to-the-center-->

          </div><!--/.col-xs-12.col-xs-height.col-full-height-->
        </div><!--/.row-same-height.row-full-height-->
      </div><!--/.row.full-height-->
  </section><!--/#main-middle-->

<!-- ==================================================
  -CAMPAIGNS IMAGES BACK -->
  <section id="campaigns-elmnts">
      <!--div id="bg-add-left"></div-->
      <!--div id="bg-add-center"></div-->
      <!--div id="bg-right"></div-->
  </section><!--/#campigns-elmnts-->