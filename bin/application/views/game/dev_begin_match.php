<div style="margin-top:300px;" ></div>
<button id="findFriendOponent" >Contra un amigo</button>
<button id="findRandomOponent" >Contra equipo aleatorio</button>


<div id="againstFriend" style="display:none;" class="oponent-window" >
    <p>contra un amigo</p>
    <?php if( count( (array) $user_friends) > 0 ): ?>
    <select name="friend_oponent" id="friendOponent">
        <option value="" selected >Selecciona</option>
        <?php foreach( (array) $user_friends  as $f): ?>
            <option value="<?php echo $this->encrypt->encode( $f->id) ?>"><?php echo $f->name ?></option>
        <?php endforeach; ?>
    </select>
    <?php else: ?>
    <p>No tienes amigos disponibles para jugar, invita amigos!</p>
    <?php endif; ?>
</div>

<div id="randomPlayer" style="display:none;" class="oponent-window"  >

</div>

<form id="submitMatch" method="POST" action="<?php echo base_url('game/start_a_match') ?>" >
    <input type="hidden" id="myOponent" name="my_oponent" value="" >
    <input type="hidden" name="oponent_type" id="oponentType" >
    <button type="submit" >Play</button>
</form>

