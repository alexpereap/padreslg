<script>
    var match_tweaks = [];

    <?php foreach( $question_1 as $qa ):  if( $qa->right_answer == 'yes' ): ?>
    match_tweaks.push( CryptoJS.AES.encrypt( '<?php echo base64_encode( $qa->answer_id ) ?>' ,  secret_key ).toString() );
    <?php endif; endforeach; ?>

    <?php foreach( $question_2 as $qa ):  if( $qa->right_answer == 'yes' ): ?>
    match_tweaks.push( CryptoJS.AES.encrypt( '<?php echo base64_encode( $qa->answer_id ) ?>' ,  secret_key ).toString() );
    <?php endif; endforeach; ?>

    <?php foreach( $question_3 as $qa ):  if( $qa->right_answer == 'yes' ): ?>
    match_tweaks.push( CryptoJS.AES.encrypt( '<?php echo base64_encode( $qa->answer_id ) ?>' ,  secret_key ).toString() );
    <?php endif; endforeach; ?>

</script>
<section id="main-middle">
      <div class="row full-height toMaxWidth overflowed">
        <div class="row-same-height row-full-height elements-field-limit animatedParent" data-sequence='1200'>

            <div id="field-left-team" class="col-md-2 col-sm-2 col-xs-4 col-xs-height col-full-height col-middle animated bounceInLeft" data-id="1">
              <div class="content to-the-center text-center ">
                <aside class="asdTeamInfo">
                  <!--BADGE TEAM-->
                  <figure class="badge-avatar">
                    <!--IMG SOURCE SAVED FROM THE BADGE CONSTRUCTOR-->
                      <img src="images/<?php echo $team_on_left_data->shield_image ?>">
                        <figcaption>
                          <!--DATA-ATTR RECIVED & SAVED FROM THE BADGE CONSTRUCTOR-->
                          <h3 style="font-size:850%; top:9%;"><?php echo $team_on_left_data->name ?></h3>
                        </figcaption>
                        <!--NAME TEAM-->
                        <figcaption class="inf-team">
                          <h4>Equipo <span><?php echo $team_on_left_data->name ?></span></h4>
                          <h5><?php echo $team_on_left_data->entity ?></h5><hr>
                          <h6><?php echo $team_on_left_data->player_firstname . ' ' . $team_on_left_data->player_lastname ?></h6>
                          <h6><?php echo $team_on_left_data->dt_firstname . ' ' . $team_on_left_data->dt_lastname ?></h6>
                        </figcaption>
                  </figure><!--/.badge.avatar-->

                </aside><!--/.asdTeamInfo-->
              </div><!--/.content.to-the-center-->
            </div><!--/#field-left-team-->

            <div id="field-center-teams" class="col-md-8 col-sm-8 col-xs-12 col-xs-height col-full-height animated growIn" data-id="2">

                <div class="content to-the-center text-center off-set--15"><!--off-set-5-->

                  <!--TIMER-->
                  <hgroup class="timer-counter">
                    <h2 id="main-runner-counter"></h2>
                    <h2 class="back">88:88</h2>
                  </hgroup><!--/.timer-counter-->

                  <!--CONTENTS TO THE FIELD-->
                  <article class="content-field">

                    <section id="game-field" class="row row-same-height "><!-- showField -->
                        <div id="game-play-field" >
                          <!--CHECK BEFORE FOR THE 'LOCAL/left' OR 'VISIT/right' TEAM & ADD CLASS theCurrentTeam-->
                          <div id="team-left" class="figPlayTeam <?php if( $this->session->userdata('current_match')->challenger == 'yes' ) echo 'theCurrentTeam' ?> " <?php if( false ): ?>style="left:62%;top:24%"<?php endif ?>></div><!--/#team-left.figPlayTeam-->
                            <hr id="trckPthLft" class="trckPth" <?php if( false ): ?>style="width:68%;" <?php endif; ?> >
                          <div id="team-right" class="figPlayTeam <?php if( $this->session->userdata('current_match')->challenger == 'no' ) echo 'theCurrentTeam' ?> "  ></div><!--/#team-right.figPlayTeam-->
                            <hr id="trckPthRgt" class="trckPth"  >
                        </div><!--/#game-play-field-->
                        <!--CONTENTS TRIVIAL-->
                        <div class="col-xs-height col-middle to80Width">
                          <!--INIT WITH A TRIVIAL ITEMS LISTS-->
                          <article id="trivial-items">
                            <nav class="trivial-slices">

                              <li id="triviaQuestion1" class="listTrivia triviaQuestion actvTrivia" data-result-qtn="false">
                                <div class="content">
                                  <h1><span class="num" data-current-trivial='1'>01</span>
                                    <?php echo $question_1[0]->title ?></h1>
                                    <ul class="trivial-questions">
                                      <?php foreach( $question_1  as $qa): ?>
                                      <li><a question-id="<?php echo $qa->id ?>" answer-id="<?php echo $qa->answer_id ?>" href="#"><?php echo $qa->answer_description ?></a></li>
                                      <?php endforeach ?>
                                    </ul>
                                    <!-- <a class="lg-Btn trnstn triviaCTA" href="#">
                                      <button class="red-CTA">
                                        Siguiente >>
                                      </button></a> --><!--/.lg-Btn.trnstn-->
                                </div><!--/.content-->
                              </li><!--/.listTrivia-->

                              <li id="triviaQuestion2" class="listTrivia triviaQuestion" data-result-qtn="true">
                                <div class="content">
                                  <h1><span class="num" data-current-trivial='2'>02</span>
                                    <?php echo $question_2[0]->title ?></h1>
                                    <ul class="trivial-questions">
                                      <?php foreach( $question_2  as $qa): ?>
                                      <li><a question-id="<?php echo $qa->id ?>" answer-id="<?php echo $qa->answer_id ?>" href="#"><?php echo $qa->answer_description ?></a></li>
                                      <?php endforeach ?>
                                    </ul>
                                    <!-- <a class="lg-Btn trnstn" href="#">
                                      <button class="red-CTA triviaCTA">
                                        Siguiente >>
                                      </button></a> --><!--/.lg-Btn.trnstn-->
                                </div><!--/.content-->
                              </li><!--/.listTrivia-->

                              <li id="triviaQuestion3" class="listTrivia triviaQuestion" data-result-qtn="false">
                                <div class="content">
                                  <h1><span class="num" data-current-trivial='3'>03</span>
                                    <?php echo $question_3[0]->title ?></h1>
                                    <ul class="trivial-questions">
                                      <?php foreach( $question_3  as $qa): ?>
                                      <li><a question-id="<?php echo $qa->id ?>" answer-id="<?php echo $qa->answer_id ?>" href="#"><?php echo $qa->answer_description ?></a></li>
                                      <?php endforeach ?>
                                    </ul>
                                    <!-- <a class="lg-Btn trnstn triviaCTA" href="#">
                                      <button class="red-CTA">
                                        Siguiente >>
                                      </button></a> --><!--/.lg-Btn.trnstn-->
                                </div><!--/.content-->
                              </li><!--/.listTrivia-->

                              <li id="trivial-responses" class="listTrivia">
                                <div id="trivial-question-result" class="content">
                                  <h1 id="" class="success-trivial-response">Felicitaciones has respondido correctamente <strong><span class="right-answer-count">1</span>/3</strong> preguntas</h1>
                                  <h1 id="" class="error-trivial-response">Lo sentimos no has acertado la pregunta <strong><span class="right-answer-count">2</span>/3</strong> preguntas</h1>
                                </div><!--/#trivial-question-result.content-->
                                <div id="trivial-end-result" class="content">
                                  <hgroup class="titles-trivial">
                                      <h1 id="congratzTxt" ><?php if( $this->session->userdata('current_match')->challenger == 'yes' ): ?>¡Felicidades! <?php endif; ?> </h1>
                                      <h3>Respondiste acertadamente <span><span class="right-answer-count">0</span> de 3</span> preguntas.</h3>
                                      <p id="congratzResume" ><?php if( $this->session->userdata('current_match')->challenger == 'yes' ): ?> Ya contestaste tus preguntas, ahora debes esperar a que el otro equipo responda para saber el resultado. <strong>Buena suerte.</strong> <?php endif; ?> </p>
                                    </hgroup><!--/.titles-trivial-->
                                  <!--FINAL COUNTER RESUME-->
                                  <hgroup class="timer-counter">
                                    <!--FINAL TIMER-->
                                    <h2 id="result-runner-counter">12.07</h2>
                                    <h2 class="back">88:88</h2>
                                  </hgroup><!--/.timer-counter-->
                                  <hr class="dvdrTrvlRslt">
                                  <hgroup class="titles-trivial last">
                                      <h3>¡Gana un punto extra!</h3>
                                      <p>Comparte en tus redes sociales lo que LG tiene para tí</p>
                                    </hgroup><!--/.titles-trivial-->
                                    <a id="getSharePoint" class="lg-Btn trnstn" href="<?php echo base_url('game/get_share_point') ?>">
                                      <button class="red-CTA">
                                        ¡Quiero el punto!
                                      </button></a><!--/.lg-Btn.trnstn-->
                                </div><!--/#trivial-question-result.content-->
                              </li><!--/#trivial-responses.listTrivia-->

                            </nav><!--/.trivial-slices-->
                          </article><!--/#trivial-items-->
                        </div>
                    </section><!--/.row.rules-init-match-->

                  <!--RULER STATS-->
                    <div class="row centered">
                      <nav class="ruller-status">
                        <li class="activeTrivial"><span>1</span></li>
                        <li><span>2</span></li>
                        <li><span>3</span></li>
                      </nav>
                    </div><!--/.row centered-->

                  </article><!--/.content-field-->

                </div><!--/.content.to-the-center-->

            </div><!--/.col-xs-8.col-sm-8.col-xs-12.col-xs-height.col-full-height-->

            <div id="field-right-team" class="col-md-2 col-sm-2 col-xs-4 col-xs-height col-full-height col-middle animated bounceInRight" data-id="1">
              <div class="content to-the-center text-center ">
                <aside class="asdTeamInfo">

                  <figure class="badge-avatar">
                    <!--IMG SOURCE SAVED FROM THE BADGE CONSTRUCTOR-->
                      <img src="images/<?php echo $team_on_right_data->shield_image ?>">
                        <figcaption>
                          <!--DATA-ATTR RECIVED & SAVED FROM THE BADGE CONSTRUCTOR-->
                          <h3 style="font-size:850%; top:8%;"><?php echo $team_on_right_data->name ?></h3>
                        </figcaption>
                        <!--NAME TEAM-->
                        <figcaption class="inf-team">
                          <h4>Equipo <span><?php echo $team_on_right_data->name ?></span></h4>
                          <h5><?php echo $team_on_right_data->entity ?></h5><hr>
                          <h6><?php echo $team_on_right_data->player_firstname . ' ' . $team_on_right_data->player_lastname ?></h6>
                          <h6><?php echo $team_on_right_data->dt_firstname . ' ' . $team_on_right_data->dt_lastname ?></h6>
                        </figcaption>
                  </figure><!--/#badge-constructor-->

                </aside><!--/.asdTeamInfo-->
              </div><!--/.content.to-the-center-->
            </div><!--/#field-right-team-->

        </div><!--/.row-same-height.row-full-height-->
      </div><!--/.row.full-height-->
  </section><!--/#main-middle-->

<!-- ==================================================
  -CAMPAIGNS IMAGES BACK -->
  <section id="campaigns-elmnts">
      <!--div id="bg-add-left"></div-->
      <!--div id="bg-add-center"></div-->
      <!--div id="bg-right"></div-->
  </section><!--/#campigns-elmnts-->