<section id="main-middle">
      <div class="slider-content">
        <div class="slider-header">
          <div class="title">Premios:</div>
        </div>
        <ul id="slider-tabs" class="slider-tabs">
          <li class="active"><span>Temporada 1</span></li><!-- 
           --><li><span>Temporada 2</span></li><!-- 
           --><li><span>Temporada 3</span></li><!-- 
           --><li><span>Temporada 4</span></li>
        </ul>
        <div class="slider-disclaimer">
          *Imágenes de referencia, las caracteristicas y <br> especificaciones están sujetos a cambios sin previo aviso.
        </div>
        <div class="slider-container">
          <div id="temp1" class="temp temp1 active">
            <div class="img-temp">
              <div class="img-wrapper">
                <img src="images/awards/temp-01.png" alt="">
                <label for="uid01"><span class="btn-more btn-aux-1"></span></label>
                <input class="toggle-info hidden" type="checkbox" name="" id="uid01">
                <div class="img-text text-aux-1">
                  <div class="title">g gpad 8.0 lgv480</div>
                  <div class="item-text">* Agarre con una sola mano pantalla IPS de 8".</div>
                </div>
                <label for="uid02"><span class="btn-more btn-aux-2"></span></label>
                <input class="toggle-info hidden" type="checkbox" name="" id="uid02">
                <div class="img-text text-aux-2">
                  <div class="title">lg music flow h3</div>
                  <div class="item-text">* Función Multi-Room</div>
                  <div class="item-text">* Transmisión de Audio sobre red Wi-Fi.</div>
                </div>
              </div>
            </div>
          </div>
          <div id="temp2" class="temp temp2 hidden">
            <div class="img-temp">
              <div class="img-wrapper">
                <img src="images/awards/temp-02.png" alt="">
                <label for="uid03"><span class="btn-more btn-aux-1"></span></label>
                <input class="toggle-info hidden" type="checkbox" name="" id="uid03">
                <div class="img-text text-aux-1">
                  <div class="title">lg music flow hs7(las750m)</div>
                  <div class="item-text">* Auto Music PLAY by RANGE-OF FLOW <sup>TM</sup>.</div>
                  <div class="item-text">* Home Cinema Mode.</div>
                </div>
                <label for="uid04"><span class="btn-more btn-aux-2"></span></label>
                <input class="toggle-info hidden" type="checkbox" name="" id="uid04">
                <div class="img-text text-aux-2">
                  <div class="title">tab-book 2, 1t540</div>
                  <div class="item-text">* Deslice autómatico con un toque.</div>
                  <div class="item-text">* Ultraliviano, pesa menos de 1Kg.</div>
                </div>
              </div>
            </div>
          </div>
          <div id="temp3" class="temp temp3 hidden">
            <div class="img-temp">
              <div class="img-wrapper">
                <img src="images/awards/temp-03.png" alt="">
                <label for="uid05"><span class="btn-more btn-aux-1"></span></label>
                <input class="toggle-info hidden" type="checkbox" name="" id="uid05">
                <div class="img-text text-aux-1">
                  <div class="title">lg ultra hd tv 49 9uf675t</div>
                  <div class="item-text">* Diseño ultra delgado.</div>
                  <div class="item-text">* Sonido ultra envolvente.</div>
                </div>
                <label for="uid06"><span class="btn-more btn-aux-2"></span></label>
                <input class="toggle-info hidden" type="checkbox" name="" id="uid06">
                <div class="img-text text-aux-2">
                  <div class="title">lg music flow hs7(las750m)</div>
                  <div class="item-text">* Auto Music PLAY by RANGE-OF FLOW <sup>TM</sup>.</div>
                  <div class="item-text">* Home Cinema Mode.</div>
                </div>
              </div>
            </div>
          </div>
          <div id="temp4" class="temp temp4 hidden">
            <div class="img-temp">
              <div class="img-wrapper">
                <img src="images/awards/temp-04.png" alt="">
                <label for="uid07"><span class="btn-more btn-aux-1"></span></label>
                <input class="toggle-info hidden" type="checkbox" name="" id="uid07">
                <div class="img-text text-aux-1">
                  <div class="title">proyector hd portable ph300</div>
                  <div class="item-text">* Batería recargable - hasta 2,5 horas.</div>
                  <div class="item-text">* Movie, Music, Photo. Archivos de Office por USB</div>
                </div>
                <label for="uid08"><span class="btn-more btn-aux-2"></span></label>
                <input class="toggle-info hidden" type="checkbox" name="" id="uid08">
                <div class="img-text text-aux-2">
                  <div class="title">Nevera frost, 130lts, silver</div>
                  <div class="item-text">* Sistema de control mecánico.</div>
                  <div class="item-text">* Bandeja de hielo.</div>
                </div>
                <label for="uid09"><span class="btn-more btn-aux-3"></span></label>
                <input class="toggle-info hidden" type="checkbox" name="" id="uid09">
                <div class="img-text text-aux-3">
                  <div class="title">lg g3 d855</div>
                  <div class="item-text">* Diseño metálico.</div>
                  <div class="item-text">* Cámara OIS + avanzada.</div>
                </div>
              </div>
            </div>
          </div>
          <div class="group-arrow">
            <span class="prev"></span>
            <span class="next"></span>
          </div>
        </div>
        <article class="conts-text">
          <h1>Mecánica:</h1>
          <ul class="lists-asds">
            <li><p><span class="num">01</span>Ingresa tus datos personales junto con los de tu Papá.</p></li>
            <li><p><span class="num">02</span>Arma tu equipo y personalízalo seleccionando un escudo.</p></li>
            <li><p><span class="num">03</span>Responde cuantas preguntas puedas en el menor tiempo posible.</p></li>
            <li><p><span class="num">04</span>Reta a tus amigos y demuestra quién es el maestro futbolero.</p></li>
            <li><p><span class="num">05</span>Habrá 3 ligas regulares con premios increíbles y la 4 será la gran final con el premio mayor de LG.</p></li>
            <a class="lg-Btn trnstn" href="#">
              <button class="red-CTA">
                Ver Reglas
              </button></a><!--/.lg-Btn.trnstn-->
          </ul>
        </article>
      </div>
  </section><!--/#main-middle-->

<!-- ================================================== 
  -CAMPAIGNS IMAGES BACK -->
  <section id="campaigns-elmnts">
      <div id="bg-add-left"></div>
      <!--0div id="bg-right"></div-->
  </section><!--/#campigns-elmnts-->