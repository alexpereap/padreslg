<section id="main-middle">
    <div class="table-content">
      <div class="table-header">
        <div class="title">Posiciones:</div>
        <div class="caption">
          <span>PJ: Partidos jugados</span>
          <span>PG: Partidos ganados</span>
        </div>
      </div>
      <div class="table-container">
        <div class="title">Top 5:</div>
        <div class="table-responsive">
          <table class="table-info">
            <thead>
              <tr>
                <th>#</th>
                <th class="text-left">Equipo</th>
                <th>PJ</th>
                <th>PG</th>
                <th>Aciertos</th>
                <th>Mejor tiempo</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach( $rank_data as $r ): ?>
              <tr>
                <td><?php echo $r->rank ?></td>
                <td class="text-left"><?php echo $r->team->name . ' ' . $r->team->entity ?></td>
                <td><?php echo $r->played_games ?></td>
                <td><?php echo $r->won_games  ?></td>
                <td><?php echo $r->right_answers ?></td>
                <td><?php echo number_format((float) ($r->best_time / 1000 ) , 2, '.', '')?></td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <div class="subtitle">Tu posición:</div>
        <div class="table-responsive">
          <table class="table-info focus">
            <tbody>
              <tr>
                <td><?php echo $user_ranking ?></td>
                <td class="text-left"><?php echo $team->name . ' ' . $team->entity ?></td>
                <td><?php echo $played_games  ?></td>
                <td><?php echo $won_games  ?></td>
                <td><?php echo $right_answers  ?></td>
                <td><?php echo number_format((float) ($best_time / 1000 ) , 2, '.', '')?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section><!--/#main-middle-->

<!-- ==================================================
  -CAMPAIGNS IMAGES BACK -->
  <section id="campaigns-elmnts">
  </section><!--/#campigns-elmnts-->