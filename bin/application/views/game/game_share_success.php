<section id="main-middle">
      <div class="row full-height toMaxWidth">
        <div class="row-same-height row-full-height">
          <div class="col-xs-12 col-xs-height col-full-height">

              <div class="content to-the-center text-center off-set--10">
                <!--TITLES-->
                <hgroup class="info-messages">
                  <h1>¡Felicidades!</h1>
                  <p class="info" style="margin-top:15px;">Ganaste un punto adicional por compartir el contenido que <strong >LG</strong> tiene para ti.</p>
                </hgroup>
                <!--FIGURE TO SHARE-->
                <article class="content-text text-center">
                  <p style="font-size:18px; max-width:400px; margin:0 auto 20px;">Continúa jugando y acumulando puntos para ser uno de los ganadores de esta temporada.</p><br/>
                </article><!--/.content-text.text-center-->
                <div class="row" style="max-width:550px; margin: 0 auto;">
                  <legend class="info-matchs-leg col-md-6 col-sm-12 col-xs-12">
                    <span><?php echo $remaining_matches_with_friends ?></span><br/>
                    Partidos restantes contra amigos
                  </legend>
                  <legend class="info-matchs-leg col-md-6 col-sm-12 col-xs-12">
                    <span><?php echo $remaining_week_plays ?></span><br/>
                    Partidos restantes contra equipos aleatorios
                  </legend>
                </div>
                <br/>
                <!--CTAS BUTTONS-->
                <div class="row" style="max-width: 840px; margin: 0 auto">
                  <div class="col-md-4 col-sm-12 col-xs-12">
                    <a class="lg-Btn trnstn" href="<?php echo base_url('game/begin_match') ?>">
                    <button class="red-CTA">
                      Iniciar Partido
                    </button></a><!--/.lg-Btn.trnstn-->
                  </div><!--/.form-group-->
                  <div class="col-md-4 col-sm-12 col-xs-12">
                    <a class="lg-Btn trnstn" href="<?php echo base_url('game/resume') ?>">
                    <button class="red-CTA">
                      Resumen de Partidos
                    </button></a><!--/.lg-Btn.trnstn-->
                  </div><!--/.form-group-->
                  <div class="col-md-4 col-sm-12 col-xs-12">
                    <a class="lg-Btn trnstn" href="<?php echo base_url('game/ranking') ?>">
                    <button class="red-CTA">
                      Posiciones
                    </button></a><!--/.lg-Btn.trnstn-->
                  </div><!--/.form-group-->
                </div>
              </div><!--/.content.to-the-center-->

          </div><!--/.col-xs-12.col-xs-height.col-full-height-->
        </div><!--/.row-same-height.row-full-height-->
      </div><!--/.row.full-height-->
  </section><!--/#main-middle-->

<!-- ================================================== 
  -CAMPAIGNS IMAGES BACK -->
  <section id="campaigns-elmnts">
      <div id="bg-add-left"></div>
      <!--0div id="bg-right"></div-->
  </section><!--/#campigns-elmnts-->