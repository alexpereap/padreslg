<!-- ==================================================
  -MAIN CONTAINER SECTION: -->
  <section id="main-middle">
      <div class="row full-height">
        <div class="row-same-height row-full-height">
          <div class="col-xs-12 col-xs-height col-full-height">

              <div class="content to-the-center text-center off-set--12">

                <nav class="step-lg-navi">
                  <ul>
                    <li><span>01</span></li>
                    <li><span>02</span></li>
                    <li class="current"><span>03</span></li>
                  </ul>
                </nav><!--/.step-lg-form-->
                <!--TITLES-->
                <hgroup class="info-messages">
                  <h1>Selecciona el escudo de tu equipo:</h1>
                  <!--h4></h4-->
                  <p class="info">Escoge uno de los escudos a continuación y personaliza tu equipo para ser el maestro futbolero.</p>
                </hgroup><!--/.info-messages-->
                <!--BADGES STEPPER-->
                  <article class="badges-step row row-centered">
                    <!--ASIDES BADGES-->
                    <aside id="teamBuilderShields" class="badges-aside col-md-6 col-sm-6 col-xs-12 col-centered">
                      <ul class="thumbs-badges" data-NameTeam="<?php echo $initials ?>">
                      <?php foreach( $shield_designs as $d ): ?>
                        <li shield-id="<?php echo $d->id ?>" data-sizeTxt="<?php echo $d->size_text ?>" data-PosY="<?php echo $d->pos_y ?>" onclick="ga('send', 'event', 'Escudo-Equipo', 'click', '/Botón-Escudo<?php echo $d->id ?>');">
                          <img src="images/<?php echo $d->image ?>">
                        </li>
                      <?php endforeach; ?>
                      </ul>
                    </aside><!--/.badges-aside-->
                    <aside class="badges-aside col-md-6 col-sm-6 col-xs-12 col-centered">
                      <figure id="badge-constructor">
                        <img src="images/lg-team-badge-clear.png">
                        <figcaption><!--NAME TEAM-->
                          <h1></h1></ficaption>
                      </figure><!--/#badge-constructor-->
                    </aside><!--/.badges-aside-->
                  </article><!--/.badges-step.row.row-centered-->

                  <!--INIT THE FORM-->
                  <form class="row frmLg mid-width-frm row" id="teamBuildForm" method="POST" action="<?php echo base_url('landing/handle_team_build') ?>" >
                  <input type="hidden" id="teamEntity" name="team_entity" value="FC - Futbol Club" >
                  <input type="hidden" id="teamShieldId" name="team_shield_id" >
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <label class="toDrop dropBadgesSelection">
                        <nav id="teamBuilderEntityNav" class="dropDownfrm">
                          <a class="chssd" href="#">FC - Futbol Club</a>
                          <ul>
                            <li><a href="#">FC - Futbol Club</a></li>
                            <li><a href="#">CD - Club Deportivo</a></li>
                            <li><a href="#">AT - Atletico</a></li>
                            <li><a href="#">IND - Independiente</a></li>
                            <li><a href="#">AS - Associazione Sportiva</a></li>
                            <li><a href="#">AC - Associazione Calcio </a></li>
                          </ul>
                        </nav>
                      </fieldset><!--/.form-group-->
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                        <input type="submit" class="inpt-cta big cta-create" value="Crear equipo" onclick="ga('send', 'event', 'Escudo-Equipo', 'click', '/Botón-Crear-Equipo');">
                      </fieldset><!--/.form-group-->
                  </form><!--/.frmLg.mid-width-frm-->

              </div><!--/.content.to-the-center-->

          </div><!--/.col-xs-12.col-xs-height.col-full-height-->
        </div><!--/.row-same-height.row-full-height-->
      </div><!--/.row.full-height-->
  </section><!--/#main-middle-->

<!-- ==================================================
  -CAMPAIGNS IMAGES BACK -->
  <section id="campaigns-elmnts">
      <!--div id="bg-add-left"></div-->
      <div id="bg-right"></div>
  </section><!--/#campigns-elmnts-->