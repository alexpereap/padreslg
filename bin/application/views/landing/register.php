<section id="main-middle">
      <div class="row full-height">
        <div class="row-same-height row-full-height">
          <div class="col-xs-12 col-xs-height col-full-height">

              <div class="content to-the-center text-center off-set-5">

                <nav class="step-lg-navi">
                  <ul>
                    <li class="current"><span>01</span></li>
                    <li><span>02</span></li>
                    <li><span>03</span></li>
                  </ul>
                </nav><!--/.step-lg-form-->
                <!--TITLES-->
                <hgroup class="info-messages">
                  <h1>Ingresa tus datos:</h1>
                  <!--h4></h4-->
                  <p class="info">Completa el siguiente formulario y participa.</p>
                </hgroup><!--/.info-messages-->
                <?php if( isset($fb_login_url) ): ?>
                  <a class="lg-Btn trnstn" href="<?php echo $fb_login_url; ?>" onclick="ga('send', 'event', 'Ingresa tus Datos', 'click', '/Botón-Conectate-Con-Facebook');">
                    <button class="red-CTA fbk-icon">
                      Conéctate con Facebook
                    </button>
                  </a><!--/.lg-Btn.trnstn-->
                <?php endif; ?>
                <?php if( $this->session->flashdata('auth_error') ): ?><p><?php echo $this->session->flashdata('auth_error'); ?></p><?php endif; ?>
                <!--INIT THE FORM-->
                  <form id="registerForm" action="<?php echo base_url('landing/preset_user_data') ?>" method="POST" class="row frmLg mid-width-frm">
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <label class="trnstn">
                        <input name="firstname" <?php if( isset($disabled_fields) ) echo 'disabled' ?> type="text" class="form-control" placeholder="Nombres" value="<?php echo isset( $user_data['firstname'] ) ? $user_data['firstname'] : '' ?>" ></label>
                      </fieldset><!--/.form-group-->
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <label class="trnstn">
                        <input name="lastname" <?php if( isset($disabled_fields) ) echo 'disabled' ?> type="text" value="<?php echo isset( $user_data['lastname'] ) ? $user_data['lastname'] : '' ?>" class="form-control" placeholder="Apellidos"></label>
                      </fieldset><!--/.form-group-->
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <label class="trnstn">
                        <input name="birthdate" <?php if( isset($disabled_fields) ) echo 'disabled' ?> type="text" class="form-control" placeholder="Fecha de nacimiento:" onfocus="(this.type='date')" onblur="(this.type='text')" value="<?php echo isset( $user_data['birthdate'] ) ? $user_data['birthdate'] : '' ?>" >
                      </label>
                      </fieldset><!--/.form-group-->
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <label class="trnstn">
                        <input name="email" <?php if( isset($disabled_fields) ) echo 'disabled' ?> type="email" value="<?php echo isset( $user_data['email'] ) ? $user_data['email'] : '' ?>" class="form-control" placeholder="Email"></label>
                      </fieldset><!--/.form-group-->
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <label class="toDrop">
                        <nav  class="dropDownfrm">
                          <a class="chssd" href="#">Cédula de ciudadania</a>
                          <ul>
                            <li><a href="#">Cédula de ciudadania</a></li>
                            <li><a href="#">Cédula de extranjeria</a></li>
                            <li><a href="#">Pasaporte</a></li>
                          </ul>
                        </nav>
                        <input type="hidden" value="Cédula de ciudadania" name="nid_type" id="nidType" >
                      </fieldset><!--/.form-group-->
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <label class="trnstn">
                        <input name="nid" <?php if( isset($disabled_fields) ) echo 'disabled' ?>  type="text" value="" class="form-control" placeholder="N° de Documento"></label>
                      </fieldset><!--/.form-group-->
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <label class="trnstn">
                        <input name="address" <?php if( isset($disabled_fields) ) echo 'disabled' ?>  type="text" value="" class="form-control" placeholder="Dirección"></label>
                      </fieldset><!--/.form-group-->
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <label class="trnstn">
                        <input name="city" <?php if( isset($disabled_fields) ) echo 'disabled' ?>  type="text" value="" class="form-control" placeholder="Ciudad"></label>
                      </fieldset><!--/.form-group-->
                     <fieldset class="row centered col-centered form-group">
                      <label class="trnstn col-md-6 col-sm-6 col-xs-12 ">
                        <input name="cellphone" <?php if( isset($disabled_fields) ) echo 'disabled' ?>  type="text" value="" class="form-control" placeholder="Celular"></label>
                        <span class="rqrdMssg">*Todos los campos son obligatorios</span>
                      </fieldset><!--/.form-group-->
                    <fieldset class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <?php if( ! isset($disabled_fields) ):  ?>
                          <input type="submit" class="inpt-cta" value="Siguiente" onclick="ga('send', 'event', 'Ingresa tus Datos', 'click', '/Botón-Siguiente');">
                      <?php endif; ?>
                    </fieldset><!--/.form-group-->
                  </form><!--/.frmLg.mid-width-frm-->

              </div><!--/.content.to-the-center-->

          </div><!--/.col-xs-12.col-xs-height.col-full-height-->
        </div><!--/.row-same-height.row-full-height-->
      </div><!--/.row.full-height-->
  </section>

  <section id="campaigns-elmnts">
      
      <div id="bg-right"></div>
  </section><!--/#campigns-elmnts-->