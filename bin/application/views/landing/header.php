<!DOCTYPE html>
  <head>
    <base href="<?php echo base_url() ?>">

    <meta property="og:site_name" content="Papá Juega Mejor En Casa Con LG"/>
    <meta property="og:title" content="Papá Juega Mejor En Casa Con LG" />
    <meta property="og:url" content="<?php echo base_url() ?>" />
    <meta property="og:description" content="Pre-inscríbete con tu Papá en el torneo de fútbol LG, y descubre quién es el maestro futbolero. Acuérdate que al pre-inscribirte ganas inmediatamente 3 puntos y 1 de los 90 kits que regala LG" />
    <meta property="fb:app_id" content="519810394841305" />
    <meta property="og:image" content="<?php echo base_url('images/lg-campaign-headlogo.png') ?>" />

    <!-- Meta, title, CSS, favicons, etc. -->
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Pre-inscríbete con tu Papá en el torneo de fútbol LG, y descubre quién es el maestro futbolero. Acuérdate que al pre-inscribirte ganas inmediatamente 3 puntos y 1 de los 90 kits que regala LG">
    <meta name="keywords" content="">
    <meta name="author" content="LG, PadresLG">
    <title>Papá Juega Mejor En Casa Con LG</title>
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
  <!-- Style Reset -->
  <link rel="stylesheet" href="css/animations.css">
  <link rel="stylesheet" href="css/jquery.sidr.light.css">
  <link rel="stylesheet" href="css/styles.css?v=<?php   echo time() ?>">
  <link rel="stylesheet" href="css/style_override.css?v=<?php   echo time() ?>">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Favicons -->
  <!--link rel="apple-touch-icon" href="/apple-touch-icon.png"-->
  <link rel="stylesheet" href="<?php echo base_url();?>vendor/bootstrapvalidator-dist-0.5.1/dist/css/bootstrapValidator.css">
  <link rel="icon" href="favicon.ico">

</head>
<!-- INIT LANDING -->
<body id="lg-dads" class="<?php if( isset($in_home) ): ?>is-home<?php endif; ?> no-head-stats">
<div id="fb-root"></div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64209110-1', 'auto');
  ga('send', 'pageview');

</script>
<script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '519810394841305',
          xfbml      : true,
          version    : 'v2.3'
      });
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<!-- ==================================================
  -MAIN HEADER: -->

    <header id="head-dads" class="animatedParent" data-sequence='2500'>
        <a id="lg-campaign-headlogo" class="animated pulse" data-id='1'>
          <img src="images/lg-campaign-headlogo.png" /></a><!--/#lg-campaign-headlogo-->
        <a id="lg-main-logo" href="http://lg.com" onclick="ga('send', 'event', 'Header', 'click', '/Botón-Logo-LG');" target="_BLANK" class="animated fadeIn" data-id='2'>
          <img src="images/lg-right-logo.png"></a><!--/#lg-main-logo-->
        <div id="lg-campaign-boxed" class="container animated fadeInDown" data-id='3'>
          <div class="row">
              <article id="blckbox-campaign">
                <!--NO HEAD STATS TO THIS VIEW-->
              </article>
          </div><!--/.row-->
        </div><!--/.container-->
    </header><!--/#head-dads"-->