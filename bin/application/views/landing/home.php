<!-- ================================================== 
  -MAIN CONTAINER SECTION: -->
  <section id="main-middle">
      <div class="row full-height">
        <div class="row-same-height row-full-height">
          <div class="col-xs-12 col-xs-height col-full-height">

              <div class="content to-the-center text-center off-set-10 animatedParent" data-sequence='950'>
                <img class="hidden-xs" src="images/lg-campaign-intro-logo.png" class="main-intro-logo animated pulse" data-id='1'>
                <!--TITLES-->
                <hgroup class="info-messages animated growIn" data-id='2'>
                  <h1 class="big">¡Bienvenido!</h1>
                </hgroup><!--/.info-messages-->

                <!--CONTENTS TO TEXT-->
                <article class="content-text text-center animated fadeIn"  data-id='3'>
                  <p><strong>Pre-inscríbete</strong> con tu Papá en el <strong>torneo de fútbol </strong><span class="red-text">LG</span>, y descubre quién es el maestro futbolero.</p>
                  <p>Acuérdate que al pre-inscribirte ganas inmediatamente<strong> 3 puntos </strong> y 1 de los <strong>90 kits</strong> que regala <spann class="red-text">LG</span></p>
                  <a class="lg-Btn trnstn" href="<?php echo base_url('landing/register'); ?>" onclick="ga('send', 'event', 'Home', 'click', '/Botón-Regístrate');">
                    <button class="red-CTA">
                      Regístrate
                    </button></a><!--/.lg-Btn.trnstn-->
                </article><!--/.content-text.text-center-->

              </div><!--/.content.to-the-center-->

          </div><!--/.col-xs-12.col-xs-height.col-full-height-->
        </div><!--/.row-same-height.row-full-height-->
      </div><!--/.row.full-height-->
  </section><!--/#main-middle-->

<!-- ================================================== 
  -CAMPAIGNS IMAGES BACK -->
  <section id="campaigns-elmnts">
      <!--div id="bg-add-left"></div-->
      <div id="bg-add-left">
        <img src="images/img-prize-no-legals.png">
        <span class="legals-message" style="max-width:350px; margin:0 0 0 5%;">*Imágenes de referencia, las características y especificaciones están sujetos a cambios sin previo aviso.</span>
      </div>
      <!--div id="bg-right"></div-->
  </section><!--/#campigns-elmnts-->