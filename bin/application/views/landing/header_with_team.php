<!DOCTYPE html>
  <head>
    <base href="<?php echo base_url() ?>">

    <meta property="og:site_name" content="Papá Juega Mejor En Casa Con LG"/>
    <meta property="og:title" content="Papá Juega Mejor En Casa Con LG" />
    <meta property="og:url" content="<?php echo base_url() ?>" />
    <meta property="og:description" content="Pre-inscríbete con tu Papá en el torneo de fútbol LG, y descubre quién es el maestro futbolero." />
    <meta property="fb:app_id" content="519810394841305" />
    <meta property="og:image" content="<?php echo base_url('images/lg-campaign-headlogo.png') ?>" />

    <!-- Meta, title, CSS, favicons, etc. -->
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Pre-inscribete torneo de fútbol LG, y descubre quién es el maestro futbolero.">
    <meta name="keywords" content="">
    <meta name="author" content="LG - PadresLG">

    <title>Papá Juega Mejor En Casa Con LG</title>
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
  <!-- Style Reset -->
  <link rel="stylesheet" href="css/animations.css">
  <link rel="stylesheet" href="css/jquery.sidr.light.css">
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/style_override.css">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Favicons -->
  <!--link rel="apple-touch-icon" href="/apple-touch-icon.png"-->
  <link rel="stylesheet" href="<?php echo base_url();?>vendor/bootstrapvalidator-dist-0.5.1/dist/css/bootstrapValidator.css">
  <link rel="icon" href="favicon.ico">

</head>
<!-- INIT LANDING -->
<body id="lg-dads" class="">
<div id="fb-root"></div>
<script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '519810394841305',
          xfbml      : true,
          version    : 'v2.3'
      });
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64209110-1', 'auto');
  ga('send', 'pageview');

</script>

<header id="head-dads" class="animatedParent" data-sequence='200'>
        <a id="lg-campaign-headlogo" class="animated pulse" data-id='1'>
          <img src="images/lg-campaign-headlogo.png" /></a><!--/#lg-campaign-headlogo-->
        <a id="lg-main-logo" href="#" class="animated fadeIn" data-id='2'>
          <img src="images/lg-right-logo.png"><img src="images/lg-right-logo-blank.png"></a><!--/#lg-main-logo-->
        <div id="lg-campaign-boxed" class="animated fadeInDown" data-id='3'>
          <div class="row">
              <article id="blckbox-campaign">
                <div class="col-md-2 col-sm-4 col-xs-12 spacerBlnk"></div>
                <div class="col-md-3 col-sm-6 col-xs-12 avatars">
                  <div id="teamBadgehead">
                      <figure class="badge-avatar">
                          <!--IMG SOURCE SAVED FROM THE BADGE CONSTRUCTOR-->
                          <img id="headTeamShield" <?php if( !isset($team) ): ?> style="display:none;" <?php endif; ?> src="images/<?php echo isset($team) ? $team->shield_image : ''  ?>">
                        <figcaption>
                          <!--DATA-ATTR RECIVED & SAVED FROM THE BADGE CONSTRUCTOR-->
                          <h3 style="font-size:650%; margin-top:15px;"><?php echo $initials ?></h3>
                        </figcaption>
                      </figure><!--/.badge-avatar-->
                    <h4 class="full-title-team">
                      Equipo <?php echo $initials ?> - <span id="headerTeamEntity" ><?php echo isset( $team ) ?  $team->entity : 'FC - Futbol Club';  ?></span>
                      </h4><!--/.full-title-team-->
                    <figure class="avatar-net first-avatar">
                      <div class="picNet">
                        <img src="http://graph.facebook.com/<?php echo $this->session->userdata('user_public')->facebook_id ?>/picture?width=25&height=25"/></div>
                      <figcaption>
                        <span><?php echo $this->session->userdata('user_public')->firstname . ' ' . $this->session->userdata('user_public')->lastname  ?></span></figcaption>
                    </figure><!--/.avatar-net-->
                    <figure class="avatar-net">
                      <div class="picNet">
                        <img src="timthumb.php?src=<?php echo site_url('uploads/' . $this->session->userdata('user_public')->father_photo ) ?>&w=25&h=25&q=100&zc=1"/></div>
                      <figcaption>
                        <span><?php echo $this->session->userdata('user_public')->father_firstname . ' ' . $this->session->userdata('user_public')->father_lastname  ?></span></figcaption>
                    </figure><!--/.avatar-net-->
                  </div><!--/#teamBadgehead-->
                </div><!--/.avatars-->
                <div class="col-md-6 col-sm-12 col-xs-12 wrapActions">
                  <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 ctas-menu">
                        <nav class="navsMain">
                          <li><a class="disabled">Iniciar Partido</a></li>
                          <li><a class="disabled">Posiciones</a></li>
                          <li><a class="disabled">Partidos Pendientes</a></li>
                          <li><a class="disabled">Editar Perfíl/Escudos</a></li>
                          <li><a class="disabled">Mecánica y Premios</a></li>
                          <li><a href="<?php echo site_url('landing/logout') ?>" class="enabled">Salir</a></li>
                        </nav>
                    </div><!--/.ctas-menu-->
                    <div class="col-md-5 col-sm-4 col-xs-12 counter">
                      <span class="counter-title">Puntos acumulados:</span>
                      <hgroup id="counter-points">
                          <h1><?php echo isset($team) ? $team->score : 0 ?></h1>
                        </hgroup><!--/.counter-title-->
                    </div><!--/.counter-->
                  </div><!--/.row-->
                </div><!--/.wrapActions-->
                <div class="col-md-2 col-sm-12 col-xs-12"></div>
              </article><!--/#blckbox-campaign-->
          </div><!--/.row-->
        </div><!--/.container-->
    </header>