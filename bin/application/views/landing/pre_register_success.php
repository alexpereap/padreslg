<section id="main-middle">
      <div class="row full-height">
        <div class="row-same-height row-full-height">
          <div class="col-xs-12 col-xs-height col-full-height">

              <div class="content to-the-center text-center off-set-5">

                <!--TITLES-->
                <hgroup class="info-messages">
                  <h1 class="big">¡Felicidades!</h1>
                  <h4>Creaste tu equipo con éxito.</h4>
                </hgroup><!--/.info-messages-->

                <!--CONTENTS TO TEXT-->
                <article class="content-text text-center">
                  <p><strong>Ya estás inscrito con tu papá en el torneo de fútbol LG,</strong> que empieza el <span class="red-text">29 de junio de 2015</span>, en donde se pondrá a prueba tu conocimiento futbolero y podrás competir para regalarle todo lo que siempre quiso.</p>
                  <p class="black-bg">&nbsp;&nbsp;Desde ya cuentas con 3 puntos por haberte pre-inscrito.&nbsp;&nbsp;</p>
                  <?php if( $points_by_share === false ): ?>
                      <p><strong>Sigue sumando, compartiendo el post con tus amigos:</strong></p>
                      <a class="lg-Btn trnstn" >
                        <button id="landingShareTrigger" type="button" class="red-CTA fbk-icon" onclick="ga('send', 'event', 'Inscripción', 'click', '/Botón-Compartir');">
                          Compartir
                        </button></a><!--/.lg-Btn.trnstn-->
                      <p style="margin-top:10px;">y gana: <span class="red-text underline">1 punto extra.</span></p>
                  <?php else: ?>
                    <p><strong>Ya has compartido y ganaste 1 punto extra!</strong></p>
                  <?php endif; ?>

                </article>

              </div><!--/.content.to-the-center-->

          </div><!--/.col-xs-12.col-xs-height.col-full-height-->
        </div><!--/.row-same-height.row-full-height-->
      </div><!--/.row.full-height-->
  </section><!--/#main-middle-->

<!-- ==================================================
  -CAMPAIGNS IMAGES BACK -->
  <section class="hidden-xs" id="campaigns-elmnts">
      <!--div id="bg-add-left"></div-->
      <div id="bg-add-left">
        <img  src="images/img-prize-no-legals.png">
        <span class="legals-message" style="max-width:350px; margin:0 0 0 5%;">*Imágenes de referencia, las características y especificaciones están sujetos a cambios sin previo aviso.</span>
      </div>
      <!--div id="bg-right"></div-->
  </section><!--/#campigns-elmnts-->