<!--MAIN CONTAINER SECTION: -->
  <section id="main-middle">
      <div class="row full-height">
        <div class="row-same-height row-full-height">
          <div class="col-xs-12 col-xs-height col-full-height">

              <div class="content to-the-center text-center off-set-5">

                <nav class="step-lg-navi">
                  <ul>
                    <li><span>01</span></li>
                    <li class="current"><span>02</span></li>
                    <li><span>03</span></li>
                  </ul>
                </nav><!--/.step-lg-form-->
                <!--TITLES-->
                <hgroup class="info-messages">
                  <h1>Agrega a tu papá:</h1>
                  <!--h4></h4-->
                  <p class="info">Arma tu equipo y prepárate para ganar premios increíbles.</p>
                </hgroup><!--/.info-messages-->
                <!--INIT THE FORM-->
                  <form method="POST" enctype="multipart/form-data" id="fatherForm" action="<?php echo base_url('landing/handle_register') ?>" class="row frmLg large-width-frm">
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <label class="trnstn">
                        <input name="father_firstname" type="text" class="form-control" placeholder="Nombres"></label>
                      </fieldset><!--/.form-group-->
                    <fieldset class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <label class="trnstn">
                        <input name="father_lastname" type="text" class="form-control" placeholder="Apellidos"></label>
                      </fieldset><!--/.form-group-->
                    <fieldset class="col-md-12 col-sm-12 col-xs-12 form-group">
                        <input type="button" class="inpt-cta" id="addFatherPhoto" value="Agrega su Foto" onclick="ga('send', 'event', 'Agrega a Tu Papá', 'click', '/Botón-Agrega-Su-Foto');">
                        <p id="fatherPhotoFeedback" class="father-photo-feddback" ></p>
                        <?php if(  $this->session->flashdata('error') ): ?>
                          <p class="father-photo-feddback"><?php echo $this->session->flashdata('error'); ?></p>
                        <?php endif; ?>
                        <input type="file" name="father_photo" style="display:none;" id="fatherPhoto">
                      </fieldset><!--/.form-group-->
                    <fieldset class="row centered col-centered form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                          &nbsp;&nbsp;&nbsp;&nbsp;<input id="checkbox1" type="checkbox" name="subscribed"  >
                          <label for="checkbox1" class="ckbname termsCheckLeft">Acepto los Términos y condiciones.</label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                          <input id="checkbox2" type="checkbox" name="terms" >
                          <label for="checkbox2" class="ckbname">Acepto recibir información de productos y Promociones.</label>
                        </div>
                      </fieldset><!--/.row centered col-centered-->
                    <fieldset class="col-md-12 col-sm-12 col-xs-12 form-group"><br/>
                      <input type="submit" class="inpt-cta big" value="Regístrate" onclick="ga('send', 'event', 'Agrega a Tu Papá', 'click', '/Botón-Regístrate');">
                    </fieldset><!--/.form-group-->
                  </form><!--/.frmLg.mid-width-frm-->

              </div><!--/.content.to-the-center-->

          </div><!--/.col-xs-12.col-xs-height.col-full-height-->
        </div><!--/.row-same-height.row-full-height-->
      </div><!--/.row.full-height-->
  </section><!--/#main-middle-->

  <section id="campaigns-elmnts">
      <div id="bg-right"></div>
  </section><!--/#campigns-elmnts-->