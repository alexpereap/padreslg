<!-- ================================================== 
  -FOOTER MAIN: -->
  <footer id="footLgDads">
    <div class="container">
       <p class="copy">Copyright © 2015 LG Electronics. Todos los derechos reservados | <a href="<?php echo base_url('terms.pdf') ?>" onclick="ga('send', 'event', 'Footer', 'click', '/Botón-Terminos-y-Condiciones');">Términos y Condiciones</a>.<a href="https://www.facebook.com/LGColombia?fref=ts"  target="_BLANK" class="iconNet" title="Buscános en Facebook" onclick="ga('send', 'event', 'Footer', 'click', '/Botón-Conoce-Facebook');"><img src="images/icon-facebook.png"></a><a href="https://twitter.com/soylg" target="_BLANK" class="iconNet" onclick="ga('send', 'event', 'Footer', 'click', '/Botón-Conoce-Twitter');"><img src="images/icon-twitter.png" title="Síguenos en Twitter"></a></p>
    </div><!--/.container-->
  </footer><!--/#footLgDads-->

<!-- ================================================== 
  -OVERLAY MOBILE -->

<?php if( isset($require_landscape) ): ?>
<section id="mobile-overlay">
  <div class="mobile-message"></div>
  <div class="tablet-message"></div>
</section>
<script>
  var slider_required = true;
</script>
<?php endif; ?>

<!--SCRIPTS-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <!--script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script-->
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.backstretch.min.js"></script>
  <script src="js/jquery.css3-animate-it.js"></script>
  <script src="js/jquery.sidr.min.js"></script>
  <script src="js/jquery.touchwipe.min.js"></script>
  <script src="js/scripts.js"></script>
  <script src="<?php echo base_url();?>vendor/bootstrapvalidator-dist-0.5.1/dist/js/bootstrapValidator.min.js" ></script>
  <script type="text/javascript" src="<?php echo base_url();?>vendor/bootstrapvalidator-dist-0.5.1/dist/js/language/es_ES.js"></script>
  <script> var site_uri = '<?php echo base_url() ?>'; </script>
  <script src="js/site.js?v=<?php echo time() ?>" ></script>
</body>
</html>