<p>
    <a href="<?php echo base_url('cms/new_question') ?>" class="btn btn-primary">Nueva pregunta</a>
</p>

<?php if( $this->session->flashdata('add_question_success') ): ?>

<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <?php echo $this->session->flashdata('add_question_success'); ?>
</div>

<?php endif; ?>

<div>
    <?php echo $output; ?>
</div>