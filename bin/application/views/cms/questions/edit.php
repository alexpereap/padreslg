<form class="form-horizontal" method="POST" action="<?php echo base_url('cms/update_question') ?>"  >
<input type="hidden" name="question_id" value="<?php echo $question[0]->id ?>" >
<fieldset>

<!-- Form Name -->
<legend>Preguntas</legend>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="question">Pregunta</label>
  <div class="col-md-6">
    <textarea required class="form-control" id="question" name="question" rows="5"><?php echo $question[0]->title ?></textarea>
  </div>
</div>

<?php foreach( $question as $key => $q ): ?>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="a1">Respuesta <?php echo $key + 1 ?></label>
  <div class="col-md-6">
  <input id="a1" name="answers[<?php echo $q->answer_id ?>]" type="text" placeholder="" class="form-control input-md" value="<?php echo $q->answer_description ?>" required>

  </div>
</div>
<?php endforeach; ?>


<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="right_answer">Respuesta correcta</label>
  <div class="col-md-4">
    <select id="right_answer" required name="right_answer" class="form-control">
      <option value="">Seleccione</option>
      <?php foreach( $question as $key => $q ): ?>
        <option <?php if( $q->right_answer == 'yes' ) echo 'selected'; ?>  value="<?php echo $q->answer_id ?>">Respuesta <?php echo $key + 1 ?></option>
      <?php endforeach; ?>
    </select>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-8">
    <button  type ="submit" id="" name="" class="btn btn-success">Guardar</button>
  </div>
</div>

</fieldset>
</form>
