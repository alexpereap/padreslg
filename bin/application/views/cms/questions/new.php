<form class="form-horizontal" method="POST" action="<?php echo base_url('cms/add_question') ?>"  >
<fieldset>

<!-- Form Name -->
<legend>Preguntas</legend>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="question">Pregunta</label>
  <div class="col-md-6">
    <textarea required class="form-control" id="question" name="question" rows="5"></textarea>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="a1">Respuesta 1</label>
  <div class="col-md-6">
  <input id="a1" name="answers[]" type="text" placeholder="" class="form-control input-md" required>

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="a2">Respuesta 2</label>
  <div class="col-md-6">
  <input id="a2" name="answers[]" type="text" placeholder="" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="a3">Respuesta 3</label>
  <div class="col-md-6">
  <input id="a3" name="answers[]" type="text" placeholder="" class="form-control input-md" required="">

  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="right_answer">Respuesta correcta</label>
  <div class="col-md-4">
    <select id="right_answer" required name="right_answer" class="form-control">
      <option value="">Seleccione</option>
      <option value="0">Respuesta 1</option>
      <option value="1">Respuesta 2</option>
      <option value="2">Respuesta 3</option>
    </select>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-8">
    <button type="submit" id="" name="" class="btn btn-success">Guardar</button>
  </div>
</div>

</fieldset>
</form>
