<?php

Class Cms extends CI_Controller{

    function __construct(){
        parent::__construct();

        if( !$this->session->userdata('user_admin') &&  $this->router->method != 'login' && $this->router->method != 'check_login_credentials' && $this->router->method != 'test_encrypt' ){

            redirect( 'cms/login' );
        }

         $this->load->library('grocery_CRUD');
    }

    public function index(){

        $this->questions();
    }

    public function login(){

        if( $this->session->userdata('user_admin') ){
            redirect('cms');
        }

        $this->load->view('cms/login');
    }

    public function check_login_credentials(){

        $result = $this->main->logInCmsUser( $_POST );
        echo json_encode( array('result' => $result) );
    }

    public function test_encrypt(){
        echo $this->encrypt->encode('Wunderman2015');
    }

    public function questions(){


        $this->grocery_crud->set_table('tbl_question');

        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_read();
        $this->grocery_crud->unset_edit();

        $this->grocery_crud->add_action('Editar', 'assets/grocery_crud/themes/flexigrid/css/images/edit.png', 'cms/edit_question');

        $output = $this->grocery_crud->render();

        $output->in_questions = true;

        $this->load->view("cms/opener", $output);
        $this->load->view('cms/questions/questions_list');
        $this->load->view("cms/closure");
    }

    public function new_question(){

        $data = array(
            'in_questions' => true
        );

        $this->load->view("cms/opener", $data);
        $this->load->view("cms/questions/new");
        $this->load->view("cms/closure");
    }

    public function add_question(){

        // add question
        $data_question = array(
            'title'      => $this->input->post('question'),
            'created_at' => date('Y-m-d H:i:s')
        );

        $question_id = $this->main->addQuestion( $data_question );

        // add question answers
        $question_answers = array();

        foreach( $this->input->post('answers') as $key => $a ){

            $key == $this->input->post('right_answer') ? $right_answer = 'yes' : $right_answer = 'no';

            $data_answers[] = array(
                'fk_question_id' => $question_id,
                'description'    => $a,
                'order'          => $key,
                'right_answer'   => $right_answer
            );
        }


        $this->main->addQuestionAnswers( $data_answers );

        $this->session->set_flashdata('add_question_success', 'Se ha añadido la pregunta');
        redirect(base_url( 'cms/questions' ));

    }

    public function edit_question( $id ){

        $data = array(
            'in_questions' => true,
            'question'     => $this->main->getQuestionDetail( $id )
        );


        $this->load->view("cms/opener", $data);
        $this->load->view("cms/questions/edit");
        $this->load->view("cms/closure");
    }

    public function update_question(){

        // updates question
        $this->main->updateQuestion( $this->input->post('question_id'), array('title' => $this->input->post('question')) );

        // updates question answers
        foreach( $this->input->post('answers') as $key => $a ){

            $key == $this->input->post('right_answer') ? $right_answer = 'yes' : $right_answer = 'no';

            $data_answers[] = array(
                'id'             => $key,
                'description'    => $a,
                'right_answer'   => $right_answer
            );
        }

        // die(krumo($data_answers));

        $this->main->updateQuestionAnswers( $data_answers );

        $this->session->set_flashdata('add_question_success', 'Se ha actualziado la pregunta');
        redirect(base_url( 'cms/questions' ));
    }

    public function logout(){
        $this->session->unset_userdata('user_admin');
        redirect( base_url('cms') );
    }
}