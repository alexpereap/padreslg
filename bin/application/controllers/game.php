<?php

require __DIR__ . '/../libraries/facebook-php-sdk-v4-4.0-dev/autoload.php';



use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;


session_start();

class Game extends CI_Controller {

    private $login_permissions = array('user_friends', 'email', 'user_birthday' );
    private $app_id = '519810394841305';
    private $app_secret = '57d26bb6ff778b0a6961b5ff6a98966a';

    private $allowed_matches_per_day = 90;
    private $allowed_friends_match_per_day = 5;

    // allowed controllers/routes to handle the current_match session
    private $allowed_current_match_routes = array(
        'handle_match_answer',
        'match',
        'parse_match_result',
        'start_a_match',
        'handle_interrumpted_match',
        'ajax_get_answer',
        'cancel_match',
        'ajax_get_answers',
        'logout'
    );

    function __construct(){
        parent::__construct();

        if( $this->session->userdata('current_match') ){

            if( !in_array($this->router->method, $this->allowed_current_match_routes) ){

                redirect( base_url('game/handle_interrumpted_match') );
            }
        }

        $current_season = $this->main->getCurrentSeason();

        if( count( $current_season ) == 0 && $this->router->method != 'comeback' ){
            redirect('game/comeback');
        }
    }

    public function index(){

        if( $this->session->userdata('user_public') ){
            redirect('game/dashboard');
        }

        $data = array(
            'in_home' => true,
            'fb_login_url' => $this->_get_fb_login_url()
        );

        $this->load->view('game/header', $data);
        $this->load->view('game/home');
        $this->load->view('game/footer');
    }

    public function login(){

        $data = array();

        FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper( base_url('game/login') );

        try {
            $session = $helper->getSessionFromRedirect();
        } catch( FacebookRequestException $ex ) {
            // When Facebook returns an error
        } catch( Exception $ex ) {
            // When validation fails or other local issues
        }

        // see if we have a session - in order to preload $data
        if ( isset( $session ) ) {

            try{

                // graph api request for user data
                $request = new FacebookRequest( $session, 'GET', '/me' );
                $response = $request->execute();

                // get response
                $graphObject = $response->getGraphObject();
                $fb_id = $graphObject->getProperty( 'id' );

                $user = $this->main->getUser( array('facebook_id' => $fb_id) );

                if( $user ){


                    $team = $this->main->getTeam( array('fk_user_id' => $user->id) );

                    if( $team ){
                        // TODO: if team exists
                        $this->_register_session( $user->id );
                        redirect( base_url('game/dashboard') );
                        return;

                    } else {

                        // Continues with team creation
                        $this->_register_session( $user->id );
                        redirect( base_url('landing/team_builder') );
                        return;
                    }

                    return;
                } else {
                    // register process in landing page
                    redirect( $this->_get_register_fb_login_url() );
                    return;
                }

                $accessToken = $session->getAccessToken();
                $longLivedAccessToken = $accessToken->extend();

                // user data prefill

                $user_data = array(
                    'firstname'      => $graphObject->getProperty( 'first_name' ),
                    'lastname'       => $graphObject->getProperty( 'last_name' ),
                    'birthdate'      => date('Y-m-d', strtotime($graphObject->getProperty( 'birthday' ))),
                    'split_birhdate' => explode('-', date('Y-m-d', strtotime($graphObject->getProperty( 'birthday' )))),
                    'email'          => $graphObject->getProperty( 'email' )
                );

                $data['user_data'] = $user_data;


            } catch( FacebookRequestException $ex ) {
                      // When Facebook returns an error
                      echo "fb<br>";
                      echo "<pre>";
                      print_r($ex);
                      echo "</pre>";
            } catch( Exception $ex ) {
                      // When validation fails or other local issues
                      echo "local</br>";
                      var_dump($ex);
            }

            // temp data with facebook data
            $this->session->set_userdata( 'temp_fb_data', array(
                'facebook_id'               => $fb_id,
                'facebook_long_lived_token' => (string) $longLivedAccessToken
            ));
        } else {

            redirect( site_url('game') );
        }
    }

    public function team_builder(){

        if( $_SERVER['REQUEST_METHOD'] != 'POST' ){
            // take me out of here
            redirect( base_url('game') );
        }

        $this->load->model('model_files');

        $this->session->set_userdata('temp_register_data', $_POST);

        if(  $_FILES['father_photo']['name'] != '' ){


            $rf = $this->model_files->upload_file('father_photo', './uploads', 'jpg|jpeg|png' );

            if( $rf['success'] === true ){

                $file_name = $rf['data']['file_name'];

            } else {

                krumo($rf['error']);

                // redirect( base_url() . 'site/set_purchase_data?error=' . $rf['error'] );
                // krumo($rf['error']);

               /* $this->session->set_flashdata('success_title', 'Ha ocurrido un problema');
                $this->session->set_flashdata('success', 'Lo sentimos, no hemos podido procesar tu imagen.<br/><ul><li>Verifica que tu imagen sea de formato .jpg, .jpeg o .png.</li><li>Guardala tu imagen en alguno de estos formatos y verifica que no pese más de 7MB.</li> Intenta subir tu reto de nuevo haciendo click <a href="' . base_url('site/engagement_challenge') . '" >aquí</a>');
                redirect(base_url('site/engagement_challenge'));*/
            }

        } else {

            /*$this->session->set_flashdata('success_title', 'Ha ocurrido un error');
            $this->session->set_flashdata('success', 'Intenta subir tu reto de nuevo haciendo click <a href="' . base_url('site/engagement_challenge') . '" >aquí</a>');
            redirect(base_url('site/engagement_challenge'));*/
        }

        $intials = substr($this->input->post('firstname'), 0,1) . substr($this->input->post('father_lastname'), 0,1);

        $data = array(
            'initials'        => $intials,
            'shield_desisngs' => $this->main->getShieldDesigns(),
            'points'          => 0,
            'dad_photo'       => $file_name
        );

        krumo($data);

        $this->load->view('game/header', $data);
        $this->load->view('game/team_builder');
        $this->load->view('game/footer');
    }

    public function handle_team_build(){

        $user_data = array();
        $father_data = array();

        // register user
        $form_data = $this->session->userdata('temp_register_data');


        $father_data = array(
            'firstname'  => $form_data['father_firstname'],
            'lastname'   => $form_data['father_lastname'],
            'photo'      => $this->input->post('dad_photo'),
            'created_at' => date('Y-m-d H:i:s')
        );

        $user_data = array(
            'firstname'                 => $form_data['firstname'],
            'lastname'                  => $form_data['lastname'],
            'birthdate'                 => $form_data['birthdate'],
            'email'                     => $form_data['email'],
            'nid_type'                  => $form_data['nid_type'],
            'nid'                       => $form_data['nid'],
            'address'                   => $form_data['address'],
            'city'                      => $form_data['city'],
            'cellphone'                 => $form_data['cellphone'],
            'facebook_id'               => $this->session->userdata('temp_fb_data')['facebook_id'],
            'facebook_long_lived_token' => $this->session->userdata('temp_fb_data')['facebook_long_lived_token'],
            'created_at'                => date('Y-m-d H:i:s')
        );


        $user_id = $this->main->saveUser($user_data);

        $father_data['fk_user_id'] = $user_id;
        $father_id = $this->main->saveFather( $father_data );

        $this->_register_session( $user_id );


        $team_data = array(
            'fk_user_id'           => $user_id,
            'fk_shield_designs_id' => $this->input->post('shield_design'),
            'name'                 => $this->input->post('name'),
            'created_at'           => date('Y-m-d H:i:s')
        );


        $this->main->saveTeam( $team_data );

        // mail
        $this->load->model('model_mails');

        $mail_body = $this->load->view('emails/pre-register',null,true);
        $this->model_mails->singleEmail( $this->session->userdata('user_public')->email, $mail_body, 'Ya estás pre-inscrito!' );

        // redirect to pre register success
        redirect( base_url('game/dashboard') );
    }

    public function dashboard(){

        $this->_require_user_public_session();

        $user_seasons_ranking = $this->main->getUserSeasonsRanking( $this->session->userdata('user_public')->id );
        $current_season = $this->main->getCurrentSeason();

        // ALL statistical data are for the current season
        $data = array(
            'remaining_week_plays'           => $this->_get_remaining_week_plays(),
            'remaining_matches_with_friends' => $this->_get_remaining_matches_with_friends(),
            'won_games'                      => $this->main->getUserWonGames( $this->session->userdata('user_public')->id ),
            'played_games'                   => $this->main->getUserPlayedGames( $this->session->userdata('user_public')->id ),
            'team_score'                     => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,
            'team'                           => $this->main->getTeam( array('fk_user_id' => $this->session->userdata('user_public')->id )),
            'current_season'                 => $current_season,
            'seasons_summary'                => $user_seasons_ranking,
            'user_ranking'                   => $this->getUserRankingForSeason( $current_season->id, $this->session->userdata('user_public')->id  ),
            'share_points'                   => $this->main->getTeamScoreQuantityByType( $this->session->userdata('user_public')->id, 'match-share' )->quantity,
            'right_answers'                  => $this->main->getUserRightAnswers( $this->session->userdata('user_public')->id )->quantity,
            'best_time'                      => $this->main->getUserBestTime( $this->session->userdata('user_public')->id )->time,
            'average_time'                   => $this->main->getUserAverageTime( $this->session->userdata('user_public')->id )->average_time,
            'right_answers_percent'          => $this->main->getUserRightAnswersPercent( $this->session->userdata('user_public')->id )->right_answer_percent,
            'pending_matches'                => $this->main->getUserPendingMatches( $this->session->userdata('user_public')->id ),
            'received_matches'               => $this->main->getUserReceivedMatches( $this->session->userdata('user_public')->id ),
        );

        $data['won_games_percentage'] = $data['played_games'] > 0 ? ($data['won_games'] * 100) / $data['played_games'] : 0;



        if( isset(  $_GET['dev'] ) ){
            krumo($data);
            krumo($this->session->userdata('user_public'));
            krumo( $this->session->userdata('current_match') );
            die();
        }

        $this->load->view('game/header_with_team', $data);
        $this->load->view('game/dashboard');
        $this->load->view('game/footer');
    }

    private function getUserRankingForSeason( $season_number, $user_id ){
        $user_seasons_ranking = $this->main->getUserSeasonsRanking( $this->session->userdata('user_public')->id );

        switch( $season_number ){

            case '1':
                $user_ranking = $user_seasons_ranking->season_one ? $user_seasons_ranking->season_one->rank : '--';
                break;

            case '2':
                $user_ranking = $user_seasons_ranking->season_two ? $user_seasons_ranking->season_two->rank : '--';
                break;

            case '3':
                $user_ranking = $user_seasons_ranking->season_three ? $user_seasons_ranking->season_three->rank : '--';
                break;

            case '4':
                $user_ranking = $user_seasons_ranking->season_four ? $user_seasons_ranking->season_four->rank : '--';
                break;
        }

        return $user_ranking;
    }

    private function _get_fb_login_url(){
        FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper( base_url('game/login') );
        return $helper->getLoginUrl( $this->login_permissions );
    }

    private function _register_session($user_id){

        $user = $this->main->getUser( array('id' => $user_id) );
        $this->session->set_userdata( 'user_public', $user );
    }

    public function logout(){
        $this->session->unset_userdata('user_public');
        $this->session->unset_userdata('current_match');
        redirect( base_url('game') );
    }

    private function _require_user_public_session(){

        if( $this->session->userdata('user_public') === false ){
            redirect('game');
            return;
        } else {

            // if the user hadn't created the team... redirects to team crations
            $team = $this->main->getTeam( array('fk_user_id' => $this->session->userdata('user_public')->id) );

            if( $team === false ){
                redirect( base_url('landing/team_builder') );
            }
        }
    }

    public function begin_match(){

        $this->_require_user_public_session();

        // gets facebook friends
        $user_friends = $this->_get_user_fb_friends( $this->session->userdata('user_public')->facebook_id );

        $data = array(
            'remaining_week_plays'           => $this->_get_remaining_week_plays(),
            'remaining_matches_with_friends' => $this->_get_remaining_matches_with_friends(),
            'user_friends'  => $this->_filter_fb_friends( $user_friends ),
            'team_score'    => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,
            'team'          => $this->main->getTeam( array('fk_user_id' => $this->session->userdata('user_public')->id )),
            'team_score'                     => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,
            'pending_matches'                => $this->main->getUserPendingMatches( $this->session->userdata('user_public')->id ),
            'received_matches'               => $this->main->getUserReceivedMatches( $this->session->userdata('user_public')->id ),
        );

        $this->load->view('game/header_with_team', $data);
        $this->load->view('game/begin_match');
        $this->load->view('game/footer');
    }

    // gets an user fb registred app friends
    private function _get_user_fb_friends( $fb_id ){

        $token = $this->session->userdata('user_public')->facebook_long_lived_token;
        $url = 'https://graph.facebook.com/v2.3/' . $fb_id . '/friends?access_token=' . $token . '&pretty=1&limit=5000';

        // php 5.6 security tweak
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"      =>false,
                "verify_peer_name" =>false,
            ),
        );

        $grap_object = json_decode(file_get_contents( $url , false, stream_context_create($arrContextOptions)));

        return $grap_object->data;

    }

    public function start_a_match(){

        $this->_require_user_public_session();

        if( $_SERVER['REQUEST_METHOD'] != 'POST' ){
            // take me out of here ?
            die();
        }

        if( $this->input->post('oponent_type') == 'friend' ){
            $fb_id = $this->encrypt->decode( $this->input->post('my_oponent') );
            $user = $this->main->getUser( array('facebook_id' => $fb_id) );
            $friend_match = 'yes';
        }

        if( $this->input->post('oponent_type') == 'random' ){

            $user_id = $this->encrypt->decode( $this->input->post('my_oponent') );
            $user = $this->main->getUser( array('id' => $user_id) );
            $friend_match = 'no';

        }

        if( $friend_match == 'yes' ){
            // check your number ofr friends matchs
            if( $this->main->getUserMatchCountWithFriendsToday( $this->session->userdata('user_public')->id ) >= $this->allowed_friends_match_per_day ){
                die('YA has jugado 10 veces hoy con amigos, selecciona otro jugador');
            }
        }


            $this->session->set_flashdata('current_match', 'ok');
            $this->session->set_userdata( 'current_match', (object) array(
                'question_number'        => 1,
                'oponent'                => $user->id,
                'oponent_team'           => $this->main->getTeam( array('fk_user_id' => $user->id) ),
                'challenger'             => 'yes',
                'answered_questions_ids' => array(),
                'answers_data'           => array(),
                'is_friend_match'        => $friend_match
                ));

            // saves the match intent
            $this->main->registerMatchIntent( $this->session->userdata('user_public')->id );

            redirect( base_url('game/match') );

    }

    // this serves as an intermediary for selecting the match question,
    // since the user can't reload the page in order to run away from the match
    public function handle_match_answer(){
        $this->_require_user_public_session();

        $current_match = $this->session->userdata('current_match');

        $match_update = array(
            'question_number'        => 3,
            'oponent'                => $current_match->oponent,
            'oponent_team'           => $current_match->oponent_team,
            'challenger'             => $current_match->challenger,
            'is_friend_match'        => $current_match->is_friend_match
        );

        $match_update['answered_questions_ids'] = array();
        $match_update['answers_data'] = array();


        if( $this->input->post('answers') ) {
            foreach( (array) $this->input->post('answers')  as $a ){
                $match_update['answered_questions_ids'][] = $a['answer_id'];

                $match_update['answers_data'][] = array(

                    'question_id' => $a['question_id'],
                    'time'        => $a['time'],
                    'answer_id'   => $a['answer_id']
                );

            }
        }

        if( isset( $current_match->match_id ) ){
            $match_update['match_id'] = $current_match->match_id;
        }

        $this->session->set_flashdata('current_match', 'ok');
        $this->session->set_userdata( 'current_match', (object) $match_update );

        $this->_parse_match_result();
    }


    public function dev_handle_match_answer(){

        $this->_require_user_public_session();

        $current_match = $this->session->userdata('current_match');

        $question_number = $current_match->question_number + 1;

        $match_update = array(
            'question_number'        => $question_number,
            'oponent'                => $current_match->oponent,
            'oponent_team'           => $current_match->oponent_team,
            'challenger'             => $current_match->challenger,
            'is_friend_match'        => $current_match->is_friend_match
        );

        $match_update['answered_questions_ids'] = (array) $current_match->answered_questions_ids;
        $match_update['answered_questions_ids'][] =  $this->session->userdata('current_match_question')->question_id;

        $match_update['answers_data']   = $current_match->answers_data;
        $match_update['answers_data'][] = array(

            'question_id' => $this->session->userdata('current_match_question')->question_id,
            'time'        => $this->session->userdata('picked_answer')->time,
            'answer_id'   => $this->session->userdata('picked_answer')->answer_id
        );

        if( isset( $current_match->match_id ) ){
            $match_update['match_id'] = $current_match->match_id;
        }

        $this->session->set_flashdata('current_match', 'ok');
        $this->session->set_userdata( 'current_match', (object) $match_update );

        // the user answered the three questions
        if(  $question_number > 3 ){

            $this->_parse_match_result();
            return;

        }

        redirect( base_url('game/match') );


    }

    public function match(){
        $this->_require_user_public_session();

        // if this doesnt exists it means that the match has ended and current_match session / flash_session are no longer available
        if( !$this->session->userdata('current_match') ){

            redirect( base_url('game') );
        }


        if( !$this->session->flashdata('current_match') ){
            // TODO: ends match -- analyze what to do after that happens
            redirect( base_url('game/handle_interrumpted_match') );
        }

        $question_1 = $this->main->getRandomQuestion();
        $question_2 = $this->main->getRandomQuestion( array( $question_1[0]->id ) );
        $question_3 = $this->main->getRandomQuestion( array( $question_1[0]->id, $question_2[0]->id ) );

        $team = $this->main->getTeam( array('fk_user_id' => $this->session->userdata('user_public')->id ));

        if( $this->session->userdata('current_match')->challenger == 'yes' ){

            $team_on_left_data = (object) array(
                'id'               => $team->fk_user_id,
                'name'             => $team->name,
                'entity'           => $team->entity,
                'shield_image'     => $team->shield_image,
                'player_firstname' => $team->player_firstname,
                'player_lastname'  => $team->player_lastname,
                'dt_firstname'     => $team->dt_firstname,
                'dt_lastname'      => $team->dt_lastname,
            );

            $team_on_right_data = (object) array(
                'id'               => $this->session->userdata('current_match')->oponent_team->fk_user_id,
                'name'             => $this->session->userdata('current_match')->oponent_team->name,
                'entity'           => $this->session->userdata('current_match')->oponent_team->entity,
                'shield_image'     => $this->session->userdata('current_match')->oponent_team->shield_image,
                'player_firstname' => $this->session->userdata('current_match')->oponent_team->player_firstname,
                'player_lastname'  => $this->session->userdata('current_match')->oponent_team->player_lastname,
                'dt_firstname'     => $this->session->userdata('current_match')->oponent_team->dt_firstname,
                'dt_lastname'      => $this->session->userdata('current_match')->oponent_team->dt_lastname,
            );

        } else {

            $team_on_left_data = (object) array(
                'id'               => $this->session->userdata('current_match')->oponent_team->fk_user_id,
                'name'             => $this->session->userdata('current_match')->oponent_team->name,
                'entity'           => $this->session->userdata('current_match')->oponent_team->entity,
                'shield_image'     => $this->session->userdata('current_match')->oponent_team->shield_image,
                'player_firstname' => $this->session->userdata('current_match')->oponent_team->player_firstname,
                'player_lastname'  => $this->session->userdata('current_match')->oponent_team->player_lastname,
                'dt_firstname'     => $this->session->userdata('current_match')->oponent_team->dt_firstname,
                'dt_lastname'      => $this->session->userdata('current_match')->oponent_team->dt_lastname,
            );

            $team_on_right_data = (object) array(
                'id'               => $team->fk_user_id,
                'name'             => $team->name,
                'entity'           => $team->entity,
                'shield_image'     => $team->shield_image,
                'player_firstname' => $team->player_firstname,
                'player_lastname'  => $team->player_lastname,
                'dt_firstname'     => $team->dt_firstname,
                'dt_lastname'      => $team->dt_lastname,
            );

        }

        $data = array(
            'question_1'         => $question_1,
            'question_2'         => $question_2,
            'question_3'         => $question_3,
            'team'               => $team,
            'in_match'           => true,
            'team_on_left_data'  => $team_on_left_data,
            'team_on_right_data' => $team_on_right_data,
            'team_score'                     => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,
            'pending_matches'                => $this->main->getUserPendingMatches( $this->session->userdata('user_public')->id ),
            'received_matches'               => $this->main->getUserReceivedMatches( $this->session->userdata('user_public')->id ),
        );

        $this->session->set_userdata('picked_answer', (object) array(
            'time'      => 30000,
            'answer_id' => null
        ));


         // krumo($data);
         // die( krumo( $this->session->userdata('current_match') ) );

        $this->load->view('game/header_with_team', $data);
        $this->load->view('game/match_course');
        $this->load->view('game/footer');
    }

    public function dev_match(){

        $this->_require_user_public_session();

        if( !$this->session->flashdata('current_match') ){
            //TODO: ends match -- analyze what to do after that happens
            redirect( base_url('game/handle_interrumpted_match') );
            return;
        }

        // die(krumo( $this->session->userdata('current_match') ));

        // initial picked answer data -- this answer is set via ajax
        // see @ajax_get_answer and game.js@user_pick_answer
        $this->session->set_userdata('picked_answer', (object) array(
            'time'      => 30000,
            'answer_id' => null
        ));

        $question = $this->main->getRandomQuestion( $this->session->userdata('current_match')->answered_questions_ids );

        $this->session->set_userdata( 'current_match_question', (object) array(
            'question_id' => $question[0]->id
        ));

        $data = array(
            'question' => $question
        );

        krumo( $this->session->userdata('current_match') );
        krumo($data);

        $this->load->view('game/header', $data);
        $this->load->view('game/match_course');
        $this->load->view('game/footer');

    }

    public function dev_ajax_get_answer(){

        // TODO: csrf protection

        $this->_require_user_public_session();

        $question = $this->main->getAnswerBydId( $this->input->post('answer_id') );
        $this->session->set_userdata('picked_answer', (object) array(
            'answer_id' => $this->input->post('answer_id'),
            'time'      => $this->input->post('time')
        ));

        $response = array(
            'right_answer' => $question->right_answer
        );

        echo json_encode($response);
    }

    private function _parse_match_result(){

        $this->_require_user_public_session();

        $current_match = $this->session->userdata('current_match');

        // case challenger
        if( $current_match->challenger == 'yes' ){
            // data for tbl_match
            $match_data = array(
                'fk_user_id_challenger' => $this->session->userdata('user_public')->id,
                'fk_user_id_oponent'    => $current_match->oponent,
                'current_turn'          => 'oponent',
                'created_at'            => date('Y-m-d H:i:s'),
                'is_friends_match'      => $current_match->is_friend_match
            );

            $match_id = $this->main->saveMatch( $match_data );

        } else {

            // TODO: if is not challenger
            $match_id = $current_match->match_id;
            $match = $this->main->getMatch( array('id' => $current_match->match_id) );

        }

        $match_answers = array();
        // answer data
        if( count( (array) $current_match->answers_data ) > 0 ){
            foreach( $current_match->answers_data as $a ){
                $match_answers[] = array(
                    'fk_match_id'           => $match_id,
                    'fk_user_id'            => $this->session->userdata('user_public')->id,
                    'fk_question_id'        => $a['question_id'],
                    'fk_question_answer_id' => $a['answer_id'],
                    'time'                  => $a['time'],
                    'created_at'            => date('Y-m-d H:i:s')
                );

            }

            $this->main->saveMatchAnswers( $match_answers );
        }



        // if is not challenger then is an oponent answering a game request
        // at this point we decide who is the winner by updating the match info
        if( $current_match->challenger == 'no' ){
            // decides who is the winner
            $challenger_answered_questions = $this->main->getMatchUserRightAnswers( $match_id,  $match->fk_user_id_challenger );
            $oponent_answered_questions    = $this->main->getMatchUserRightAnswers( $match_id,  $match->fk_user_id_oponent );

            // if they have equal number of right answers the winner is decided by time
            if( $challenger_answered_questions == $oponent_answered_questions ){

                $challenger_time = $this->main->getMatchAnswersTime( $match_id,  $match->fk_user_id_challenger  );
                $oponent_time    = $this->main->getMatchAnswersTime( $match_id,  $match->fk_user_id_oponent  );

                if( $challenger_time < $oponent_time ){
                    $winner_user_id = $match->fk_user_id_challenger;
                } else {
                    $winner_user_id = $match->fk_user_id_oponent;
                }

                // TODO: TIED TIME ANALYSIS

            } else if( $challenger_answered_questions > $oponent_answered_questions ){
                $winner_user_id = $match->fk_user_id_challenger;
            } else {
                $winner_user_id = $match->fk_user_id_oponent;
            }

            // updates match data
            $match_data = array(
                'fk_user_id_winner' => $winner_user_id,
                'ended_at'          => date('Y-m-d H:i:s')
            );

            $this->main->updateMatch( $match_id, $match_data );

            // add Score to the winner
            $score_data = array(
                'fk_team_id'  => $winner_user_id,
                'fk_match_id' => $match_id,
                'score'       => 3,
                'type'        => 'match-point'
            );

            $this->main->addScore( $score_data );
        }

        // erases current match
        $this->session->unset_userdata('current_match');


        if( $current_match->challenger == 'yes' ){
            $the_result = array(
                'success'   => true,
                'match_id'  => $match_id,
                'match_end' => false
            );
        } else {

            $the_result = array(
                'success'       => true,
                'match_id'      => $match_id,
                'match_end'     => true,
                'match_summary' => $this->_get_match_result( $match_id )
            );
        }

        echo json_encode( $the_result );

        // redirect( base_url('game/match_result/' . $match_id ) );

    }

    public function get_share_point( $match_id ){
        $this->_require_user_public_session();

        // gets the match
        $match = $this->main->getMatch( array( 'id' => $match_id ) );


        if( $match === false ){

            // match doesn't exists
            die();
        }

        if( $match->fk_user_id_challenger == $this->session->userdata('user_public')->id){

            $who_are_you   = 'challenger';
            $already_share = $match->challenger_share;
        } else if ( $match->fk_user_id_oponent == $this->session->userdata('user_public')->id  ){
            $who_are_you = 'oponent';
            $already_share = $match->oponent_share;
        } else {

            // neither oponenet or challenger trying to access a match review
            die();
        }


        $type_of_shares = array('facebook','url');
        $share_type = $type_of_shares[ rand(0,1) ];


        if( $share_type == 'url' ){
            $share_content = $this->main->getShareContentRandom();
        } else {
            $share_content = null;
        }

        if( $already_share == 'yes' ){
            redirect('game/game_share_success');
        }

        $data = array(
            'match'                              => $match,
            'match_share_points'                 => $this->_user_today_match_share_points( $this->session->userdata('user_public')->id ),
            'share_type'                         => $share_type,
            'share_content'                      => $share_content,
            'who_are_you'                        => $who_are_you,
            'already_share'                      => $already_share,
            'challeger_team'                     => $this->main->getTeam( array('fk_user_id' => $match->fk_user_id_challenger) ),
            'oponent_team'                       => $this->main->getTeam( array('fk_user_id' => $match->fk_user_id_oponent) ),
            'match_time_challenger'              => $this->main->getMatchAnswersTime( $match_id, $match->fk_user_id_challenger ),
            'match_time_oponent'                 => $this->main->getMatchAnswersTime( $match_id, $match->fk_user_id_oponent ),
            'number_of_right_answers_challenger' => $this->main->getMatchUserRightAnswers( $match_id,  $match->fk_user_id_challenger ),
            'number_of_right_answers_oponent'    => $this->main->getMatchUserRightAnswers( $match_id, $match->fk_user_id_oponent ),
            'your_number_of_right_answers'       => $this->main->getMatchUserRightAnswers( $match_id, $this->session->userdata('user_public')->id  ),
            'your_match_time'                    => $this->main->getMatchAnswersTime( $match_id, $this->session->userdata('user_public')->id  ),
            'remaining_week_plays'               => $this->_get_remaining_week_plays(),
            'remaining_matches_with_friends'     => $this->_get_remaining_matches_with_friends(),
            'won_games'                          => $this->main->getUserWonGames( $this->session->userdata('user_public')->id ),
            'played_games'                       => $this->main->getUserPlayedGames( $this->session->userdata('user_public')->id ),
            'team_score'                         => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,
            'team'                               => $this->main->getTeam( array('fk_user_id' => $this->session->userdata('user_public')->id )),
            'team_score'                     => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,
            'pending_matches'                => $this->main->getUserPendingMatches( $this->session->userdata('user_public')->id ),
            'received_matches'               => $this->main->getUserReceivedMatches( $this->session->userdata('user_public')->id ),
        );

        if( $match->fk_user_id_winner != '' ){

            $match->fk_user_id_winner == $this->session->userdata('user_public')->id ? $data['you_re_winner'] = 'yes' : $data['you_re_winner'] = 'no';
        }



        $this->session->set_userdata('match_point_share_match_id', $match->id);

        // krumo($data);

        $this->load->view('game/header_with_team', $data);
        $this->load->view('game/get_share_point');
        $this->load->view('game/footer');

    }

    public function match_result( $match_id ){

        $this->_require_user_public_session();
        $data = $this->_get_match_result( $match_id );

        die(krumo($data));

        $this->session->set_userdata('match_point_share_match_id', $match->id);

        $this->load->view('game/header', $data);
        $this->load->view('game/match_result');
        $this->load->view('game/footer');
    }

    private function _get_match_result( $match_id ){
        $this->_require_user_public_session();

        // gets the match
        $match = $this->main->getMatch( array( 'id' => $match_id ) );


        if( $match === false ){

            // match doesn't exists
            die();
        }

        if( $match->fk_user_id_challenger == $this->session->userdata('user_public')->id){

            $who_are_you   = 'challenger';
            $already_share = $match->challenger_share;
        } else if ( $match->fk_user_id_oponent == $this->session->userdata('user_public')->id  ){
            $who_are_you = 'oponent';
            $already_share = $match->oponent_share;
        } else {

            // neither oponenet or challenger trying to access a match review
            die();
        }


        $type_of_shares = array('facebook','url');
        $share_type = $type_of_shares[ rand(0,1) ];


        if( $share_type == 'url' ){
            $share_content = $this->main->getShareContentRandom();
        } else {
            $share_content = null;
        }

        $data = array(
            'match'                              => $match,
            'match_share_points'                 => $this->_user_today_match_share_points( $this->session->userdata('user_public')->id ),
            'share_type'                         => $share_type,
            'share_content'                      => $share_content,
            'who_are_you'                        => $who_are_you,
            'already_share'                      => $already_share,
            'challeger_team'                     => $this->main->getTeam( array('fk_user_id' => $match->fk_user_id_challenger) ),
            'oponent_team'                       => $this->main->getTeam( array('fk_user_id' => $match->fk_user_id_oponent) ),
            'match_time_challenger'              => $this->main->getMatchAnswersTime( $match_id, $match->fk_user_id_challenger ),
            'match_time_oponent'                 => $this->main->getMatchAnswersTime( $match_id, $match->fk_user_id_oponent ),
            'number_of_right_answers_challenger' => $this->main->getMatchUserRightAnswers( $match_id,  $match->fk_user_id_challenger ),
            'number_of_right_answers_oponent'    => $this->main->getMatchUserRightAnswers( $match_id, $match->fk_user_id_oponent ),
            'your_number_of_right_answers'       => $this->main->getMatchUserRightAnswers( $match_id, $this->session->userdata('user_public')->id  ),
            'your_match_time'                    => $this->main->getMatchAnswersTime( $match_id, $this->session->userdata('user_public')->id  )
        );

        if( $match->fk_user_id_winner != '' ){

            $match->fk_user_id_winner == $this->session->userdata('user_public')->id ? $data['you_re_winner'] = 'yes' : $data['you_re_winner'] = 'no';
        }

        return $data;
    }

    // gets how many times a user has won share points today
    private function _user_today_match_share_points($team_id){

        $r = $this->main->getTeamScoreQuantityToday( $team_id, 'match-share' );
        return $r->quantity;

    }

    public function ajax_insert_match_end_score(){

        $this->_require_user_public_session();

        $match_id = $this->session->userdata('match_point_share_match_id');
        $match = $this->main->getMatch( array( 'id' => $match_id ) );

        if( $match->fk_user_id_challenger == $this->session->userdata('user_public')->id){

            $match_update_data = array('challenger_share' => 'yes');
        } else if ( $match->fk_user_id_oponent == $this->session->userdata('user_public')->id  ){
            $match_update_data = array('oponent_share' => 'yes');

        } else {
            // neither oponenet or challenger trying to access a match review
            die();
        }

        // adds share point
        $score_data = array(
            'fk_team_id'  => $this->session->userdata('user_public')->id,
            'fk_match_id' => $match_id,
            'type'        => $this->input->post('type'),
            'score'       => 1,
            'extra_info'  => $this->input->post('extra_info')
        );

        $this->main->addScore( $score_data );
        $this->main->updateMatch( $match_id, $match_update_data );

    }

    public function ajax_get_random_player(){

        $this->_require_user_public_session();

        $user_friends = $this->_get_user_fb_friends( $this->session->userdata('user_public')->facebook_id );
        $oponent = $this->main->getRandomOponent( $this->session->userdata('user_public')->id, $user_friends );


        if( count($oponent) > 0 ){
            echo json_encode(array(
                'oponent_name'   => $oponent->firstname . ' ' . $oponent->lastname,
                'oponent_enc_id' => $this->encrypt->encode( $oponent->id ),
                'success'         => true
            ));
        } else {
            echo json_encode(array(
                'success' => false,
                'msg'     => 'No hemos encontrado un jugador para ti ahora mismo, intentalo de nuevo más tarde'
            ));
        }
    }

    public function resume(){
        $this->_require_user_public_session();

        $data = array(
            'ended_matches'                  => $this->main->getUserEndedMatches( $this->session->userdata('user_public')->id ),
            'pending_matches'                => $this->main->getUserPendingMatches( $this->session->userdata('user_public')->id ),
            'received_matches'               => $this->main->getUserReceivedMatches( $this->session->userdata('user_public')->id ),
            'team'                           => $this->main->getTeam( array('fk_user_id' => $this->session->userdata('user_public')->id )),
            'remaining_week_plays'           => $this->_get_remaining_week_plays(),
            'remaining_matches_with_friends' => $this->_get_remaining_matches_with_friends(),
            'team_score'                     => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,

        );

        // krumo($data);

        $this->load->view('game/header_with_team', $data);
        $this->load->view('game/resume');
        $this->load->view('game/footer');

    }

    public function dev_resume(){

        $this->_require_user_public_session();

        $data = array(
            'ended_matches'    => $this->main->getUserEndedMatches( $this->session->userdata('user_public')->id ),
            'pending_matches'  => $this->main->getUserPendingMatches( $this->session->userdata('user_public')->id ),
            'received_matches' => $this->main->getUserReceivedMatches( $this->session->userdata('user_public')->id )
        );

        krumo($data);

        $this->load->view('game/dev_header', $data);
        $this->load->view('game/dev_resume');
        $this->load->view('game/footer');
    }

    // functon when the oponent responds a challenger match
    public function play_match( $match_id ){

        $this->_require_user_public_session();

        $match = $this->main->getMatch( array('id' => $match_id) );
        // krumo($match);

        $valid_match = false;

        // checks if you can actually respond this match
        if( $match->fk_user_id_oponent == $this->session->userdata('user_public')->id && $match->fk_user_id_winner == '' ){
            $valid_match = true;
        }

        if( $valid_match === false ){

            // TODO: ACtion when isn't valid match
            die();
        }

        $this->session->set_flashdata('current_match', 'ok');

        // the oponent takes the role of the challenger
        $this->session->set_userdata( 'current_match', (object) array(
            'match_id'               => $match_id,
            'question_number'        => 1,
            'oponent'                => $match->fk_user_id_challenger,
            'oponent_team'           => $this->main->getTeam( array('fk_user_id' => $match->fk_user_id_challenger )),
            'challenger'             => 'no',
            'answered_questions_ids' => array(),
            'answers_data'           => array(),
            'is_friend_match'        => $match->is_friends_match
        ));

        // saves the match intent
        $this->main->registerMatchIntent( $this->session->userdata('user_public')->id );

        redirect( base_url('game/match') );

    }

    public function decline_match( $match_id ){
        $this->_require_user_public_session();
        $match = $this->main->getMatch( array('id' => $match_id) );


        $valid_match = false;

        // checks if you can actually respond this match
        if( $match->fk_user_id_oponent == $this->session->userdata('user_public')->id && $match->fk_user_id_winner == '' ){
            $valid_match = true;
        }

        if( $valid_match === false ){

            // TODO: ACtion when isn't valid match
            die();
        }

        // set match as declined
        $this->main->updateMatch( $match->id, array(
            'declined' => 'yes',
            'ended_at' => date('Y-m-d H:i:s')
        ));

        // removes the match intent for the oponent
        $this->main->deleteMatchIntentForUser( $match->fk_user_id_challenger );

        redirect( base_url('game/resume') );
    }

    public function cancel_match(){
        $this->_require_user_public_session();

        $this->session->unset_userdata('current_match');
        $this->session->set_flashdata('cancel_msg', 'Has cancelado un partido, gastando una oportunidad de juego hoy');

        redirect( base_url('game/dashboard') );
    }

    // returns the number of days of the week taking by start point the day of the week of the season start date
    private function _get_elapsed_activity_days(){
        $this->_require_user_public_session();
        $current_season = $this->main->getCurrentSeason();

//        krumo($current_season);

        $day_start_of_current_season = date('j', strtotime( $current_season->start_date ) );
        $days_elpased_till_today = date('j') - ( $day_start_of_current_season  - 1);

        return $days_elpased_till_today;
    }

    private function _get_remaining_week_plays(){
        $this->_require_user_public_session();

        $elapsed_days = $this->_get_elapsed_activity_days();
        $total_allowed_matchs = $this->allowed_matches_per_day * $elapsed_days;

        return $total_allowed_matchs - $this->main->getUserMatchIntentsCountThisWeek( $this->session->userdata('user_public')->id );

    }

    public function _get_remaining_matches_with_friends(){
        $this->_require_user_public_session();

        $number = $this->allowed_friends_match_per_day - $this->main->getUserMatchCountWithFriendsToday( $this->session->userdata('user_public')->id );

        // THe number will be negative if the invitations and the match requests between friends is larger than 10, in this case we handle the number.
        $number < 0 ? $number = 0 : $number = $number;

        return $number;
    }


    // when oponent quits a match
    public function handle_interrumpted_match(){

        $this->_require_user_public_session();

        /*krumo( $this->session->userdata('user_public') );
        krumo( $this->session->userdata('current_match') );*/

        if( $this->session->userdata('current_match')->challenger == 'yes' ){

            $this->session->unset_userdata('current_match');
            $this->session->set_flashdata('cancel_msg', 'Has Cancelado un partido, gastando una oportunidad de juego hoy');

            redirect( base_url('game/dashboard') );

        } else{
            // The oponent closes the browser / tab / window -- the matchpoint is given to the challenger
            // The challenger wins the match

            $current_match = $this->session->userdata('current_match');
            $match = $this->main->getMatch( array('id' => $current_match->match_id) );

            // updates match data
            $match_data = array(
                'fk_user_id_winner' => $match->fk_user_id_challenger,
                'ended_at'          => date('Y-m-d H:i:s')
            );

            $this->main->updateMatch( $match->id, $match_data );

            // add Score to the winner
            $score_data = array(
                'fk_team_id'  => $match->fk_user_id_challenger,
                'fk_match_id' => $match->id,
                'score'       => 3,
                'type'        => 'match-point'
            );

            $this->main->addScore( $score_data );

            $this->session->unset_userdata('current_match');
            $this->session->set_flashdata('cancel_msg', 'Has cancelado el partido de tu oponenete, has perdido el partido!');

            redirect( base_url('game/dashboard') );

        }
    }

    private function _get_register_fb_login_url(){
        FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper( base_url('landing/register') );
        return $helper->getLoginUrl( $this->login_permissions );
    }

    private function _filter_fb_friends( $user_friends ){

        return $this->main->filterFbFriends( $user_friends );

    }

    public function game_mechanics(){

        $this->_require_user_public_session();

        $data = array(
            'team_score'    => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,
            'team'          => $this->main->getTeam( array('fk_user_id' => $this->session->userdata('user_public')->id )),
            'team_score'                     => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,
            'pending_matches'                => $this->main->getUserPendingMatches( $this->session->userdata('user_public')->id ),
            'received_matches'               => $this->main->getUserReceivedMatches( $this->session->userdata('user_public')->id ),
        );

        $this->load->view('game/header_with_team', $data);
        $this->load->view('game/game_mechanics');
        $this->load->view('game/footer');
    }

    public function game_share_success(){

        $this->_require_user_public_session();

         $data = array(
            'team_score'    => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,
            'team'          => $this->main->getTeam( array('fk_user_id' => $this->session->userdata('user_public')->id )),
            'remaining_week_plays'           => $this->_get_remaining_week_plays(),
            'remaining_matches_with_friends' => $this->_get_remaining_matches_with_friends(),
            'pending_matches'                => $this->main->getUserPendingMatches( $this->session->userdata('user_public')->id ),
            'received_matches'               => $this->main->getUserReceivedMatches( $this->session->userdata('user_public')->id ),
        );

        $this->load->view('game/header_with_team', $data);
        $this->load->view('game/game_share_success');
        $this->load->view('game/footer');
    }

    public function ranking(){

        $this->_require_user_public_session();

        $current_season = $this->main->getCurrentSeason();
        $ranking = $this->main->getRanking( $current_season->id );

        // krumo($ranking);
        $rank_data = array();

        foreach( $ranking as $r ){

            $rank_data[] = (object) array(
                'team_id'       => $r->team_id,
                'won_games'     => $this->main->getUserWonGames( $r->team_id ),
                'played_games'  => $this->main->getUserPlayedGames( $r->team_id ),
                'right_answers' => $this->main->getUserRightAnswers( $r->team_id )->quantity,
                'best_time'     => $this->main->getUserBestTime( $r->team_id )->time,
                'team'          => $this->main->getTeam( array('fk_user_id' => $r->team_id )),
                'rank'          => $r->rank,
                'score'         => $r->score
            );
        }



        $data = array(
            'team_score'    => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,
            'team'          => $this->main->getTeam( array('fk_user_id' => $this->session->userdata('user_public')->id )),
            'team_score'    => $this->main->getTeamScore( $this->session->userdata('user_public')->id )->score,
            'won_games'     => $this->main->getUserWonGames( $this->session->userdata('user_public')->id ),
            'played_games'  => $this->main->getUserPlayedGames( $this->session->userdata('user_public')->id ),
            'right_answers' => $this->main->getUserRightAnswers( $this->session->userdata('user_public')->id )->quantity,
            'best_time'     => $this->main->getUserBestTime( $this->session->userdata('user_public')->id )->time,
            'user_ranking'  => $this->getUserRankingForSeason( $current_season->id, $this->session->userdata('user_public')->id  ),
            'rank_data'     => $rank_data,
            'pending_matches'                => $this->main->getUserPendingMatches( $this->session->userdata('user_public')->id ),
            'received_matches'               => $this->main->getUserReceivedMatches( $this->session->userdata('user_public')->id ),
        );

        $this->load->view('game/header_with_team', $data);
        $this->load->view('game/ranking');
        $this->load->view('game/footer');
    }

    public function comeback(){
        $data = array(
            'in_home' => true
        );

        $this->load->view('game/header', $data);
        $this->load->view('game/home_comeback');
        $this->load->view('game/footer');
    }

}