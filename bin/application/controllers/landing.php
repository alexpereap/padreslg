<?php

require __DIR__ . '/../libraries/facebook-php-sdk-v4-4.0-dev/autoload.php';



use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;


session_start();

class Landing extends CI_Controller {

    private $login_permissions = array('user_friends', 'email', 'user_birthday' );
    private $app_id = '519810394841305';
    private $app_secret = '57d26bb6ff778b0a6961b5ff6a98966a';

    public function index(){
        $this->home();
    }

    public function home(){

        $data = array(
            'in_home' => true
        );

        $this->load->view('landing/header', $data);
        $this->load->view('landing/home');
        $this->load->view('landing/footer');
    }


    public function register(){

        $data = array();
        $data['in_register'] = true;

        FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper( base_url('landing/register') );

        try {
            $session = $helper->getSessionFromRedirect();
        } catch( FacebookRequestException $ex ) {
            // When Facebook returns an error

            redirect( base_url('landing/register') );
        } catch( Exception $ex ) {
            // When validation fails or other local issues

            redirect( base_url('landing/register') );
        }

        // see if we have a session - in order to preload $data
        if ( isset( $session ) ) {

            try{

                // graph api request for user data
                $request = new FacebookRequest( $session, 'GET', '/me' );
                $response = $request->execute();

                // get response
                $graphObject = $response->getGraphObject();
                $fb_id = $graphObject->getProperty( 'id' );

                $user = $this->main->getUser( array('facebook_id' => $fb_id) );

                if( $user ){
                    // TODO:user already exists action

                    $team = $this->main->getTeam( array('fk_user_id' => $user->id) );

                    if( $team ){
                        // TODO: if team exists
                        $this->_register_session( $user->id );
                        redirect( base_url('landing/pre_register_success') );
                        return;

                    } else {

                        // Continues with team creation
                        $this->_register_session( $user->id );
                        redirect( base_url('landing/team_builder') );
                        return;
                    }

                }

                $accessToken = $session->getAccessToken();
                $longLivedAccessToken = $accessToken->extend();

                // user data prefill

                $user_data = array(
                    'firstname'      => $graphObject->getProperty( 'first_name' ),
                    'lastname'       => $graphObject->getProperty( 'last_name' ),
                    'birthdate'      => date('Y-m-d', strtotime($graphObject->getProperty( 'birthday' ))),
                    'split_birhdate' => explode('-', date('Y-m-d', strtotime($graphObject->getProperty( 'birthday' )))),
                    'email'          => $graphObject->getProperty( 'email' )
                );

                $data['user_data'] = $user_data;


            } catch( FacebookRequestException $ex ) {
                      // When Facebook returns an error
                     $this->session->set_flashdata('auth_error', 'Ha ocurrido un problema de autenticación con facebook, intentalo de nuevo.');
            } catch( Exception $ex ) {
                      // When validation fails or other local issues
                      $this->session->set_flashdata('auth_error', 'Ha ocurrido un problema de autenticación con facebook, intentalo de nuevo.');
            }

            // temp data with facebook data
            $this->session->set_userdata( 'user_public', array(
                'facebook_id'               => $fb_id,
                'facebook_long_lived_token' => (string) $longLivedAccessToken
            ));
        } else {

            $data['fb_login_url']    = $this->_get_fb_login_url();
            $data['disabled_fields'] = true;

        }


        $this->load->view('landing/header', $data);
        $this->load->view('landing/register');
        $this->load->view('landing/footer');



    }

    private function _get_fb_login_url(){
        FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper( base_url('landing/register') );
        return $helper->getLoginUrl( $this->login_permissions );
    }

    public function preset_user_data(){
        $current_user_data = $this->session->userdata('user_public');

        $this->session->set_userdata('user_public', (object) array(
            'facebook_id'               => $current_user_data['facebook_id'],
            'facebook_long_lived_token' => $current_user_data['facebook_long_lived_token'],
            'firstname'                 => $this->input->post('firstname'),
            'lastname'                  => $this->input->post('lastname'),
            'birthdate'                 => $this->input->post('birthdate'),
            'email'                     => $this->input->post('email'),
            'nid_type'                  => $this->input->post('nid_type'),
            'nid'                       => $this->input->post('nid'),
            'address'                   => $this->input->post('address'),
            'city'                      => $this->input->post('city'),
            'cellphone'                 => $this->input->post('cellphone')
        ));

        redirect(base_url('landing/register_step_2'));
    }

    public function register_step_2(){

        if( $this->session->userdata('user_public') === false ){
            redirect(base_url('landing'));
        }

        // krumo( $this->session->userdata('user_public') );

        $this->load->view('landing/header');
        $this->load->view('landing/register-2');
        $this->load->view('landing/footer');
    }

    public function handle_register(){

        $this->load->model('model_files');

        /*krumo($_POST);
        krumo($_FILES);
        krumo( $this->session->userdata('user_public') );*/

        $current_user_data = $this->session->userdata('user_public');


        $user_data = array();
        $father_data = array();
        // File Handling

        if(  $_FILES['father_photo']['name'] != '' ){


            $rf = $this->model_files->upload_file('father_photo', './uploads', 'jpg|jpeg|png' );

            if( $rf['success'] === true ){

                $file_name = $rf['data']['file_name'];
                $father_data = array(
                    'firstname'  => $this->input->post('father_firstname'),
                    'lastname'   => $this->input->post('father_lastname'),
                    'photo'      => $file_name,
                    'created_at' => date('Y-m-d H:i:s')
                );

            } else {

                $this->session->set_flashdata('error', 'Lo sentimos, no hemos podido procesar tu imagen.<br/>Verifica que tu imagen sea de formato .jpg, .jpeg o .png.<br/>Guardala tu imagen en alguno de estos formatos y verifica que no pese más de 7MB.');
                redirect(base_url('landing/register_step_2'));

            }

        } else {

            $this->session->set_flashdata('error', 'Lo sentimos, no hemos podido procesar tu imagen.<br/>Verifica que tu imagen sea de formato .jpg, .jpeg o .png.<br/>Guardala tu imagen en alguno de estos formatos y verifica que no pese más de 7MB.');
            redirect(base_url('landing/register_step_2'));

        }


        $user_data = array(
            'firstname'                 => $current_user_data->firstname,
            'lastname'                  => $current_user_data->lastname,
            'birthdate'                 => $current_user_data->birthdate,
            'email'                     => $current_user_data->email,
            'nid_type'                  => $current_user_data->nid_type,
            'nid'                       => $current_user_data->nid,
            'address'                   => $current_user_data->address,
            'city'                      => $current_user_data->city,
            'cellphone'                 => $current_user_data->cellphone,
            'facebook_id'               => $current_user_data->facebook_id,
            'facebook_long_lived_token' => $current_user_data->facebook_long_lived_token,
            'created_at'                => date('Y-m-d H:i:s'),
            'subscribed'                => isset( $_POST['terms'] ) ? 'yes' : 'no'
        );

        $user_id = $this->main->saveUser($user_data);

        $father_data['fk_user_id'] = $user_id;
        $father_id = $this->main->saveFather( $father_data );

        $this->_register_session( $user_id );

        redirect( base_url('landing/team_builder') );
    }

    private function _register_session($user_id){

        $user = $this->main->getUser( array('id' => $user_id) );
        $this->session->set_userdata( 'user_public', $user );
    }

    public function team_builder(){

        if( $this->session->userdata('user_public') === false ){
            redirect('landing/register');
            return;
        }

        if( !isset( $this->session->userdata('user_public')->father_firstname ) ){
            redirect('landing/register');
            return;
        }

        // krumo($this->session->userdata('user_public'));
        $initials = substr($this->session->userdata('user_public')->firstname, 0,1) . substr($this->session->userdata('user_public')->father_firstname, 0,1);

        $data = array(
            'initials'          => $initials,
            'shield_designs'    => $this->main->getShieldDesigns(),
            'points'            => 0,
            'require_landscape' => true
        );

        // die(krumo($data));

        $this->load->view('landing/header_with_team', $data);
        $this->load->view('landing/team_builder');
        $this->load->view('landing/footer');
    }

    public function handle_team_build(){

        if( $this->session->userdata('user_public') === false ){
            redirect('landing/register');
            return;
        }

        $initials = substr($this->session->userdata('user_public')->firstname, 0,1) . substr($this->session->userdata('user_public')->father_firstname, 0,1);

        $team_data = array(
            'fk_user_id'           => $this->session->userdata('user_public')->id,
            'fk_shield_designs_id' => $this->input->post('team_shield_id'),
            'name'                 => $initials,
            'entity'               => $this->input->post('team_entity'),
            'created_at'           => date('Y-m-d H:i:s')
        );

        $this->main->saveTeam( $team_data );

        // team id always same as user id
        $team_id = $this->session->userdata('user_public')->id;

        $start_season = $this->main->getSeason( array('id' => 1) );

        // first 3 points for preregister
        /*$score_data = array(
            'fk_team_id' => $team_id,
            'score'      => 3,
            'type'       => 'pre-register',
            'created_at' => $start_season->start_date
        );

        $this->main->addScore( $score_data );*/

        // mail
        $this->load->model('model_mails');

        $mail_body = $this->load->view('emails/pre-register',null,true);
        $this->model_mails->singleEmail( $this->session->userdata('user_public')->email, $mail_body, 'Ya estás pre-inscrito!' );

        // redirect to pre register success
        // redirect( base_url('landing/pre_register_success') );

        // goes to game dashboard
        redirect( base_url( 'game/dashboard' ) );
    }

    public function pre_register_success(){

        if( $this->session->userdata('user_public') === false ){
            redirect('landing/register');
            return;
        }

        $team = $this->main->getTeam( array('fk_user_id' => $this->session->userdata('user_public')->id ));


        $data = array(
            'team'            => $team,
            'points_by_share' => $this->_user_has_points_by_landing_share( $this->session->userdata('user_public')->id ),
            'initials'        => $team->name,
            'require_landscape' => true
        );

        $this->load->view('landing/header_with_team', $data);
        $this->load->view('landing/pre_register_success');
        $this->load->view('landing/footer');

    }

    private function _user_has_points_by_landing_share( $user_id ){

        $score_by_landing_share = $this->main->getTeamScoreDetail(array(
            'fk_team_id' => $user_id,
            'type'       => 'landing-share'
        ));

        return (count( $score_by_landing_share ) > 0);
    }

    public function test(){
        $this->load->view('emails/pre-register');
    }

    public function ajax_insert_team_score(){

        if( $this->_user_has_points_by_landing_share( $this->session->userdata('user_public')->id ) === false ){

            $start_season = $this->main->getSeason( array('id' => 1) );

            $data = array(
                'fk_team_id' => $this->session->userdata('user_public')->id,
                'type'       => 'landing-share',
                'score'      => 1,
                'created_at' => $start_season->start_date
            );

            $this->main->addScore( $data );
        }
    }

    public function logout(){
        $this->session->unset_userdata('user_public');
        redirect( base_url('game') );
    }
}