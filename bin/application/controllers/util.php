<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Util extends CI_Controller {

	public function __construct()
	{

		parent::__construct();
		$this->load->helper("url");

		$this->load->model("main","model");
	}

	public function registeredUsersCount()
	{
		echo $this->model->getUsersCount();
	}
}
