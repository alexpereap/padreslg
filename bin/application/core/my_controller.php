<?php

class MY_Controller extends CI_Controller{

    protected $allowed_domains;

    function __construct(){
        parent::__construct();

        // allowrd domains for ajax accesing cross site request foregery protection
        $this->allowed_domains = array(
            'http://localhost/2015/lg/padreslg/bin/',
            'http:://padres.masqueuntv.com/'
        );
    }
}