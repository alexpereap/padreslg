<?php

Class Main extends CI_Model{


    public function getUser( $constraints ){

        $q = $this->db->get_where('vw_user_father', $constraints );
        return count( $q->row() ) > 0 ? $q->row() : false;
    }

    public function saveFather( $data ){
        $this->db->insert('tbl_user_father', $data );
        return $this->db->insert_id();
    }

    public function saveUser( $data ){
        $this->db->insert('tbl_user', $data );
        return $this->db->insert_id();
    }

    public function getTeam( $constraints ){
        $q = $this->db->get_where('vw_team_with_score', $constraints );
        return count( $q->row() ) > 0 ? $q->row() : false;
    }

    public function getShieldDesigns(){
        $q = $this->db->get('tbl_shield_designs');
        return $q->result();
    }

    public function saveTeam( $data ){
        $this->db->insert('tbl_team', $data);
    }

    public function addScore( $data ){

        if( !isset( $data['created_at'] ) ){

            $data['created_at'] = date('Y-m-d H:i:s');
        }

        $this->db->insert('tbl_team_score', $data );
        return $this->db->insert_id();
    }

    public function getTeamScore( $team_id ){

        $current_season = $this->getCurrentSeason();

        switch( $current_season->id ){
            case '1':
                $q = $this->db->get_where('vw_season_one_score', array( 'team_id' => $team_id ));
                break;

            case '2':
                $q = $this->db->get_where('vw_season_two_score', array( 'team_id' => $team_id ));
                break;

            case '3':
                $q = $this->db->get_where('vw_season_three_score', array( 'team_id' => $team_id ));
                break;

            case '4':
                $q = $this->db->get_where('vw_season_four_score', array( 'team_id' => $team_id ));
                break;
        }


        $result = $q->row();

        if( count( $result ) == 0 ){
            $result = (object) array(
                'team_id' => $team_id,
                'score'   => 0
            );
        }

        return $result;
    }

    public function getTeamScoreDetail( $constraints ){
        $q = $this->db->get_where('tbl_team_score', $constraints);
        return $q->result();
    }

    public function logInCmsUser( $userdata ){


        $user = $this->getCmsUserByUsername( $userdata['username'] );

        if( count( $user ) > 0 ){

            $password = $this->encrypt->decode( $user->password );

            if( $password == $userdata['password'] ){


                $session_data = array(
                    'user_id'   => $user->id,
                    'username'  => $user->username,
                    'logged_in' => TRUE
                    );

                $this->session->set_userdata('user_admin',$session_data);

                return true;
            }
        }

        return false;

    }

    public function getCmsUserByUsername( $username ){
        $q = $this->db->get_where('tbl_cms_user',  array('username' => $username));
        return $q->row();
    }

    public function addQuestion( $data ){

        $this->db->insert('tbl_question', $data);
        return $this->db->insert_id();
    }

    public function addQuestionAnswers( $data ){

        $this->db->insert_batch('tbl_question_answers', $data);
    }

    public function getQuestionDetail( $question_id ){
        $q = $this->db->get_where('vw_questions_answers', array('id' => $question_id) );
        return $q->result();
    }

    public function updateQuestion( $question_id, $data ){
        $this->db->where('id', $question_id );
        $this->db->update( 'tbl_question', $data );
    }

    public function updateQuestionAnswers( $data ){
        $this->db->update_batch('tbl_question_answers', $data, 'id');
    }

    public function getRandomQuestion( $exclude_ids = array() ){

        $constraint = '';

        if( count( $exclude_ids ) > 0 ){

            $constraint = "WHERE id NOT IN (" . implode(',', $exclude_ids) . ")";
        }

        $query = "SELECT * FROM tbl_question  $constraint  ORDER BY RAND()";
        $q = $this->db->query( $query );

        $question = $q->row();

        // gets questio answers
        return $this->getQuestionDetail( $question->id );
    }

    public function getAnswerBydId( $answer_id ){
        $q = $this->db->get_where('tbl_question_answers', array('id' => $answer_id) );
        return $q->row();
    }

    public function saveMatch( $match_data ){

        $this->db->insert('tbl_match', $match_data);
        return $this->db->insert_id();
    }

    public function saveMatchAnswers( $match_answers ){

        $this->db->insert_batch( 'tbl_match_answers', $match_answers );

    }

    public function getMatch( $constraints ){
        $q = $this->db->get_where('tbl_match', $constraints);
        return count( $q->row() ) > 0 ? $q->row() : false;
    }

    public function getTeamScoreQuantityToday( $team_id, $type_of_score ){

        $query = "SELECT COUNT(id) as quantity FROM tbl_team_score where fk_team_id = $team_id
                and type = '$type_of_score'
                and  date(created_at) = date( wunapps_now() )";

        $q = $this->db->query($query);

        return $q->row();
    }

    public function getTeamScoreQuantityByType( $team_id, $type_of_score ){

        $current_season = $this->getCurrentSeason();

        $query = "SELECT COUNT(id) as quantity FROM tbl_team_score where fk_team_id = $team_id
                and type = '$type_of_score'
                AND created_at >= '{$current_season->start_date}' && created_at <= '{$current_season->end_date}' ";

        $q = $this->db->query($query);

        return $q->row();
    }

    public function getShareContentRandom(){

        $this->db->order_by('id','RANDOM');
        $this->db->limit(1);
        $q = $this->db->get('tbl_share_content');

        return $q->row();
    }

    public function updateMatch($match_id, $data){

        $this->db->where('id', $match_id);
        $this->db->update('tbl_match', $data );
    }

    public function getMatchAnswersTime( $match_id, $user_id ){
        $query = "SELECT sum(time) as time FROM tbl_match_answers where fk_match_id = $match_id and fk_user_id = $user_id";
        $q = $this->db->query($query);

        $result = $q->row();

        return $result->time;
    }

    public function getMatchUserAnswers( $match_id, $user_id ){
        $q = $this->db->get_where('vw_match_answers_detail', array(
            'match_id' => $match_id,
            'user_id'  => $user_id
        ));

        return $q->result();
    }

    public function getMatchUserRightAnswers( $match_id, $user_id ){
        $query = "SELECT count(match_answer_id) as quantity FROM vw_match_answers_detail
                    WHERE match_id = $match_id and user_id = $user_id and right_answer = 'yes'";
        $q = $this->db->query($query);

        $result = $q->row();
        return $result->quantity;
    }

    public function getRandomOponent( $user_id, $fb_friends ){

        $user_friends_ids = array();

        if( count( $fb_friends ) > 0 ){

            foreach( $fb_friends as $u ){
                $user_friends_ids[] = $u->id;
            }

            $query = "SELECT * FROM tbl_user WHERE facebook_id NOT IN (" . implode(',', $user_friends_ids) . ") AND id != $user_id ORDER BY RAND() LIMIT 1";
        } else {
            $query = "SELECT * FROM tbl_user WHERE id != $user_id ORDER BY RAND() LIMIT 1";
        }


        $q = $this->db->query($query);

        return $q->row();

    }

    public function getUserEndedMatches( $user_id ){

        $query = "SELECT * FROM vw_matches_overview WHERE (challenger_id = $user_id OR oponent_id = $user_id) AND winner_id IS NOT NULL ORDER BY match_created_at DESC";
        $q = $this->db->query($query);

        return $q->result();
    }

    public function getUserPendingMatches( $user_id ){

        $current_season = $this->getCurrentSeason();

        $query = "SELECT * FROM vw_matches_overview WHERE challenger_id = $user_id AND winner_id IS NULL
                    AND match_created_at >= '{$current_season->start_date}' && match_created_at <= '{$current_season->end_date}'";

        $q = $this->db->query($query);

        return $q->result();
    }

    public function getUserReceivedMatches( $user_id ){

        $current_season = $this->getCurrentSeason();

        $query = "SELECT * FROM vw_matches_overview WHERE oponent_id = $user_id AND winner_id IS NULL
                    AND match_created_at >= '{$current_season->start_date}' && match_created_at <= '{$current_season->end_date}'";
        $q = $this->db->query($query);

        return $q->result();
    }

    public function registerMatchIntent( $user_id ){

        $q = $this->db->insert('tbl_match_intents', array(
            'fk_user_id' => $user_id,
            'created_at' => date('Y-m-d H:i:s')
        ));

        return $this->db->insert_id();
    }

    public function deleteMatchIntentForUser( $user_id ){

        $this->db->order_by('id', 'DESC');
        $this->db->where('fk_user_id', $user_id);
        $this->db->limit(1);
        $q = $this->db->get('tbl_match_intents');

        $intent = $q->row();

        if( count($intent) > 0 ){
            $this->db->where('id', $intent->id );
            $this->db->delete('tbl_match_intents');
        }
    }

    public function getUserMatchIntentsCountThisWeek( $user_id ){

        $current_season = $this->getCurrentSeason();

        $query = "SELECT COUNT(id) as quantity FROM tbl_match_intents WHERE
                fk_user_id = $user_id
                AND created_at >= '{$current_season->start_date}'
                AND created_at <= '{$current_season->end_date}'";

        $q = $this->db->query($query);

        $result = $q->row();
        return $result->quantity;
    }

    public function getUserMatchCountWithFriendsToday( $user_id ){

        $query = "SELECT COUNT(id) as quantity FROM tbl_match WHERE
                 (fk_user_id_challenger = $user_id OR fk_user_id_oponent = $user_id)
                 AND is_friends_match = 'yes'
                 AND DATE(created_at) = DATE( wunapps_now() )";

        $q = $this->db->query($query);

        $result = $q->row();
        return $result->quantity;

    }

    public function getSeasons(){

        $q = $this->db->get('tbl_seasons');
        $seasons = $q->result();

    }

    public function getCurrentSeason(){
        $q = $this->db->get('vw_current_season');
        return $q->row();
    }

    public function getSeason( $constraints ){
        $q = $this->db->get_where('tbl_seasons', $constraints);
        return $q->row();
    }

    public function getUserWonGames( $user_id ){

        $current_season = $this->getCurrentSeason();

        $query = "SELECT count(id) as quantity from tbl_match WHERE fk_user_id_winner = $user_id
                    AND created_at >= '{$current_season->start_date}' && created_at <= '{$current_season->end_date}'";
        $q = $this->db->query($query);

        $result = $q->row();
        return $result->quantity;
    }

    public function getUserPlayedGames( $user_id ){

        $current_season = $this->getCurrentSeason();

        $query = "SELECT COUNT(id) as quantity FROM tbl_match_intents WHERE
                fk_user_id = $user_id
                AND created_at >= '{$current_season->start_date}' && created_at <= '{$current_season->end_date}'  ";

        $q = $this->db->query($query);

        $result = $q->row();
        return $result->quantity;
    }


    public function getUsersCount(){

        return  $this->db->count_all_results('tbl_user');
    }

    public function filterFbFriends( $user_friends ){

        $fb_ids = array();

        foreach( $user_friends as $u ){
            $fb_ids[] = $u->id;
        }

        if( count($fb_ids) > 0 ){
            $query = "SELECT facebook_id as id, CONCAT( firstname, ' ', lastname ) as name FROM tbl_user WHERE facebook_id IN ( " . implode(',', $fb_ids ) . " )";
            $q = $this->db->query( $query );
            return $q->result();
        } else{

            return array();
        }


    }

    public function getUserSeasonsRanking( $user_id ){

        $season_one = $this->db->get_where( 'vw_season_one_ranking', array( 'team_id' => $user_id )  );
        $season_two = $this->db->get_where( 'vw_season_two_ranking', array( 'team_id' => $user_id )  );
        $season_three = $this->db->get_where( 'vw_season_three_ranking', array( 'team_id' => $user_id )  );
        $season_four = $this->db->get_where( 'vw_season_four_ranking', array( 'team_id' => $user_id )  );

        $result = (object) array(
            'season_one'   => count($season_one->row()) > 0 ?  $season_one->row() : false,
            'season_two'   => count($season_two->row()) > 0 ?  $season_two->row() : false,
            'season_three' => count($season_three->row()) > 0 ?  $season_three->row() : false,
            'season_four'  => count($season_four->row()) > 0 ?  $season_four->row() : false
        );

        return $result;
    }

    // current season
    public function getUserRightAnswers( $user_id ){

        $q = $this->db->get_where('vw_user_right_answers', array('user_id' => $user_id ) );
        $result = $q->row();

        if( count($result) == 0 ){
            $result = (object) array (
                'user_id'  => $user_id,
                'quantity' => 0
            );
        }

        return $result;
    }

    // current season
    public function getUserBestTime( $user_id ){
        $q = $this->db->get_where('vw_user_best_time', array('user_id' => $user_id ) );
        $result = $q->row();

        if( count($result) == 0 ){
            $result = (object) array (
                'user_id'  => $user_id,
                'time' => '---'
            );
        }

        return $result;
    }

    // current season
    public function getUserAverageTime( $user_id ){
        $q = $this->db->get_where('vw_average_time', array('user_id' => $user_id ) );
        $result = $q->row();

        if( count($result) == 0 ){
            $result = (object) array (
                'user_id'      => $user_id,
                'average_time' => 0
            );
        }

        return $result;
    }

    // current season
    public function getUserRightAnswersPercent( $user_id ){
        $q = $this->db->get_where('vw_user_right_answer_percent', array('user_id' => $user_id ) );
        $result = $q->row();

        if( count($result) == 0 ){
            $result = (object) array (
                'user_id'               => $user_id,
                'right_answer_percent'  => 0
            );
        }

        return $result;
    }

    public function getRanking( $season ){

        $this->db->limit(5);

        switch( $season ){

            case 1:
                $q = $this->db->get('vw_season_one_ranking');
                break;

            case 2:
                $q = $this->db->get('vw_season_two_ranking');
                break;

            case 3:
                $q = $this->db->get('vw_season_three_ranking');
                break;

            case 4:
                $q = $this->db->get('vw_season_four_ranking');
                break;
        }

        return $q->result();

    }

    public function getAllUsers(){
        $this->db->select('email');
        $q = $this->db->get('tbl_user');

        return $q->result();
    }

}

