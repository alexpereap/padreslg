$(document).ready(function() {

	$('#tabs li').click(function(event){
		console.log('click');
		if($(this).hasClass('active'))
			return 0;
		var id = $(this).index();
		$('#tabs li').removeClass('active');
		$(this).addClass('active');

		// containers
		$('.tabs-container').removeClass('active');
		$('.tabs-container').eq(id).addClass('active');
		
	});

	$('#slider-tabs li').click(function(event){
		if($(this).hasClass('active'))
			return 0;
		var id = $(this).index();
		$('#slider-tabs li').removeClass('active');
		$(this).addClass('active');
		// $('.slider-container .temp').removeClass('active');
		// $('.slider-container .temp').eq(id).addClass('active');
		
		// 
		var active = $('.slider-container .active');
		
		active.addClass('visibilityHidden');
		active.eq(id).addClass('visibilityHidden');

		active.on('transitionend',function(e){
			active.addClass('hidden');
			$('.slider-container .temp').removeClass('active');
			$('.slider-container .temp').eq(id).addClass('active');
			$('.slider-container .temp').eq(id).removeClass('hidden visibilityHidden');
	    });
	});

	
	$('.group-arrow .next').click(function(event) {
		var next = $('#slider-tabs li.active').next();
		if(next.length > 0)
			next.trigger('click');
		else
			$('#slider-tabs li:first').trigger('click');
	});

	$('.group-arrow .prev').click(function(event) {
		var prev = $('#slider-tabs li.active').prev();
		if(prev.length > 0)
			prev.trigger('click');
		else
			$('#slider-tabs li:last').trigger('click');
	});
	
	$(document).keydown(function(event) {
		switch(event.which){
			case 37:
			$('.group-arrow .prev').trigger('click');
			break;
			case 39:
			$('.group-arrow .next').trigger('click');
			break;
		}
	});


});