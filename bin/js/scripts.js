/*READY COMMONS*/
  jQuery(document).ready(function($) { /*"use strict";*/

  	jQuery.backstretch("./images/bg-mainDads-LG.jpg",{fade:200});
  	jQuery('#sider-cta').sidr();

  	//*CUSTOM DROPDOWNS*//
  	jQuery(".dropDownfrm a.chssd").click(function(event){
        event.preventDefault();
        //event.stopPropagation();
        var theDropdown = jQuery(this).parent().find('ul');
        if( theDropdown.is(':visible') ){ theDropdown.slideUp(300);
        }else{ theDropdown.slideDown(400); }
        theDropdown.parent().find('a.chssd').toggleClass('open');
    });

  	//*CHOOSING BADGES*//
  	jQuery(".thumbs-badges li img").click(function(event){
  		event.preventDefault();
  		/*UPDATE TO DEFAULT BADGES*/
  		jQuery('.thumbs-badges li').removeClass('choosed');
  		var titTeam = jQuery('#badge-constructor figcaption h1');
  			titTeam.text('').hide();
  		var $_this = jQuery(this), theBadge = $_this.parent();
  			theBadge.removeClass('inactive').addClass('choosed').siblings('.thumbs-badges li:not(.choosed)').addClass('inactive');
  		/*PREPARE vars LARGE BADGE */
			var theBagdeSRC = $_this.attr('src'), nameTeam = theBadge.parent().attr('data-NameTeam'), sizeData = theBadge.attr('data-sizeTxt'), posYdata = theBadge.attr('data-PosY');
			//console.log('src:'+theBagdeSRC);

		    jQuery('#badge-constructor img').fadeOut(400,function(){
		        $_this.theBagdeSRC = theBagdeSRC;
		        jQuery(this).fadeIn(400, function(){ /*SET THE TEAM NAME*/
					titTeam.css({'margin-top': posYdata+'0px', 'font-size': sizeData+'%'}).text(nameTeam).fadeIn();
				}).attr('src', theBagdeSRC);
		    });

  	});

    //GET NUMBER FROM
    var $numPoints = jQuery('#counter-points h1').text(), min_size = 6;
    //**COUNTER ANIMATED POINTS*//
      function incrementPoints(){
        var drawNum = parseFloat(jQuery('#counter-points h1').text()) + 1;
        jQuery('#counter-points h1').text( leading_zero( drawNum )  );
        //jQuery('#counter-points h1').text(parseFloat(jQuery('#counter-points h1').text())+1);
        //var incrementvalue =  parseFloat( jQuery('#counter-points h1').text() ) + 1 ;
        //jQuery('#counter-points h1').text( parseInt( incrementvalue , 6) );
          if(parseFloat(jQuery('#counter-points h1').text()) < $numPoints){
             setTimeout(incrementPoints,15.5);
          }
      };

      /* http://stackoverflow.com/questions/29532899/increment-a-number-with-out-getting-rid-of-leading-zeroes-jquery
        ;) */
        function leading_zero (num) {
          var new_num = num.toString();
          for (var i = new_num.length; i < min_size; i++) {
              new_num = '0' + new_num;
          }
          return new_num;
        };

    //CHECK TO THE POINTS TWEEN

    if( ( $numPoints != null || $numPoints != '' ) && ( parseFloat($numPoints) > 0 )  ){
      jQuery('#counter-points h1').text('000000');
      jQuery(window).load(function(){
        setTimeout( incrementPoints, 2000 );
      });
    }else{
      jQuery('#counter-points h1').text('000000');
    };
    /*PANEL SIDER*/
  	function makeSiderMobile(){
  		// ONLY MOBILE STUFF
  		 if(jQuery(window).width() < 979 ){
  		 	//PREVENT OPEN SIDER
  		 	jQuery.sidr('close');
  		 	if(jQuery("#sidr #head-dads").length){}else{
  		 		// CLONE AND APPEND HEAD DADS COMMONS
	  		 	jQuery("#head-dads").clone().appendTo('#sidr');
	  		 	jQuery("#sidr #lg-campaign-headlogo, #sidr #lg-main-logo").removeClass();
	  		 	jQuery("#sidr #lg-campaign-boxed").removeClass().attr('class',"container");
	  		 }
  		 }else{  };
  	};

      /*if(jQuery('#sidr')[0] || jQuery('#sidr').length > 0) {
    	  // SWIPPE TOUCH MOBILE
      	jQuery(window).touchwipe({

              wipeLeft: function() {
                jQuery.sidr('close'); // Close
              },
              wipeRight: function() {
                jQuery.sidr('open'); // Open
              },
              preventDefaultEvents: false

        });
      };*/

  		/*TRIGGER RESIZE FUNCTION*/
      jQuery(window).on('resize',function() {
          //console.log('resize');
          makeSiderMobile();
      }).trigger('resize');


    /* GAMEPLAY */

      var picked_answers_summary = [];
      var right_answer_count = 0;
      var game_time = 30000;
      var the_current_trivial_object = false;


      if( jQuery('.no-animated-head').length > 0 || jQuery('.no-animated-head')[0] ){
        jQuery('#lg-campaign-boxed').removeClass('fadeInDown');
        //jQuery('#head-dads').removeClass('animatedParent');
      }
      /*COUNTDOWN*/
        if( jQuery('body').hasClass('iscountdown') ){

          // some seconds before countdown begins
          setTimeout(function(){

            jQuery(".timer-counter").css('visibility', 'visible');
            jQuery("#main-runner-counter").runner({
              startAt: game_time,
              stopAt: 0,
              autostart: true,
              countdown: true
            }).on('runnerFinish', function(eventObject, info) {

                // time expires
                $(".triviaQuestion").hide();

                if( the_current_trivial_object === false ){
                    $trivial_object = jQuery('.triviaQuestion').last()
                } else {
                    $trivial_object = the_current_trivial_object;
                }

                animateTheFieldGame( $trivial_object , true);
            });

          }, 3000);

        };
      /*VALIDE IF TRIVIAL GROUP EXIST*/
        if(jQuery('#trivial-items')[0] || jQuery('#trivial-items').length > 0 ){
          if(jQuery(window).width() < 767 ){
            setTimeout(function(){ jQuery('#field-left-team .content, #field-right-team .content').addClass('toOpacity');
            }, 1200);
              if (jQuery(window).width() < 569) {
                jQuery('#footLgDads').hide();
              };
          }
          //PREPARE QUESTIONS TRIVIALS
            jQuery('li.triviaQuestion').hide();
            jQuery('li.triviaQuestion.actvTrivia').show();

            //CTA BTNS INSIDE TRIVIA QUESTION
              /*jQuery('.triviaCTA').click(function(e) {
                e.preventDefault();
                //CALL THE FUNCTION VALIDATION TRIVIAL
                fadeTrivialQuestion(jQuery(this).closest('.triviaQuestion'));
              });*/
              jQuery('.trivial-questions li a').click(function(e){


                // prevents double click
                if(  jQuery(this).hasClass('selected') ){
                  console.log("dbl click blocked");
                    return false;
                }

                e.preventDefault();
                jQuery('.trivial-questions li a').removeClass('selected')/*.removeClass('unselected')*/;
                jQuery(this).addClass('selected');

                the_current_trivial_object = jQuery(this).closest('.triviaQuestion');

                fadeTrivialQuestion( the_current_trivial_object , $(this).attr('answer-id'), $(this).attr('question-id') );
              });

            //FUNCTION TO SHOW NEXT TRIVIA QUESTION / FINAL RESULT
            function fadeTrivialQuestion($trivial, answer_id, question_id) {

              picked_answers_summary.push({
                question_id : question_id,
                answer_id : answer_id,
                time : null
              });


                $trivial.fadeOut('fast',function() {
                  jQuery('.triviaQuestion').removeClass('actvTrivia');
                  //RESOLVE THE LENGTH TRIVIAL WITH N° ID´s - triviaQuestion#
                  $trivResponseRes = jQuery('li#trivial-responses #trivial-question-result');
                  right_answer = false;

                  if( parseInt( answer_id ) == parseInt(CryptoJS.enc.Base64.parse( CryptoJS.AES.decrypt(match_tweaks[0],secret_key).toString(CryptoJS.enc.Utf8) ).toString(CryptoJS.enc.Utf8)) )
                    right_answer = true;
                  if( parseInt( answer_id ) == parseInt(CryptoJS.enc.Base64.parse( CryptoJS.AES.decrypt(match_tweaks[1],secret_key).toString(CryptoJS.enc.Utf8) ).toString(CryptoJS.enc.Utf8)) )
                    right_answer = true;
                  if( parseInt( answer_id ) == parseInt(CryptoJS.enc.Base64.parse( CryptoJS.AES.decrypt(match_tweaks[2],secret_key).toString(CryptoJS.enc.Utf8) ).toString(CryptoJS.enc.Utf8)) )
                    right_answer = true;

                  if( right_answer === true )
                    right_answer_count++;

                  $(".right-answer-count").text( right_answer_count );


                    if ($trivial.attr('id') !== jQuery('.triviaQuestion:last').attr('id')) {
                      animateTheFieldGame($trivial);
                      //SHOW THE ERROR OR SUCCESS TRIVIAL RESULT FOR THE QUESTION
                        jQuery('#trivial-responses').addClass('actvTrivia').fadeIn();
                        jQuery('#trivial-question-result').fadeIn();

                        $trivResponseRes.find('h1').hide();
                        //RESPONSE TRIVIAL QUESTION VALIDATE ATTR LI.triviaQuestion DATA-RESULT-QTN

                        console.log(right_answer);

                        if( right_answer === true ){
                          $trivResponseRes.find('h1.success-trivial-response').fadeIn();
                        }else{
                          $trivResponseRes.find('h1.error-trivial-response').fadeIn();
                        }
                        /* THE MAGIC IS CALLED AFTER
                          --- $trivial.next('.triviaQuestion').addClass('actvTrivia').fadeIn('fast'); */
                    } else {
                      //TRIVIAL IS COMPLETED
                        console.log('Trivial done!');

                        var theCurrentTeam = jQuery('.figPlayTeam.theCurrentTeam');
                        animateTheFieldGame($trivial, true);
                          //moveTeamAtPosition( $trivial, theCurrentTeam, 3 )
                    }
                });
            };



            //TWEEN FIELD GAME
            function animateTheFieldGame($trivial, isCompleted){
              //PAUSE THE COUNTER
                jQuery("#main-runner-counter").runner('stop');

                elapsed_time = jQuery("#main-runner-counter").runner('info').time;

                if( picked_answers_summary.length != 0 ){
                      if( picked_answers_summary.length == 1 ){
                        picked_answers_summary[ picked_answers_summary.length-1 ].time = game_time - elapsed_time;
                        remaining_time = elapsed_time;
                      } else {
                        picked_answers_summary[ picked_answers_summary.length-1 ].time = remaining_time - elapsed_time;
                        remaining_time = elapsed_time;
                      }
                }

                console.log(picked_answers_summary);

                var $theTeamIs = jQuery('.figPlayTeam.theCurrentTeam'), trivialPos = $trivial.find('h1 span.num').attr('data-current-trivial');

                if (!isCompleted) {
                  //SHOW THE FIELD GAME AFTER RESULT IN 2secondS IS HIDES
                  setTimeout(function(){
                    //RESET TO DEFAULTS
                      jQuery('.listTrivia.actvTrivia').fadeOut(function(){ jQuery(this).removeClass('actvTrivia'); });
                      jQuery('#game-field').addClass('showField');
                        jQuery('#game-play-field').animate({opacity:1},'slow');
                      moveTeamAtPosition($trivial, $theTeamIs, trivialPos, right_answer_count, isCompleted);
                  }, 2000);
                }else{
                  //TRIVIAL IS COMPLETED
                    setAnswersToServer( picked_answers_summary );

                    jQuery('#game-field').addClass('showField');
                    jQuery('#game-play-field').animate({opacity:1},'slow');

                    setTimeout(function(){ moveTeamAtPosition($trivial, $theTeamIs, trivialPos, right_answer_count, isCompleted); }, 2000);
                }

            };

            function setAnswersToServer( answers ){

                // TODO: OVERLAY WHILE APP SAVES INFO TO SERVER

                $.ajax({
                    url  : 'game/handle_match_answer',
                    type : 'post',
                    dataType: 'json',
                    data : ({
                        answers : answers
                    }),
                    success:function(result){
                        $("#getSharePoint").attr('href', $("#getSharePoint").attr('href') + '/' + result.match_id );

                        console.log(result);

                        // build game resume
                        if( result.match_end === true ){

                            match_summary = result.match_summary;
                            rival_time          = parseInt(match_summary.match_time_challenger) / 1000;
                            rival_right_answers = match_summary.number_of_right_answers_challenger;

                            my_time = parseInt(match_summary.your_match_time) / 1000;
                            my_right_answers = match_summary.your_number_of_right_answers;

                            if( match_summary.you_re_winner == 'yes' ){

                              $("#congratzTxt").text( '¡Felicidades! has ganado el partido' );
                              $("#congratzResume").text( 'Has ganado a tu oponente respondiendo ' + my_right_answers + ' de 3 preguntas en un tiempo de ' + my_time + ' segundos.' );

                            } else {

                                $("#congratzTxt").text( 'Lo sentimos has perdido el partido' );
                                $("#congratzResume").text( 'Tu oponente ha ganado respondiendo ' + rival_right_answers + ' de 3 preguntas en un tiempo de ' + rival_time + ' segundos.' );
                            }
                        }
                    }
                });
            };

            /*GAMEPLAYS TEAMS*/
            var limitsXPosNumber = [28,62,90], pathPosYTeamLeft = [75,24,50], pathPosYTeamRight = [24,75,50];
              /*minNumber = 0, maxNumber = 100,*/
              //moveTeamAtPosition(minNumber, maxNumber);
              function moveTeamAtPosition($trivial, $team, posQstnTrvl, pathToAdvance, isCompleted){

                //console.log('question trivial pos:'+posQstnTrvl)
                //var randomNumber = Math.floor(Math.random()*(max-min+1)+min);
                var leftORright, posXFieldTrvl, posYFieldTrvl, paramsAnimate, posQstnTrvlIS, dly;

                // means the user at least has one right answer and animation movement its neccesary
                if( pathToAdvance > 0 ){
                    posXFieldTrvl = limitsXPosNumber[pathToAdvance-1];
                }
                if (posQstnTrvl != 3) { posQstnTrvlIS = true;  dly = 1000; }else{ dly = 200 };
                //CHECK OUT FOR THE TEAM TO MOVE
                  if( $team.attr('id') == 'team-left' ){
                    leftORright = 'left';

                    if( pathToAdvance > 0 ){
                        posYFieldTrvl = pathPosYTeamLeft[pathToAdvance-1];
                    }
                    paramsAnimate = { top:posYFieldTrvl+'%', left:posXFieldTrvl+'%' };
                    jQuery('#trckPthLft.trckPth').delay(dly).animate({width:(posXFieldTrvl+8)+'%', opacity:1});
                      //console.log('trackPathWidth'+posXFieldTrvl+' trackPathWidthOpacity');
                  }else if( $team.attr('id') == 'team-right' ){
                    leftORright = 'right';
                    posYFieldTrvl = pathPosYTeamRight[pathToAdvance-1];
                    paramsAnimate = { top:posYFieldTrvl+'%', right:posXFieldTrvl+'%' };
                    jQuery('#trckPthRgt.trckPth').delay(dly).animate({width:(posXFieldTrvl+8.2)+'%', opacity:1});
                  };
                //return false;
                //ANIMATE THE TEAM
                if(posQstnTrvlIS && !isCompleted ){
                  //NORMAL TRIVIAL TWEENS
                  setTimeout(function(){
                    $team.animate(paramsAnimate,'slow', function(){
                      //RETURN TO THE TRIVIAL QUESTION VAL FROM fadeTrivialQuestion
                        setTimeout(function(){
                          jQuery('#game-field').removeClass('showField');
                          //THE MAGIC
                          $trivial.next('.triviaQuestion').addClass('actvTrivia').fadeIn('fast');
                           jQuery('.ruller-status li').removeClass('activeTrivial').eq(posQstnTrvl).addClass('activeTrivial');
                          jQuery('#game-play-field').animate({opacity:0},'fast', function(){
                            jQuery(this).removeClass();
                          });
                          jQuery("#main-runner-counter").runner('start');
                        }, 1200);
                        //console.log('HAS ANIMATED');
                    });
                  }, 1000);
                }else{
                  //SHOW THE COMPLETE TRIVIAL RESULT
                    $team.animate(paramsAnimate,'slow', function(){
                      setTimeout(function(){
                        jQuery('#game-field').removeClass('showField');
                        jQuery('#game-play-field').animate({'opacity':0},'fast');
                        //RUNNER HAS BEEN STOPED --
                        jQuery("#main-runner-counter").runner('stop');
                          var finalTime = jQuery("#main-runner-counter").text();
                          jQuery('#result-runner-counter').text(finalTime);
                        //SHOW END RESULTS
                        $trivial.next('.triviaQuestion').removeClass('actvTrivia');
                        jQuery('#trivial-responses, #trivial-question-result').hide();
                        jQuery('#trivial-responses').fadeIn('fast', function(){
                          jQuery('#trivial-end-result').fadeIn('fast');
                        });
                      }, 1000);
                    });

                }
              };

        };

    /*CHARTS*/
      if( jQuery('.info-charts')[0] || jQuery('.info-charts').length > 0 ){
        jQuery(function() {
          jQuery('.chart').easyPieChart({
            barColor: '#c20e3a',
            trackColor: '#dee0e0',
            lineWidth: 22,
            size: 137,
            scaleColor : false,
            lineCap: 'butt',
            animate: 1400,
            onStop: function(el){
              jQuery('.fig-the-chart').addClass('bgChart');
            }
          });
        });
      };

  });

