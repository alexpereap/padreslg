$(document).ready(function(){



    // Footer fix
    footerFix();

    $("#teamBuildForm").submit(function(e){

        if( $("#teamShieldId").val() == '' ){
            alert("Primero Selecciona un escudo!");
            e.preventDefault();
            return false;
        }

    });

    // team builder shield
    $("#teamBuilderShields ul li").click(function(){

        shield_id = $(this).attr('shield-id');
        $("#teamShieldId").val( shield_id );

        img_src = $(this).find('img').first().attr('src');

        $("#headTeamShield").fadeOut(500, function(){ $("#headTeamShield").attr('src', img_src); $(this).fadeIn() })
    });

    // team builder entity
    $("#teamBuilderEntityNav ul li").click(function(){

        $("#headerTeamEntity").text( $(this).text() );
        $("#teamEntity").val( $(this).text() );
    });

    // register form validator
    $("#fatherForm").bootstrapValidator({

        feedbackIcons: {
           /*valid: 'glyphicon glyphicon-ok',*/
           invalid: 'glyphicon glyphicon-remove',
           validating: 'glyphicon glyphicon-refresh'
       },
       fields : {
            father_firstname: {
             validators: {
                 notEmpty: {
                     message: 'El nombre es requerido'
                 },
                 stringLength: {
                        min: 2,
                        max: 100
                    },
                     regexp: {
                     regexp: /^[A-za-zÁÉÍÓÚáéíóúñÑäëïöüÄËÏÖÜ\s]+$/,
                     message: 'El nombre solo puede contener letras'
                    }
                }
            },
            father_lastname: {
             validators: {
                 notEmpty: {
                     message: 'El apellido es requerido'
                 },
                 stringLength: {
                        min: 2,
                        max: 100
                    },
                     regexp: {
                     regexp: /^[A-za-zÁÉÍÓÚáéíóúñÑäëïöüÄËÏÖÜ\s]+$/,
                     message: 'El nombre solo puede contener letras'
                    }
                }
            }
       }
    }).on('success.form.bv', function(e) {

        if( $("input[name=father_photo]").val() == '' ){

            e.preventDefault();
            $("input[type=submit]").removeAttr('disabled');
            alert("Debes subir una foto");
            return false;
        }

        if( !$("#checkbox1").prop('checked') ){
            e.preventDefault();
            $("input[type=submit]").removeAttr('disabled');
            alert("Debes aceptar los términos y condiciones");
            return false;
        }

        ext = $("#fatherPhoto").val().split('.');
        ext = ext[ ext.length - 1 ].toLowerCase();
        console.log(ext);

        if( ext != 'png'  && ext != 'jpg' && ext != 'jpeg' ){
            e.preventDefault();
            $("input[type=submit]").removeAttr('disabled');
            alert("Por favor sube un archivo de extensión jpg, jpeg, png");
            return false;
        }
    });

    // register add father photo handler
    $("#addFatherPhoto").click(function(e){
        e.preventDefault();
        $("#fatherPhoto").click();
    });

    $("#fatherPhoto").change(function(){
        $("#fatherPhotoFeedback").html( $(this).val() );
    });

    // dropdown functionality
    $(".dropDownfrm ul li a").click(function(e){

        e.preventDefault();
        $(".dropDownfrm ul").slideUp();
        input_value = $(this).html();

        $(".chssd").html( input_value );
        $("#nidType").val( input_value );

    });

    // form validation
    $("#registerForm").bootstrapValidator({

        feedbackIcons: {
           /*valid: 'glyphicon glyphicon-ok',*/
           invalid: 'glyphicon glyphicon-remove',
           validating: 'glyphicon glyphicon-refresh'
       },
       fields : {
          firstname: {
             validators: {
                 notEmpty: {
                     message: 'El nombre es requerido'
                 },
                 stringLength: {
                        min: 2,
                        max: 100
                    },
                     regexp: {
                     regexp: /^[A-za-zÁÉÍÓÚáéíóúñÑäëïöüÄËÏÖÜ\s]+$/,
                     message: 'El nombre solo puede contener letras'
                    }
                }
            },
          lastname: {
             validators: {
                 notEmpty: {
                     message: 'El apellido es requerido'
                 },
                 stringLength: {
                        min: 2,
                        max: 100
                    },
                     regexp: {
                     regexp: /^[A-za-zÁÉÍÓÚáéíóúñÑäëïöüÄËÏÖÜ\s]+$/,
                     message: 'El nombre solo puede contener letras'
                    }
                }
            },
            email: {
             validators: {
                 notEmpty: {
                     message: 'El correo electrónico es requerido'
                 },
                 emailAddress: {
                     message: 'El correo electrónico no es valido'
                 }
                     }
            },
            nid:{
             validators: {
                 notEmpty: {
                     message: 'El campo de identificación es requerido'
                 },
                 regexp: {
                     regexp: /^[0-9]+$/,
                     message: 'El número de cédula solo puede contener números'
                 }
             }
            },
            address:{
             validators: {
                 notEmpty: {
                     message: 'El campo de dirección es requerido'
                 }
             }
            },
            city:{
             validators: {
                 notEmpty: {
                     message: 'La ciudad es requerida'
                 },
                 stringLength: {
                        min: 2,
                        max: 20
                    }

             }
            },
             cellphone: {
             message: 'El número de telefono celular no es valido',
             validators: {
                notEmpty: {
                     message: 'Campo requerido'
                 },
                 regexp: {
                     regexp: /^[0-9]+$/,
                     message: 'El número de telefono celular solo puede contener números'
                 },
                 stringLength: {
                        min: 10,
                        max: 10
                    }
                }
            },
       }
    });

});

// share window display
$(window).load(function(){

      $("#landingShareTrigger").click(function(){

        FB.ui(
          {
            method: 'share',
            href: site_uri,
          },
          // callback
          function(response) {
            if (response && !response.error_code) {
              // Post completed action
              $.ajax({
                  url  : 'landing/ajax_insert_team_score',
                  type : 'post',
                  success: function(result){

                    console.log(result);
                       window.location.reload();
                  }
              });

            } else {
              // alert('Error while posting.');
            }
          }
        );
    });
});

// share window display
$(window).load(function(){

      $("#shareTrigger").click(function(){

        FB.ui(
          {
            method: 'share',
            href: site_uri,
          },
          // callback
          function(response) {
            if (response && !response.error_code) {
              // Post completed action
              $.ajax({
                  url  : 'game/ajax_insert_match_end_score',
                  type : 'post',
                  success: function(result){

                    console.log(result);
                       window.location.reload();
                  }
              });

            } else {
              // alert('Error while posting.');
            }
          }
        );
    });
});

$(window).resize(function(){
    footerFix();
});

$( "#main-middle" ).scroll(function() {

var scrollPosition = $(this).scrollTop();
   console.log( scrollPosition );

});

function footerFix(){
    if($(window).width() <= 979 ){

        the_height = $(window).height();
        // console.log( $( "#main-middle" ).scrollTop() );
        console.log( $( "#main-middle" ).height() );



    }
}