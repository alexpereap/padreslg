var secret_key = 'a4PxHPO0ibJxBMrlD102n9n40NsR9xJe';

$(window).load(function(){

    $("#shareMatchPointFb").click(function(){

        FB.ui(
          {
            method: 'share',
            href: this_site_uri,
          },
          // callback
          function(response) {
            if (response && !response.error_code) {
              // Post completed action
              $.ajax({
                  url  : 'game/ajax_insert_match_end_score',
                  type : 'post',
                  data : ({
                        type       : 'match-share',
                        extra_info : 'facebook-share'
                  }),
                  success: function(result){
                      $("#shareMatchPointFb").remove();
                      window.location.reload();
                  }
              });

            } else {
              // alert('Error while posting.');
            }
          }
        );
    });

    $("#shareMatchPointUrl").click(function(){

        $.ajax({
          url  : 'game/ajax_insert_match_end_score',
          type : 'post',
          data : ({
            type       : 'match-share',
            extra_info : 'url-share'
        }),
            success: function(result){
                $("#shareMatchPointFb").remove();
                $("#shareMatchPointUrl").remove();

                // alert("has ganado un punto extra por compartir!");
                window.location.reload();
            }
        });
    });
});

$(document).ready(function(){

    $("#submitMatch").submit(function(e){
        if( $("#myOponent").val() == '' ){
            alert("Escoje primero un oponente!");
            e.preventDefault();
            return false;
        }
    });

    // game countadon
    if( $("#inMatchCourse").val() == 1 ){

        gameMechanics();
    }

    // start game
    var type_of_oponent;

    $("#findRandomOponent").click(function(){

        $("#againstFriend").hide();

        type_of_oponent = 'random';
        $("#oponentType").val( type_of_oponent );

        $.ajax({
            url : 'game/ajax_get_random_player',
            type: 'post',
            dataType : 'json',
            async : false,
            success : function(result){

                if( result.success === false ){
                    alert( result.msg );
                }

                $("#randomPlayer").html( result.oponent_name );
                $("#randomPlayer").show();
                $("#myOponent").val( result.oponent_enc_id );
                $("#friendOponent").val('');
            }
        });


    });

    $("#findFriendOponent").click(function(){

        $("#randomPlayer").hide();

        $("#againstFriend").show();
        type_of_oponent = 'friend';
        $("#oponentType").val( type_of_oponent );

    });

    $("#friendOponent").change(function(){
        $("#myOponent").val( $(this).val() );
    });

});

var initial = 10000;
var count = initial;
var counter; //10 will  run it every 100th of a second
var initialMillis;

function gameMechanics(){

    // stores the answer id
    var answer_id;
    startTimer();

    $("input[name=answer]").change(function(){

        // onlu permits this operation once (when the user hadn't choose any answer)
        if( typeof answer_id == 'undefined' ){
            answer_id = $(this).val();

            // stop timer
            stopTimer();

            // disables answer options
            $("input[name=answer]").attr('disabled','disabled');

            // ajax service to parse the_answer_id
            user_pick_answer( answer_id );

        }
    });

}

function user_pick_answer( answer_id ){

    $.ajax({

        url      : 'game/ajax_get_answer',
        type     : 'post',
        dataType : 'json',
        data     : ({
            answer_id : answer_id,
            time      : initial - count

        }),
        success  : function(result){

            alert(result.right_answer);
            $("#nextQuestion").show();
        }
    });
}

function timer() {

    // question time expired
    if (count <= 0) {
        clearInterval(counter);
        $("#timer").html('0.000');

        alert("te demoraste mucho, avanza a la siguiente pregunta")
        $("input[name=answer]").attr('disabled','disabled');
        $("#nextQuestion").show();

        return;
    }
    var current = Date.now();

    count = count - (current - initialMillis);
    initialMillis = current;
    displayCount(count);
}

function displayCount(count) {
    var res = count / 1000;

    if( res > 0 )
    document.getElementById("timer").innerHTML = res.toPrecision(count.toString().length );
}

function startTimer(){
    clearInterval(counter);
    initialMillis = Date.now();
    counter = setInterval(timer, 10);
};

function stopTimer() {
    clearInterval(counter);
};



/*function timer(){

    // user answered
    if (stop_timer === true ){
        clearInterval(counter);
        return;
    }

    // user time expired
    if( count <= 0  ){

        alert("te demoraste mucho, avanza a la siguiente pregunta")
        $("input[name=answer]").attr('disabled','disabled');
        $("#nextQuestion").show();

    }


    count--;
    $("#timer").stop(true,true).html(count /100);
}*/